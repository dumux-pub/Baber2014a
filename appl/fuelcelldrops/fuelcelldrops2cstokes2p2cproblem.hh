/*****************************************************************************
 *   Copyright (C) 2009-2012  by Katherina Baber                      *
 *   Copyright (C) 2009 by Andreas Lauser, Bernd Flemisch                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FUELCELLDROPS2CSTOKES2P2CPROBLEM_HH
#define DUMUX_FUELCELLDROPS2CSTOKES2P2CPROBLEM_HH

//choose coupling concept: 1 coupling with storage term; 0 coupling with pore velocity
#define COUPLINGCONCEPT 1

//this is an isothermal model (flag needed for common file coupledmortardropsproblem.hh)
#define NI 0

#include <numeric> // required for std::partial_sum used in multidomainmcmgmapper.hh
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dumux/io/vtkmultiwriter.hh>
#include <appl/xfem/common/helperfunctions.hh>
#include <dumux/linear/pardisobackend.hh>

#include <dumux/multidomain/common/multidomainproblem.hh>
#include <dumux/modelcoupling/mortar2cstokes2p2cdrops/mortar2cstokes2p2cdropslocaloperator.hh>
#include <dumux/modelcoupling/mortar2cstokes2p2cdrops/mortar2cstokes2p2cdropsnewtoncontroller.hh>
#include <dumux/modelcoupling/common/coupledmortardropsproblem.hh>

#include "fuelcelldrops2cstokes2p2cspatialparameters.hh"
#include <dumux/material/fluidsystems/h2oairdropsfluidsystem.hh>

#include "stokes2cfuelcelldropssubproblem.hh"
#include "2p2cfuelcelldropssubproblem.hh"

namespace Dumux
{
template <class TypeTag>
class TwoCStokesTwoPTwoCFuelCellDropsProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoCStokesTwoPTwoCFuelCellDropsProblem, INHERITS_FROM(MultiDomainEnriched));

// Set the grid type
SET_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, Grid)
{
 public:
    typedef typename Dune::UGGrid<2> type;
    //typedef typename Dune::YaspGrid<2> type;
};

// Specify the multidomain gridview
SET_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, GridView)
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
 public:
    typedef typename MultiDomainGrid::LeafGridView type;
};

// Set the global problem
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, Problem, TwoCStokesTwoPTwoCFuelCellDropsProblem<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, SubDomain1TypeTag, TTAG(Stokes2cFuelCellDropsSubProblem));
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, SubDomain2TypeTag, TTAG(TwoPTwoCFuelCellDropsSubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(Stokes2cFuelCellDropsSubProblem, MultiDomainTypeTag, TTAG(TwoCStokesTwoPTwoCFuelCellDropsProblem));
SET_TYPE_PROP(TwoPTwoCFuelCellDropsSubProblem, MultiDomainTypeTag, TTAG(TwoCStokesTwoPTwoCFuelCellDropsProblem));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(Stokes2cFuelCellDropsSubProblem, OtherSubDomainTypeTag, TTAG(TwoPTwoCFuelCellDropsSubProblem));
SET_TYPE_PROP(TwoPTwoCFuelCellDropsSubProblem, OtherSubDomainTypeTag, TTAG(Stokes2cFuelCellDropsSubProblem));

SET_PROP(Stokes2cFuelCellDropsSubProblem, SpatialParams)
{ typedef Dumux::TwoCStokesTwoPTwoCFuelCellDropsSpatialParams<TypeTag> type; };

SET_PROP(TwoPTwoCFuelCellDropsSubProblem, SpatialParams)
{ typedef Dumux::TwoCStokesTwoPTwoCFuelCellDropsSpatialParams<TypeTag> type; };

// Set the fluid system
SET_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, FluidSystem)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dumux::FluidSystems::H2OAirDrops<Scalar,
        Dumux::H2O<Scalar>,
        /*useComplexrelations=*/false> type;
};

SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, JacobianAssembler, Dumux::MultiDomainEnrichedAssembler<TypeTag>);
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, MultiDomainCouplingLocalOperator, Dumux::MortarTwoCStokesTwoPTwoCDropsLocalOperator<TypeTag>);

SET_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, SolutionVector)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator) MultiDomainGridOperator;
 public:
    typedef typename MultiDomainGridOperator::Traits::Domain type;
};

// newton parameters
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, NewtonController, Dumux::MortarTwoCStokesTwoPTwoCDropsNewtonController<TypeTag>);
//#ifdef HAVE_PARDISO
//SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, LinearSolver,
//PardisoBackend<TypeTag>);
//#else
SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, LinearSolver,
SuperLUBackend<TypeTag>);
//#endif // HAVE_PARDISO
//SET_TYPE_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, LinearSolver, SuperLUBackend<TypeTag>);

//default is numEq, for mortar it has to be 1
SET_INT_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, LinearSolverBlockSize, 1);
// used for powercouplingGFS: here y-velocity pressure, transport-flux, drop volume, drop pressure
#if COUPLINGCONCEPT //with storage term
    SET_INT_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, NumEq, 4);
#else // with pore velocity
    SET_INT_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, NumEq, 5);
#endif
    
//typeTags for drops
SET_PROP(TwoCStokesTwoPTwoCFuelCellDropsProblem, DropMap)
{
    private:
        typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
        typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;
        typedef typename MultiDomainGridView::IndexSet::IndexType IndexType;
        typedef typename Dumux::DropInformation DropInformation;
    public:
        typedef typename std::map<IndexType, DropInformation> type;
};
}

/*!
 * \brief The problem class for the coupling of a two-component Stokes (stokes2c)
 *        and a two-phase two-component Darcy model (2p2c) using the mortar method.
 *        The formation, growth and detachment of drops on the interface is also described.
 *
 *        The problem class for the coupling of a isothermal two-component Stokes (stokes2c)
 *        and a isothermal two-phase two-component Darcy model (2p2c).
 *        It uses the 2p2cCoupling model and the Stokes2ccoupling model and provides
 *        the problem specifications for common parameters of the two submodels.
 *        The formation, growth and detachment of drops on the interface is also described.
 *        The initial and boundary conditions of the submodels are specified in the two subproblems,
 *        2p2cnisubproblem.hh and stokes2cnisubproblem.hh, which are accessible via the coupled problem.
 */
template <class TypeTag = TTAG(TwoCStokesTwoPTwoCFuelCellDropsProblem) >
class TwoCStokesTwoPTwoCFuelCellDropsProblem : public CoupledMortarDropsProblem<TypeTag>
{
    template<int dim>
    struct VertexLayout
    {
        bool contains(Dune::GeometryType gt)
        { return gt.dim() == 0; }
    };

    typedef TwoCStokesTwoPTwoCFuelCellDropsProblem<TypeTag> ThisType;
    typedef CoupledMortarDropsProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) NewtonController;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Stokes2cFuelCellDropsTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCFuelCellDropsTypeTag;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, Problem) Stokes2cFuelCellDropsSubProblem;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, Problem) TwoPTwoCFuelCellDropsSubProblem;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, GridView) Stokes2cDropsGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, GridView) TwoPTwoCDropsGridView;

    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, SolutionVector) TwoPTwoCDropsSolutionVector;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, PrimaryVariables) Stokes2cDropsPrimaryVariables;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, PrimaryVariables) TwoPTwoCDropsPrimaryVariables;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, VolumeVariables) VolumeVariables1;

    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, ElementVolumeVariables) ElementVolumeVariables2;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, VolumeVariables) VolumeVariables2;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, FluxVariables) FluxVariables1;
    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, FluxVariables) BoundaryVariables1;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;

    typedef Dumux::VtkMultiWriter<MultiDomainGridView> VtkMultiWriter;

    typedef typename SubDomainGrid::Traits::template Codim<0>::EntityPointer SubDomainElementPointer;
    typedef typename Stokes2cDropsGridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename TwoPTwoCDropsGridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;

    typedef typename GET_PROP_TYPE(Stokes2cFuelCellDropsTypeTag, Indices) Stokes2cDropsIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCFuelCellDropsTypeTag, Indices) TwoPTwoCDropsIndices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) GridFunctionSpace;

    enum { dim = Stokes2cDropsGridView::dimension };
    enum { // indices in the Stokes domain
        momentumXIdx1 = Stokes2cDropsIndices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx1 = Stokes2cDropsIndices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx1 = Stokes2cDropsIndices::momentumZIdx, //!< Index of the z-component of the momentum balance
        massBalanceIdx1 = Stokes2cDropsIndices::massBalanceIdx, //!< Index of the mass balance
        transportEqIdx1 = Stokes2cDropsIndices::transportEqIdx //!< Index of the transport equation
    };
    enum{
        velocityXIdx1 = Stokes2cDropsIndices::velocityXIdx,
        velocityYIdx1 = Stokes2cDropsIndices::velocityYIdx,
        velocityZIdx1 = Stokes2cDropsIndices::velocityZIdx,

        pressureIdx1 = Stokes2cDropsIndices::pressureIdx,
        massOrMoleFracIdx = Stokes2cDropsIndices::massOrMoleFracIdx
    };
    enum { // indices of the PVs in the Darcy domain
        massBalanceIdx2 = TwoPTwoCDropsIndices::pressureIdx,
        switchIdx2 = TwoPTwoCDropsIndices::switchIdx
    };
    enum { // indices of the balance equations
        contiTotalMassIdx2 = TwoPTwoCDropsIndices::contiNEqIdx,//GET_PROP_VALUE(TwoPTwoCFuelCellDropsTypeTag, ReplaceCompIdx) ,
        transportEqIdx = TwoPTwoCDropsIndices::contiWEqIdx,//GET_PROP_VALUE(TwoPTwoCFuelCellDropsTypeTag, TransportEqIdx)
    };
    enum { transportCompIdx1 = Stokes2cDropsIndices::transportCompIdx };
    enum {
        wCompIdx2 = TwoPTwoCDropsIndices::wCompIdx,
        nCompIdx2 = TwoPTwoCDropsIndices::nCompIdx
    };
    enum {
        wPhaseIdx2 = TwoPTwoCDropsIndices::wPhaseIdx,
        nPhaseIdx2 = TwoPTwoCDropsIndices::nPhaseIdx
    };
    enum { phaseIdx = GET_PROP_VALUE(Stokes2cFuelCellDropsTypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(Stokes2cFuelCellDropsTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(TwoPTwoCFuelCellDropsTypeTag, NumEq)
    };
#if COUPLINGCONCEPT //with storage term
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        totalLMP = 4 //!< total number of langrange multiplier vx, vy, p, labdaX
    };
#else //with pore velocity
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        pDrop = 4, //!< lagrange multiplier: drop water pressure
        totalLMP = 5 // 4 //!< total number of langrange multiplier vx, vy, p, labdaX, pdrop
    };
#endif

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename MultiDomainGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename SubDomainGrid::template Codim<0>::LeafIterator SubDomainElementIterator;
    typedef typename Stokes2cDropsGridView::IntersectionIterator StokesIntersectionIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator SubDomainInterfaceIterator;
    typedef typename MultiDomainGrid::template Codim<dim>::LeafIterator MultiDomainVertexIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator::Intersection Intersection;//is this the right type?
    typedef typename Dune::PDELab::IntersectionGeometry<Intersection>::IntersectionGeometry IntersectionGeometry;
    typedef typename MultiDomainGridView::IndexSet::IndexType IndexType;

public:
    TwoCStokesTwoPTwoCFuelCellDropsProblem(MultiDomainGrid &mdGrid,
                                  TimeManager &timeManager)
        : ParentType(mdGrid, timeManager)
    {
        // subdivide grid in two subdomains
        mortarStokes2cDrops_ = this->sdID1();
        mortarTwoPTwoCDrops_ = this->sdID2();

        mdGrid.startSubDomainMarking();

        // define location of the interface
        const Scalar interface = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        episodeLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Output, EpisodeLength);
        initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

        ElementIterator eendit = mdGrid.template leafend<0>();
        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (elementIt->partitionType() != Dune::InteriorEntity)
                continue;

            GlobalPosition globalPos = elementIt->geometry().center();

            if (globalPos[1] < interface) // for TOP problem
                mdGrid.addToSubDomain(mortarTwoPTwoCDrops_,*elementIt);
            else
                mdGrid.addToSubDomain(mortarStokes2cDrops_,*elementIt);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();

        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());

//        //mark entities for refinement
//        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
//        {
//          GlobalPosition global = elementIt->geometry().center();
//
//          if (global[1] > interface)
//            mdGrid.mark(1,*elementIt);
//        }
//
//        mdGrid.preAdapt();
//        mdGrid.adapt();
//        mdGrid.postAdapt();
////
//        //mark entities for refinement
//        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
//        {
//          GlobalPosition global = elementIt->geometry().center();
//
//          if (global[1] > interface)
//            mdGrid.mark(1,*elementIt);
//        }
//
//        mdGrid.preAdapt();
//        mdGrid.adapt();
//        mdGrid.postAdapt();
////
//        //mark entities for refinement
//        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
//        {
//          GlobalPosition global = elementIt->geometry().center();
//
//          if (global[1] > interface)
//            mdGrid.mark(1,*elementIt);
//        }
//
//        mdGrid.preAdapt();
//        mdGrid.adapt();
//        mdGrid.postAdapt();
////
//        //mark entities for refinement
//        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
//        {
//          GlobalPosition global = elementIt->geometry().center();
//
//          if (global[1] > interface)
//            mdGrid.mark(1,*elementIt);
//        }
//
//        mdGrid.preAdapt();
//        mdGrid.adapt();
//        mdGrid.postAdapt();

//        //mark entities for refinement
//        for (ElementIterator elementIt = mdGrid.template leafbegin<0>(); elementIt != eendit; ++elementIt)
//        {
//          GlobalPosition global = elementIt->geometry().center();
//
//          if (global[1] > interface)
//            mdGrid.mark(1,*elementIt);
//        }
//
//        mdGrid.preAdapt();
//        mdGrid.adapt();
//        mdGrid.postAdapt();

//        MultiDomainVertexIterator vertendit = mdGrid.template leafend<dim>();
//        int i=0;
//        for (MultiDomainVertexIterator vertIt = this->mdGrid().template leafbegin<dim>(); vertIt != vertendit; ++vertIt)
//        {
//            std::cout<<"global coord of vertex in domain "<<i<<" indexSet "<<this->gridView().indexSet().index(*vertIt)<<": "<<vertIt->geometry().center()<<std::endl;
//            i++;
//        }
//        SubDomainVertexIterator vertendit2 = this->sdGrid2().template leafend<dim>();
//        int i=0;
//        for (SubDomainVertexIterator vertIt2 = this->sdGrid2().template leafbegin<dim>(); vertIt2 != vertendit2; ++vertIt2)
//        {
//          std::cout<<"global coord of vertex in domain2 "<<i <<"indexSet "<<this->sdGridView2().indexSet().index(*vertIt2)<<": "<<vertIt2->geometry().center()<<std::endl;
//          i++;
//        }
//        SubDomainVertexIterator vertendit1 = this->sdGrid1().template leafend<dim>();
//        int j=0;
//        for (SubDomainVertexIterator vertIt1 = this->sdGrid1().template leafbegin<dim>(); vertIt1 != vertendit1; ++vertIt1)
//        {
//          std::cout<<"global coord of vertex in domain1 "<<j<<"indexSet "<<this->sdGridView1().indexSet().index(*vertIt1)<<": "<<vertIt1->geometry().center()<<std::endl;
//          j++;
//        }

        // initialize the tables of the fluid system
        FluidSystem::init();

        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);
    }

    ~TwoCStokesTwoPTwoCFuelCellDropsProblem()
    {
        fluxFile_.close();
    };

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        //init sub-models and mortar-dofs
        ParentType::init();
        this->sdProblem2().spatialParams().plotMaterialLaw();

        //determine integration points for mortar method (especially important for non-conforming grids)
        MortarGeometry<TypeTag> mortarGeometry(*this);//this = globalProblem
        mortarGeometry.createEnrichedIPGeometryMap();

        std::cout << "Writing flux data at interface\n";
        if (this->timeManager().time() == 0)
        {
            fluxFile_.open("fluxes.out");
            fluxFile_ << "time flux1 advFlux1 diffFlux1 totalFlux1 flux2 wPhaseFlux2 nPhaseFlux2\n";
            counter_ = 1;
        }
        else
            fluxFile_.open("fluxes.out", std::ios_base::app);
    }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void postTimeStep()
    {
        ParentType::postTimeStep();

        if (shouldWriteFluxFile() || shouldWriteVaporFlux())
        {
            counter_ = this->sdProblem1().currentVTKFileNumber() + 1;

            calculateFirstInterfaceFluxes();
            calculateSecondInterfaceFluxes();
        }
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    /*
     * \brief Calculates fluxes and coupling terms at the interface for the Stokes model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateFirstInterfaceFluxes()
    {
        const MultiDomainGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables1 elemVolVarsPrev1, elemVolVarsCur1;
        Scalar sumVaporFluxes = 0.;
        Scalar advectiveVaporFlux = 0.;
        Scalar diffusiveVaporFlux = 0.;

        // count number of elements to determine number of interface nodes
        int numElements = 0;
        const SubDomainInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(mortarStokes2cDrops_, mortarTwoPTwoCDrops_);
        for (SubDomainInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(mortarStokes2cDrops_, mortarTwoPTwoCDrops_); ifIt != endIfIt; ++ifIt)
            numElements++;

        const int numInterfaceVertices = numElements + 1;
        std::vector<InterfaceFluxes<numEq1> > outputVector(numInterfaceVertices); // vector for the output of the fluxes
        FVElementGeometry1 fvGeometry1;
        int interfaceVertIdx = -1;

        // loop over the element faces on the interface
        for (SubDomainInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(mortarStokes2cDrops_, mortarTwoPTwoCDrops_); ifIt != endIfIt; ++ifIt)
        {
            const int firstFaceIdx = ifIt->indexInFirstCell();
            const MultiDomainElementPointer mdElementPointer1 = ifIt->firstCell(); // ATTENTION!!!
            const MultiDomainElement& mdElement1 = *(mdElementPointer1);
            const SubDomainElementPointer sdElementPointer1 = this->sdElementPointer1(mdElement1);
            const SubDomainElement1& sdElement1 = *(sdElementPointer1);
            fvGeometry1.update(this->sdGridView1(), sdElement1);

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
            const int numVerticesOfFace = referenceElement1.size(firstFaceIdx, 1, dim);

            // evaluate residual of the sub model without boundary conditions (stabilization is removed)
            // the element volume variables are updated here
            this->localResidual1().evalNoBoundary(sdElement1, fvGeometry1,
                                                  elemVolVarsPrev1, elemVolVarsCur1);

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem1 = referenceElement1.subEntity(firstFaceIdx, 1, nodeInFace, dim);
                const DimVector& vertexGlobal = mdElement1.geometry().corner(vertInElem1);
                const int firstGlobalIdx = this->mdVertexMapper().map(mortarStokes2cDrops_, mdElement1, vertInElem1, dim);
                const ElementSolutionVector1& firstVertexDefect = this->localResidual1().residual();

                // loop over all interface vertices to check if vertex id is already in stack
                bool existing = false;
                for (int interfaceVertex=0; interfaceVertex < numInterfaceVertices; ++interfaceVertex)
                {
                    if (firstGlobalIdx == outputVector[interfaceVertex].globalIdx)
                    {
                        existing = true;
                        interfaceVertIdx = interfaceVertex;
                        break;
                    }
                }

                if (!existing)
                    interfaceVertIdx++;

                if (shouldWriteFluxFile()) // compute only if required
                {
                    outputVector[interfaceVertIdx].interfaceVertex = interfaceVertIdx;
                    outputVector[interfaceVertIdx].globalIdx = firstGlobalIdx;
                    outputVector[interfaceVertIdx].xCoord = vertexGlobal[0];
                    outputVector[interfaceVertIdx].yCoord = vertexGlobal[1];
                    outputVector[interfaceVertIdx].count += 1;
                    for (int eqIdx=0; eqIdx < numEq1; ++eqIdx)
                        outputVector[interfaceVertIdx].defect[eqIdx] +=
                            firstVertexDefect[vertInElem1][eqIdx];
                }

                int boundaryFaceIdx =
                    fvGeometry1.boundaryFaceIndex(firstFaceIdx, nodeInFace);

                const BoundaryVariables1 boundaryVars1(this->sdProblem1(),
                                                       sdElement1,
                                                       fvGeometry1,
                                                       boundaryFaceIdx,
                                                       elemVolVarsCur1,
                                                       /*onBoundary=*/true);

                advectiveVaporFlux += computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
                diffusiveVaporFlux += computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem1);
//                sumVaporFluxes += firstVertexDefect[vertInElem1][transportEqIdx1];

//                const GlobalPosition& globalPos = fvGeometry1.subContVol[nodeInFace].global;
//                problem specific function, in problem orientation of interface is known
//                if(this->sdProblem1().isInterfaceCornerPoint(globalPos) && globalPos[0] == 0)
//                {
//                     int verticalBoundaryFaceIdx =
//                             fvGeometry1.boundaryFaceIndex(1, 0);
//                 Stokes2cDropsPrimaryVariables priVars(0.0);
//                //                         DimVector faceCoord = this->fvGeometry_().boundaryFace[boundaryFaceIdx].ipGlobal;
//                                         std::cout<<globalPos<<std::endl;
//                   //calculate the actual boundary fluxes and add to residual (only for momentum and transport equation, mass balance already has outflow)
//                   this->localResidual1().computeFlux(priVars, 0, true/*on boundary*/);
////                       sumVaporFluxes += priVars[transportEqIdx1];
//                }
            }
        }

        if (shouldWriteFluxFile())
        {
            std::cout << "Writing flux file\n";
            char outputname[20];
            sprintf(outputname, "%s%05d%s","fluxes1_", counter_,".out");
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Xcoord1 "
                    << "totalFlux1 "
                    << "componentFlux1 "
                    << "momentumXFlux1 "
                    << "momentumYFlux1 "
                    << "count1 "
                    << std::endl;
            for (int interfaceVertIdx=0; interfaceVertIdx < numInterfaceVertices; interfaceVertIdx++)
            {
                if (outputVector[interfaceVertIdx].count > 2)
                    std::cerr << "too often at one node!!";

                if (outputVector[interfaceVertIdx].count==2)
                {
                    outfile << outputVector[interfaceVertIdx].xCoord << " "
                            << outputVector[interfaceVertIdx].defect[massBalanceIdx1] << " " // total mass flux
                            << outputVector[interfaceVertIdx].defect[transportEqIdx1] << " " // total flux of component
                            << outputVector[interfaceVertIdx].defect[momentumXIdx1] << " " // total flux of momentum X-direction
                            << outputVector[interfaceVertIdx].defect[momentumYIdx1] << " " // total flux of momentum Y-direction
                        //                        << outputVector[interfaceVertIdx].interfaceVertex << " "
                        //                        << outputVector[interfaceVertIdx].globalIdx << " "
                        //                        << outputVector[interfaceVertIdx].yCoord << " "
                            << outputVector[interfaceVertIdx].count << " " << std::endl;

                    sumVaporFluxes += outputVector[interfaceVertIdx].defect[massBalanceIdx1];
                }
            }
            outfile.close();
        }

        if (counter_ > 1)
        {
            fluxFile_ << this->timeManager().time() + this->timeManager().timeStepSize() << " "
                      << sumVaporFluxes << " " << advectiveVaporFlux << " " << diffusiveVaporFlux << " "
                      << advectiveVaporFlux-diffusiveVaporFlux << " ";
        }
        return;
    }

    /*
     * \brief Calculates fluxes and coupling terms at the interface for the Darcy model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateSecondInterfaceFluxes()
    {
        const MultiDomainGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables2 elemVolVarsPrev2, elemVolVarsCur2;

        Scalar sumVaporFluxes = 0.;
        Scalar sumWaterFluxInGasPhase = 0.;

        // count number of elements to determine number of interface nodes
        int numElements = 0;
        const SubDomainInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(mortarStokes2cDrops_, mortarTwoPTwoCDrops_);
        for (SubDomainInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(mortarStokes2cDrops_, mortarTwoPTwoCDrops_); ifIt != endIfIt; ++ifIt)
            numElements++;

        const int numInterfaceVertices = numElements + 1;
        std::vector<InterfaceFluxes<numEq2> > outputVector(numInterfaceVertices); // vector for the output of the fluxes
        FVElementGeometry2 fvGeom2;
        int interfaceVertIdx = -1;

        // loop over the element faces on the interface
        for (SubDomainInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(mortarStokes2cDrops_, mortarTwoPTwoCDrops_); ifIt != endIfIt; ++ifIt)
        {
            const int secondFaceIdx = ifIt->indexInSecondCell();
            MultiDomainElementPointer mdElementPointer2 = ifIt->secondCell();// ATTENTION!!!
            const MultiDomainElement& mdElement2 = *(mdElementPointer2);
            const SubDomainElementPointer sdElementPointer2 = mdGrid.subDomain(mortarTwoPTwoCDrops_).subDomainEntityPointer(mdElement2);
            const SubDomainElement2& sdElement2 = *sdElementPointer2;
            fvGeom2.update(this->sdGridView2(), sdElement2);

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement2.size(secondFaceIdx, 1, dim);

            // evaluate residual of the sub model without boundary conditions
            this->localResidual2().evalNoBoundary(sdElement2,fvGeom2,
                                                  elemVolVarsPrev2, elemVolVarsCur2);

            // evaluate the vapor fluxes within each phase
            this->localResidual2().evalPhaseFluxes();

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem2 = referenceElement2.subEntity(secondFaceIdx, 1, nodeInFace, dim);
                const DimVector& vertexGlobal = mdElement2.geometry().corner(vertInElem2);
                const int secondGlobalIdx = this->mdVertexMapper().map(mortarTwoPTwoCDrops_, mdElement2, vertInElem2, dim);
                const ElementSolutionVector2& secondVertexDefect = this->localResidual2().residual();

                bool existing = false;
                // loop over all interface vertices to check if vertex id is already in stack
                for (int interfaceVertex=0; interfaceVertex < numInterfaceVertices; ++interfaceVertex)
                {
                    if (secondGlobalIdx == outputVector[interfaceVertex].globalIdx)
                    {
                        existing = true;
                        interfaceVertIdx = interfaceVertex;
                        break;
                    }
                }

                if (!existing)
                    interfaceVertIdx++;

                if (shouldWriteFluxFile())
                {
                    outputVector[interfaceVertIdx].interfaceVertex = interfaceVertIdx;
                    outputVector[interfaceVertIdx].globalIdx = secondGlobalIdx;
                    outputVector[interfaceVertIdx].xCoord = vertexGlobal[0];
                    outputVector[interfaceVertIdx].yCoord = vertexGlobal[1];
                    for (int eqIdx=0; eqIdx < numEq2; ++eqIdx)
                        outputVector[interfaceVertIdx].defect[eqIdx] += secondVertexDefect[vertInElem2][eqIdx];
                    outputVector[interfaceVertIdx].count += 1;
                }
                if (!existing) // add phase storage only once per vertex
                    sumWaterFluxInGasPhase +=
                        this->localResidual2().evalPhaseStorage(vertInElem2);

//                sumVaporFluxes += secondVertexDefect[vertInElem2][transportEqIdx];
                sumWaterFluxInGasPhase +=
                    this->localResidual2().elementFluxes(vertInElem2);
            }
        }

        if (shouldWriteFluxFile())
        {
            char outputname[20];
            sprintf(outputname, "%s%05d%s","fluxes2_", counter_,".out");
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Xcoord2 "
                    << "totalFlux2 "
                    << "componentFlux2 "
                    << "count2 "
                    << std::endl;

            for (int interfaceVertIdx=0; interfaceVertIdx < numInterfaceVertices; interfaceVertIdx++)
            {
                //            if (outputVector[interfaceVertIdx].count == 1)
                //            {
                //                outputVector[interfaceVertIdx].rhoV *= 2;
                //                outputVector[interfaceVertIdx].normalForce *= 2;
                //            }
                if (outputVector[interfaceVertIdx].count > 2)
                    std::cerr << "too often at one node!!";

                if (outputVector[interfaceVertIdx].count==2)
                {
                    outfile << outputVector[interfaceVertIdx].xCoord << " "
                            << outputVector[interfaceVertIdx].defect[contiTotalMassIdx2] << " " // total mass flux
                            << outputVector[interfaceVertIdx].defect[transportEqIdx] << " " // total flux of component
                        //                        << outputVector[interfaceVertIdx].interfaceVertex << " "
                        //                        << outputVector[interfaceVertIdx].globalIdx << " "
                        //                        << outputVector[interfaceVertIdx].yCoord << " "
                            << outputVector[interfaceVertIdx].count << " "
                            << std::endl;

                    sumVaporFluxes += outputVector[interfaceVertIdx].defect[contiTotalMassIdx2];
                }
            }
            outfile.close();
        }

        if (counter_ > 1){
            Scalar sumWaterFluxInLiquidPhase = sumVaporFluxes - sumWaterFluxInGasPhase;
            fluxFile_ << sumVaporFluxes << " "
                      << sumWaterFluxInLiquidPhase << " "
                      << sumWaterFluxInGasPhase << std::endl;
        }
        return;
    }

    Scalar computeAdvectiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        Scalar advFlux = elemVolVars1[vertInElem1].density() *
            elemVolVars1[vertInElem1].fluidState().massFraction(phaseIdx, transportCompIdx1) *
            boundaryVars1.normalVelocity();
        return advFlux;
    }

    Scalar computeDiffusiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const BoundaryVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        Scalar diffFlux = boundaryVars1.moleFractionGrad(transportCompIdx1) *
            boundaryVars1.face().normal *
            boundaryVars1.diffusionCoeff(transportCompIdx1) *
            boundaryVars1.molarDensity() *
            FluidSystem::molarMass(transportCompIdx1);
        return -diffFlux;
    }

    /*!
     * \brief Returns true if a restart file should be written to disk.
     */
    bool shouldWriteRestartFile() const
    {
        const int freqRestart = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
        if ((this->timeManager().timeStepIndex() > 0 &&
             (this->timeManager().timeStepIndex() % freqRestart == 0))
            // also write a restart file at the end of each episode
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }


    /*!
     * \brief Returns true if a VTK output file should be written.
     */
    bool shouldWriteOutput() const
    {
        const int freqOutput = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        if (this->timeManager().timeStepIndex() % freqOutput == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }

    /*!
     * \brief Returns true if a file with the fluxes across the
     * free-flow -- porous-medium interface should be written.
     */
    bool shouldWriteFluxFile() const
    {
        const int freqFluxOutput = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqFluxOutput);
        if (this->timeManager().timeStepIndex() % freqFluxOutput == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }

    /*!
     * \brief Returns true if the summarized vapor fluxes
     *        across the free-flow -- porous-medium interface,
     *        representing the evaporation rate (related to the
     *        interface area), should be written.
     */
    bool shouldWriteVaporFlux() const
    {
        const int freqVaporFluxOutput = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqVaporFluxOutput);
        if (this->timeManager().timeStepIndex() % freqVaporFluxOutput == 0
            || this->timeManager().episodeWillBeOver())
            return true;
        return false;

    }

    Stokes2cFuelCellDropsSubProblem& stokes2cDropsProblem()
    { return this->sdProblem1(); }
    const Stokes2cFuelCellDropsSubProblem& stokes2cDropsProblem() const
    { return this->sdProblem1(); }

    TwoPTwoCFuelCellDropsSubProblem& twoPtwoCDropsProblem()
    { return this->sdProblem2(); }
    const TwoPTwoCFuelCellDropsSubProblem& twoPtwoCDropsProblem() const
    { return this->sdProblem2(); }

private:
    typename MultiDomainGrid::SubDomainType mortarStokes2cDrops_;
    typename MultiDomainGrid::SubDomainType mortarTwoPTwoCDrops_;

    int counter_;
    Scalar episodeLength_;
    Scalar initializationTime_;

    template <int numEquations>
    struct InterfaceFluxes
    {
        int count;
        int interfaceVertex;
        int globalIdx;
        Scalar xCoord;
        Scalar yCoord;
        Dune::FieldVector<Scalar, numEquations> defect;

        InterfaceFluxes()
        {
            count = 0;
            interfaceVertex = 0;
            globalIdx = 0;
            xCoord = 0.0;
            yCoord = 0.0;
            defect = 0.0;
        }
    };
    std::ofstream fluxFile_;
    Dune::shared_ptr<VtkMultiWriter> resultWriter_;

};

} //end namespace

#endif
