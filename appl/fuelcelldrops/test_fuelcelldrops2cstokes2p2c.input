#############################################################
#Configuration file for test_mortarecstokes2p2cdrops
#############################################################

#############################################################
#Runtime parameters
#############################################################

#############################################################
[TimeManager]
#############################################################

#Simulated time
TEnd= 1.e4

#Initial time step 
DtInitial = 0.5 #5e-1#0.1

MaxTimeStepSize = 10#180#0.1 1e3

ReducedTimeStepSize = 0.5

PrintProperties = false
PrintParameters = false

InitTime = -864 
ParameterFile = "test_fuelcelldrops2cstokes2p2c.input"
#Restart

#############################################################
[Grid]
#############################################################

#Name of the grid dgf
#File ="grids/interfacedomain.dgf"
File ="grids/test_fuelcelldrops2cstokes2p2c.dgf"

#domain extension for homogeneous, one-sided problem
UpperRightX = 5.1e-3
UpperRightY = 1.5e-3
#UpperRightZ

# number of cells for homogeneous, one-sided problem
NumberOfCellsX = 51 #51#102#150
NumberOfCellsY = 15 #15#30#45
NumberOfCellsZ = 1

#############################################################
#additional self defined parameters

UseInterfaceMeshCreator = true 

#Grading of the mesh
Grading = 1.13#99#13#1.25
Refinement = 0
RefineTop = true

#for one-sided problem
InterfacePos = 3e-4#25
RunUpDistanceX = 0.0#-0.02

#############################################################
#Runtime parameters with default properties
#############################################################

#############################################################
[Problem]
#############################################################

#EnableGravity = false

#############################################################
#additional self defined parameters (do not have default properties)
#############################################################
[Problem.FF]
#############################################################
InitialPressure = 2.5e5 #Gurau&Mann(2010)
InitialTemperature = 343 #Gurau&Mann(2010)
InitialMassFraction = 0.005 #0.005
InitialvxMax = 6.0 #Hu et al 2004 6
Initialvy = 0.0#-0.001
BeaversJosephSlipVel = 0.0#1e-5 
SinusVelVar = 0.0
DirichletMassFraction = 0.005

#############################################################
[Problem.PM]
#############################################################
InitialPressure = 2.5e5 #gas pressure - guess
InitialTemperature = 343
InitialSw = 0.1#0.5 #Sw is Swater
Sw = 0.1#0.5 #Sw is Swater
Source = 200#420#600

############################################################
#self defined parameters (do not have default properties)
[SpatialParams] 
############################################################

xMaterialInterface = 100#0.5

#Used in the homogeneous case
PermeabilityX = 7.5e-12 #Gurau&Mann(2010)
PermeabilityY = 3.9e-11 #Gurau&Mann(2010)
Porosity = 0.8 #Gurau&Mann(2010)
SwetR = 0.0#0.2 #Gurau&Mann(2010)
SnonwetR = 0.0#0.2 #Gurau&Mann(2010)
VgAlpha = 6.66578e-5 #Gostick et al.
VgN = 3.652 #Gostick et al.
Pentry = 6260 #Gurau&Mann(2010)
BCLambda = 0.05 #Gurau&Mann(2010) die geben das negativ an?
PlotMaterialLaw = false 

#############################################################
[Stokes]
#############################################################

StabilizationAlpha = -1.0#-10
StabilizationBeta = 0.0

#############################################################
#additional self defined parameters (do not have default properties)

alphaBJ = 1.0

#############################################################
[FuelCell]
#############################################################
Theta = 130 #contact angle (Promo Chaozhong Chap. 8 )
ContactAngleHysteresis = 20 #Cho et al 2012 (should be calculated dependent on velocity etc see Kumbur et al.)
TubeModelThickness = 18e-6 #GDL thickness = 250e-6m / 7 fiber layers / 2

NodesPerDrop = 52 #52#103#151 #number of drops
SimpleDragForce = true
#############################################################
[Newton]
#############################################################

RelTolerance = 1e-5#5e-7
TargetSteps = 8#10
MaxSteps = 12#15
WriteConvergence = false
MaxTimeStepDivisions = 20

#EnableRelativeCriterion
#EnableAbsoluteCriterion
#SatisfyAbsAndRel
#AbsTolerance
UseLineSearch = 1
#EnableChop

#############################################################
[LinearSolver]
#############################################################

ResidualReduction = 1e-10
Verbosity = 0
MaxIterations = 200
#PreconditionerRelaxation
#PreconditionerIterations
#GMResRestart

#############################################################
[Implicit]
#############################################################

MassUpwindWeight = 1
#EnableHints
#EnableJacobianRecycling
#EnablePartialReassemble
#EnableSmoothUpwinding
#NumericDifferenceMethod

############################################################
[VTK]
############################################################
#Simulation name for output of the FF subdomain
NameFF = mortarstokes2cdrops
#Simulation name for output of the PM subdomain
NamePM = mortar2p2cdrops

#OutputLevel
#AddVelocity
#AddTemperatures
#AddEnthalpies
#AddInternalEnergies
#AddFugacities
#AddPorosity
#AddBoundaryTypes
#AddSaturations
#AddPressures
#AddVelocities
#AddDensities
#AddMobilities
#AddAverageMolarMass
#AddMassFractions
#AddMoleFractions
#AddMolarities

#############################################################
#self defined parameters (do not have default properties)
[Output]
#############################################################


#Define the length of an episode (for output)
EpisodeLength = 43200

#Frequency of restart file, flux and VTK output
FreqRestart = 500  # how often restart files are written out 
FreqOutput = 1#10 # frequency of VTK output
FreqMassOutput = 1 #5 # frequency of mass and evaporation rate output (Darcy)
FreqFluxOutput = 1 #5 # frequency of detailed flux output
FreqVaporFluxOutput = 1 # frequency of summarized flux output
