/*****************************************************************************
 *   Copyright (C) 2011 by Katherina Baber                                   *
 *   Copyright (C) 2010 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version, as long as this copyright notice    *
 *   is included in its original form.                                       *
 *                                                                           *
 *   This program is distributed WITHOUT ANY WARRANTY.                       *
 *****************************************************************************/
#ifndef DUMUX_NCNISTOKES2P2CNIFUELCELLDROPS_SPATIALPARAMETERS_HH
#define DUMUX_NCNISTOKES2P2CNIFUELCELLDROPS_SPATIALPARAMETERS_HH

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedacosta.hh>
#include <dumux/material/fluidmatrixinteractions/2p/philtophoblaw.hh>//efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/io/plotfluidmatrixlaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams, SpatialParams,
        NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
//    typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
    typedef RegularizedAcosta<Scalar> EffMaterialLaw;
//    typedef LinearMaterial<Scalar> EffMaterialLaw;
public:
    // define the material law parameterized by absolute saturations
    typedef EffToAbsLaw<EffMaterialLaw> type;
//    typedef PhilToPhobLaw<EffMaterialLaw> type;
};
}


/** \todo Please doc me! */
template<class TypeTag>
class NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;
    typedef Dune::FieldMatrix<CoordScalar, dim, dim> DimMatrix;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

//    typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
//    typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
//public:
////    typedef EffToAbsLaw<EffMaterialLaw> MaterialLaw;
//    typedef PhilToPhobLaw<EffMaterialLaw> MaterialLaw;
//    typedef typename MaterialLaw::Params MaterialLawParams;
public:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;

    NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams(const GridView& gridView)
        : ParentType(gridView)
    {
        try
        {
            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, xMaterialInterface);
            // porosities
            porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);

            // intrinsic permeabilities
            permeability_[0][1] = 0.0;
            permeability_[1][0] = 0.0;
            permeability_[0][0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, PermeabilityX);
            permeability_[1][1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, PermeabilityY);

            // residual saturations - CAREFULL: INCONSISTENT NAMING
            params_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SwetR)); //here Swr is the residual saturation of the wetting phase: gas for hydrophobic material
            params_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SnonwetR)); //here Snr is the residual saturation of the non-wetting phase: water for hydrophobic material

            //parameters for the vanGenuchten law
//            params_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgAlpha));
//            params_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, VgN));
            //parameters for the linear material law
//            params_.setEntryPc(0.);
//            params_.setMaxPc(10000.);
//            params_.setEntryPc(500.);
//            params_.setMaxPc(5000.);
            //parameters for the Acosta saturation capillary pressure relation
            params_.setAcA(-1168.75); //imbition -1168.75; drainage -600;
            params_.setAcB(8.5); //imbition 8.5; drainage 25;
            params_.setAcC(-0.2); //imbition -0.2; drainage -16;
            params_.setAcD(-700); //imbition -700; drainage -3300;
            params_.setAcE(0.0); //imbition 0; drainage 800;
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    ~NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams()
    {}

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element       The current finite element
     * \param fvGeometry    The current finite volume geometry of the element
     * \param scvIdx       The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    DimMatrix intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        return permeability_;
    }

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        return porosity_;
    }


    /*!
     * \brief return the brooks-corey context depending on the position
     *
    * \param element The current finite element
    * \param fvGeometry The current finite volume geometry of the element
    * \param faceIdx The index sub-control volume face where the
    *                      intrinsic velocity ought to be calculated.
    */
    const MaterialLawParams& materialLawParams(const Element &element,
                                                const FVElementGeometry &fvGeometry,
                                                const int scvIdx) const
    {
        return params_;
    }
    
    Scalar heatCapacity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return
        710// specific heat capacity of granite [J / (kg K)]
        * 1430 // density of granite [kg/m^3]
        * (1 - porosity(element, fvGeometry, scvIdx));
    }

    Scalar thermalConductivitySolid(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        return 15.6;
    }


    // this is called from the coupled problem
    // and creates a gnuplot output of the Pc-Sw curve
    /*!
     * \brief docme
     */
    void plotMaterialLaw()
    {
        if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, PlotMaterialLaw))
        {
            PlotFluidMatrixLaw<MaterialLaw> coarseFluidMatrixLaw_;
            coarseFluidMatrixLaw_.plotpC(params_,
                    GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SwetR),
                    1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SnonwetR));
//            coarseFluidMatrixLaw_.plotkrw(params_,
//                    GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SwetR),
//                    1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SnonwetR));
//            coarseFluidMatrixLaw_.plotkrn(params_,
//                    GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SwetR),
//                    1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, SnonwetR));
        }
    }
private:
    DimMatrix permeability_;
    Scalar porosity_;

    Scalar xMaterialInterface_;
    MaterialLawParams params_;
};

} // end namespace
#endif

