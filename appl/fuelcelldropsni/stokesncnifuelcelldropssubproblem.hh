/*****************************************************************************
 *   Copyright (C) 2009-2012 by Katherina Baber                              *
 *   Copyright (C) 2007-2008 by Bernd Flemisch                               *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version, as long as this copyright notice    *
 *   is included in its original form.                                       *
 *                                                                           *
 *   This program is distributed WITHOUT ANY WARRANTY.                       *
 *****************************************************************************/
/**
 * @file
 * \ingroup Stokes2cProblems
 * @brief  Definition of a non-isothermal compositional Stokes problem using the mortar coupling
 * @author Klaus Mosthaf
 */
#ifndef DUMUX_STOKESNCNIFUELCELLDROPS_SUBPROBLEM_HH
#define DUMUX_STOKESNCNIFUELCELLDROPS_SUBPROBLEM_HH

#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dumux/multidomain/couplinglocalresiduals/stokesncnicouplinglocalresidual.hh>
#include <dumux/freeflow/stokesncni/stokesncnimodel.hh>

namespace Dumux
{

template <class TypeTag>
class StokesncniFuelCellDropsSubProblem;

//////////
// Specify the properties for the Stokes problem
//////////
namespace Properties
{
NEW_TYPE_TAG(StokesncniFuelCellDropsSubProblem,
	INHERITS_FROM(BoxStokesncni, SubDomain, NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams));

//SET_PROP(StokesncniFuelCellDropsSubProblem2, ModelParameterGroup)
//{ static const char *value() { return "FF"; }; };

// Set the problem property
SET_TYPE_PROP(StokesncniFuelCellDropsSubProblem, Problem, Dumux::StokesncniFuelCellDropsSubProblem<TypeTag>);

/*!
 * \brief Set the property for the material parameters by extracting
 *        it from the material law.
 */
SET_PROP(StokesncniFuelCellDropsSubProblem, MaterialLawParams)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
public:
    typedef typename MaterialLaw::Params type;
};

//! Use the Stokes2cCouplingLocalResidual for the computation of the local residual in the Stokes domain
SET_TYPE_PROP(StokesncniFuelCellDropsSubProblem,
              LocalResidual,
              StokesncniCouplingLocalResidual<TypeTag>);

// set the grid operator
SET_PROP(StokesncniFuelCellDropsSubProblem, GridOperator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ConstraintsTrafo) ConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dumux::PDELab::MultiDomainLocalOperator<TypeTag> LocalOperator;

    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};
public:
    typedef Dune::PDELab::GridOperator<GridFunctionSpace,
            GridFunctionSpace, LocalOperator,
            Dune::PDELab::ISTLBCRSMatrixBackend<numEq, numEq>,
            Scalar, Scalar, Scalar,
            ConstraintsTrafo, ConstraintsTrafo,
            true> type;
};

// set the local operator used for sub-models
SET_PROP(StokesncniFuelCellDropsSubProblem, LocalOperator)
{ typedef Dumux::PDELab::MultiDomainLocalOperator<TypeTag> type; };
//
// set the grid functions space for the sub-models
SET_PROP(StokesncniFuelCellDropsSubProblem, ScalarGridFunctionSpace)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, LocalFEMSpace) FEM;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Constraints) Constraints;
public:
    typedef Dune::PDELab::GridFunctionSpace<GridView, FEM, Constraints, Dune::PDELab::ISTLVectorBackend<1> > type;
};

SET_TYPE_PROP(StokesncniFuelCellDropsSubProblem, Constraints, Dune::PDELab::NonoverlappingConformingDirichletConstraints);

SET_PROP(StokesncniFuelCellDropsSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};

//! Set Scalar to type long double for higher accuracy
SET_TYPE_PROP(StokesncniFuelCellDropsSubProblem, Scalar, double);

SET_PROP(StokesncniFuelCellDropsSubProblem, PhaseIdx)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    static const int value = FluidSystem::nPhaseIdx;
};

// Disable gravity in the Stokes domain
SET_BOOL_PROP(StokesncniFuelCellDropsSubProblem, ProblemEnableGravity, false);

// switch inertia term on or off
SET_BOOL_PROP(StokesncniFuelCellDropsSubProblem, EnableNavierStokes, false);

// use mass fractions
SET_BOOL_PROP(StokesncniFuelCellDropsSubProblem, UseMoles, false);
}

/*!
 * \ingroup Stokes2cBoxProblems
 * \brief Stokes2c problem with air flowing
 *        from the left to the right.
 *
 * The stokes subdomain is sized 1m times 1m. The boundary conditions for the momentum balances
 * are all set to Dirichlet. The mass balance receives
 * outflow bcs, which are replaced in the localresidual by the sum
 * of the two momentum balances. In the middle of the right boundary,
 * one vertex receives Dirichlet bcs, to set the pressure level.
 *
 * This sub problem uses the \ref Stokes2cModel. It is part of the 2cnistokes2p2cni model and
 * is combined with the 2p2cnisubproblem for the Darcy domain.
 *
 */
template <class TypeTag>
class StokesncniFuelCellDropsSubProblem : public StokesProblem<TypeTag>
{
    typedef StokesncniFuelCellDropsSubProblem<TypeTag> ThisType;
    typedef StokesProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    // soil parameters for beavers & joseph
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    enum {
        // Number of equations and grid dimension
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        dim = GridView::dimension
    };
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { // equation indices
        massBalanceIdx = Indices::massBalanceIdx,

        momentumXIdx = Indices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, //!< Index of the z-component of the momentum balance

        transportEqIdx = Indices::transportEqIdx, //!< Index of the transport equation (massfraction)
        energyEqIdx = Indices::energyEqIdx //!< Index of the energy equation
    };
    enum { // primary variable indices
        pressureIdx = Indices::pressureIdx,
        velocityXIdx = Indices::velocityXIdx,
        velocityYIdx = Indices::velocityYIdx,
        velocityZIdx = Indices::velocityZIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum {
        phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx),
        transportCompIdx = Indices::transportCompIdx,
        phaseCompIdx = Indices::phaseCompIdx
    };
    enum { numComponents = Indices::numComponents };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;

public:
    StokesncniFuelCellDropsSubProblem(TimeManager &timeManager, const GridView gridView)
        : ParentType(timeManager, gridView),
          spatialParams_(gridView)
    {
        try
        {
            bboxMin_[0] = 0.0;
            bboxMax_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX);

            bboxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            bboxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY);

            refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialTemperature);
            refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialPressure);
            refMassfrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialMassFraction);
            vxMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialvxMax);
            bjSlipVel_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, BeaversJosephSlipVel);
            sinusVelVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, SinusVelVar);

            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, xMaterialInterface);
            runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX); // first part of the interface without coupling
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

            alphaBJ_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Stokes, alphaBJ);
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, VTK, NameFF); }

//    /*!
//      * \brief Returns the temperature within the domain.
//      *
//      */
//     Scalar temperature() const
//     {
//         return refTemperature_; // -> 36 degrees Celsius
//     };
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex (entity) on the boundary for which the
     *               conditions needs to be specified
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        const Scalar time = this->timeManager().time();

        values.setAllDirichlet();

        if (onUpperBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setDirichlet(energyEqIdx);
        }

        if (onLeftBoundary_(globalPos))
        {
            values.setDirichlet(transportEqIdx);
            values.setDirichlet(energyEqIdx);

            if (onUpperBoundary_(globalPos)) // corner point
                values.setAllDirichlet();
        }

        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();
//            values.setOutflow(transportEqIdx);

            if (onUpperBoundary_(globalPos)) // corner point
                values.setAllDirichlet();
        }

//        //mass balance has to be set to outflow everywhere
        // it does not get a coupling condition, since pn is a condition for stokes
//        values.setOutflow(massBalanceIdx);

        if (onLowerBoundary_(globalPos))
        {
            values.setAllMortarCoupling();
            values.setDirichlet(momentumXIdx);
//            values.setMortarCoupling(transportEqIdx);
////            values.setNeumann(transportEqIdx);
//            values.setNeumann(energyEqIdx);
//
//            if (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos))
//                values.setAllDirichlet();
////            if(globalPos[0] > runUpDistanceX_)/ && !onRightBoundary_(globalPos))
////            {
//                values.setAllMortarCoupling();
//                values.setDirichlet(momentumXIdx);
//                values.setNeumann(energyEqIdx);
////            }
////            else
////            {
////                values.setAllDirichlet();
////
////                if(onRightBoundary_(globalPos))
////                    values.setOutflow(transportEqIdx);
////                else
////                    values.setNeumann(transportEqIdx);
////            }
        }

        //mass balance has to be set to outflow everywhere
        // it does not get a coupling condition, since pn is a condition for stokes
        values.setOutflow(massBalanceIdx);


//         if (onUpperBoundary_(globalPos))
//         {
//             values.setNeumann(massBalanceIdx);
//             values.setNeumann(transportEqIdx);
//
//            if (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)) // corner points
//                values.setDirichlet(transportEqIdx);
//         }

         if (onRightBoundary_(globalPos))// && !onUpperBoundary_(globalPos))// &&
          {
              if (!onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
                  values.setDirichlet(pressureIdx, massBalanceIdx);
          }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        sub-control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex representing the "half volume on the boundary"
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        initial_(values, globalPos);

        values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.0;
        values[pressureIdx] = refPressure_;
        values[massOrMoleFracIdx] = refMassfrac_;
        values[temperatureIdx] = refTemperature_;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos = is.geometry().center();
        //const GlobalPosition &vertexPos = element.geometry().corner(scvIdx);

        values = 0.;

        FluidState fluidState;
        updateFluidStateForBC_(fluidState);

        const Scalar density =
                FluidSystem::density(fluidState, phaseIdx);
        const Scalar enthalpy =
                FluidSystem::enthalpy(fluidState, phaseIdx);
        const Scalar xVelocity = xVelocity_(globalPos);

        if (onLeftBoundary_(globalPos)
                     && !onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
        {
            values[transportEqIdx] = -xVelocity*density*refMassfrac_;
            values[energyEqIdx] = -xVelocity*density*enthalpy;
        }
    }

    /*!
     * \brief Evaluate the Beavers-Joseph coefficient
     *        at the center of a given intersection
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeff(const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        // not used in mortar coupling
        if (onLowerBoundary_(globalPos))
        {
                return alphaBJ_;
        }
        return 0.0;
    }

    Scalar beaversJosephCoeff(const Element &element,
                 const FVElementGeometry &fvGeometry,
                // const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        if (onLowerBoundary_(globalPos))
        {
                return alphaBJ_;
        }
        else
            return 0.0;
    }
    /*!
     * \brief Evaluate the intrinsic permeability
     *        at the corner of a given element
     *
     * \return permeability in x-direction
     */
    Scalar permeability(const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        return spatialParams_.intrinsicPermeability(element,
                                                    fvGeometry,
                                                    scvIdx)[0][0];
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &,
                const int scvIdx) const
    {
        // ATTENTION: The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos
            = element.geometry().corner(scvIdx);

        initial_(values, globalPos);
    }
    // \}

    /*!
     * \brief Determine if we are on a corner
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;

    }

    /*!
     * \brief Returns the spatial parameters object.
     */
    SpatialParams &spatialParameters()
    { return spatialParams_; }
    const SpatialParams &spatialParameters() const
    { return spatialParams_; }

    const Scalar refPressure() const
    { return refPressure_; }

    const bool fixPressureAtPos(const GlobalPosition &globalPos) const
    {
        if (globalPos[0] < 0.226 && globalPos[0] > 0.224
                && globalPos[1] < 0.476 && globalPos[1] > 0.474)
            return true;
        else
            return false;
    }

    //necessary for calculation of boundary fluxes at corners of the coupling interface
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    {
       if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
           (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)))
           return true;
       else
           return false;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bboxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bboxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bboxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bboxMax_[1] - eps_; }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[velocityXIdx] = xVelocity_(globalPos); //GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, Initialvx);//xVelocity_(globalPos);
        values[velocityYIdx] = 0.;//GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, Initialvy);//0.;
        values[pressureIdx] =  refPressure_;// + 1.189*this->gravity()[1]*(globalPos[1] - bboxMin_[1]);
        values[massOrMoleFracIdx] = refMassfrac_;
        values[temperatureIdx] = refTemperature_;
    }

    const Scalar xVelocity_(const GlobalPosition &globalPos) const
        {
            // const Scalar relativeHeight = (globalPos[1]-bboxMin_[1])/height_();
            // linear profile
    //        return vmax*relativeHeight + bjSlipVel_; // BJ slip velocity is added as sqrt(Kxx)
            // parabolic profile
            return  vxMax_*(globalPos[1] - bboxMin_[1])*(bboxMax_[1] - globalPos[1])
                    / (0.25*height_()*height_()) + bjSlipVel_;
            // logarithmic profile
    //        return 0.1*vmax*log((relativeHeight+1e-3)/1e-3) + bjSlipVel_;
        }

    void updateFluidStateForBC_(FluidState& fluidState) const
    {
        fluidState.setTemperature(refTemperature());
        fluidState.setPressure(phaseIdx, refPressure_);

        Scalar massFraction[numComponents];
        massFraction[transportCompIdx] = refMassfrac();
        massFraction[phaseCompIdx] = 1 - massFraction[transportCompIdx];

        // calculate average molar mass of the gas phase
        Scalar M1 = FluidSystem::molarMass(transportCompIdx);
        Scalar M2 = FluidSystem::molarMass(phaseCompIdx);
        Scalar X2 = massFraction[phaseCompIdx];
        Scalar massToMoleDenominator = M2 + X2*(M1 - M2);

        fluidState.setMoleFraction(phaseIdx, transportCompIdx, massFraction[transportCompIdx]*M2/massToMoleDenominator);
        fluidState.setMoleFraction(phaseIdx, phaseCompIdx, massFraction[phaseCompIdx]*M1/massToMoleDenominator);
    }
    const Scalar refTemperature() const
    { return refTemperature_; }

    const Scalar refMassfrac() const
    { return refMassfrac_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    const Scalar height_() const
    { return bboxMax_[1] - bboxMin_[1]; }

    // spatial parameters
    SpatialParams spatialParams_;

    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bboxMin_;
    GlobalPosition bboxMax_;

    Scalar refPressure_;
    Scalar refTemperature_;
    Scalar refMassfrac_;

    Scalar vxMax_;
    Scalar bjSlipVel_;
    Scalar sinusVelVar_;
    Scalar alphaBJ_;

    Scalar xMaterialInterface_;
    Scalar runUpDistanceX_;
    Scalar initializationTime_;
};
} //end namespace

#endif
