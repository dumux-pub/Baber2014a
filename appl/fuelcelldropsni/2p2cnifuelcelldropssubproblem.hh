/*****************************************************************************
 *   Copyright (C) 2009 by Katherina Baber                          *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version, as long as this copyright notice    *
 *   is included in its original form.                                       *
 *                                                                           *
 *   This program is distributed WITHOUT ANY WARRANTY.                       *
 *****************************************************************************/
#ifndef DUMUX_2P2CNIFUELCELLDROPS_SUBPROBLEM_HH
#define DUMUX_2P2CNIFUELCELLDROPS_SUBPROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dumux/implicit/2p2cni/2p2cnimodel.hh>
#include <dumux/multidomain/couplinglocalresiduals/2p2cnicouplinglocalresidual.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityfuelcell.hh>

namespace Dumux
{
template <class TypeTag>
class TwoPTwoCNIFuelCellDropsSubProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoPTwoCNIFuelCellDropsSubProblem,
	INHERITS_FROM(BoxTwoPTwoCNI, SubDomain, NCNIStokesTwoPTwoCNIFuelCellDropsSpatialParams));

// Set the problem property
SET_TYPE_PROP(TwoPTwoCNIFuelCellDropsSubProblem, Problem,
        Dumux::TwoPTwoCNIFuelCellDropsSubProblem<TTAG(TwoPTwoCNIFuelCellDropsSubProblem)>);

//! Use the 2p2cniCouplingLocalResidual for the computation of the local residual in the Darcy domain
SET_TYPE_PROP(TwoPTwoCNIFuelCellDropsSubProblem,
              LocalResidual,
              TwoPTwoCNICouplingLocalResidual<TypeTag>);

// choose pg and Sl as primary variables
SET_INT_PROP(TwoPTwoCNIFuelCellDropsSubProblem, Formulation, TwoPTwoCFormulation::pnsw);
// one component balance is replaced by the total mass balance
SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, ReplaceCompEqIdx)
{
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    static const int value = Indices::contiNEqIdx;
};

// Set the model parameter group for the TypeTag
//SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, ModelParameterGroup)
//{ static const char *value() { return "PM"; }; };

// set the grid operator
SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, GridOperator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ConstraintsTrafo) ConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dumux::PDELab::MultiDomainLocalOperator<TypeTag> LocalOperator;

    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};

public:
    typedef Dune::PDELab::GridOperator<GridFunctionSpace,
            GridFunctionSpace, LocalOperator,
            Dune::PDELab::ISTLBCRSMatrixBackend<numEq, numEq>,
            Scalar, Scalar, Scalar,
            ConstraintsTrafo,
            ConstraintsTrafo,
            true> type;
};

// set the local operator
SET_TYPE_PROP(TwoPTwoCNIFuelCellDropsSubProblem, LocalOperator,
        Dumux::PDELab::MultiDomainLocalOperator<TypeTag>);

// set the grid functions space for the sub-models
SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, ScalarGridFunctionSpace)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, LocalFEMSpace) FEM;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Constraints) Constraints;
    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};
public:
    typedef Dune::PDELab::GridFunctionSpace<GridView, FEM, Constraints,
            Dune::PDELab::ISTLVectorBackend<1> > type;
};

// set the constraints for multidomain / pdelab
SET_TYPE_PROP(TwoPTwoCNIFuelCellDropsSubProblem, Constraints,
        Dune::PDELab::NonoverlappingConformingDirichletConstraints);

// the fluidsystem is set in the coupled problem
SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};


//! Somerton is used as model to compute the effective thermal heat conductivity
SET_PROP(TwoPTwoCNIFuelCellDropsSubProblem, ThermalConductivityModel)
{ private :
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  public:
    typedef ThermalConductivityFuelCell<Scalar> type;
};

// enable/disable velocity output
SET_BOOL_PROP(TwoPTwoCNIFuelCellDropsSubProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(TwoPTwoCNIFuelCellDropsSubProblem, ProblemEnableGravity, false);

// use mass fractions
SET_BOOL_PROP(TwoPTwoCNIFuelCellDropsSubProblem, UseMoles, false);

}


template <class TypeTag = TTAG(TwoPTwoCNIFuelCellDropsSubProblem) >
class TwoPTwoCNIFuelCellDropsSubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    typedef TwoPTwoCNIFuelCellDropsSubProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    // the type tag of the coupled problem
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;

    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq) };
    enum { // the equation indices
        contiTotalMassIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),
        transportEqIdx = Indices::contiWEqIdx,
        energyEqIdx = Indices::energyEqIdx
    };
    enum { // the indices of the primary variables
		pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { // the indices for the phase presence
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };
    enum { // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    TwoPTwoCNIFuelCellDropsSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        try
        {
            bboxMin_[0] = 0.0;
            bboxMax_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX);

            bboxMin_[1] = 0.0;
            bboxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);

            runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX); // first part of the interface without coupling
            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, xMaterialInterface);
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

            refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, InitialTemperature);
            refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, InitialPressure);
            initialSw_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, InitialSw);
            sw_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, Sw);
            
            source_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, Source);
            storageLastTimestep_ = Scalar(0);
            lastMassOutputTime_ = Scalar(0);

            outfile.open("evaporation.out");
            outfile << "time; evaporationRate" << std::endl;
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    ~TwoPTwoCNIFuelCellDropsSubProblem()
    {
        outfile.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, VTK, NamePM); }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        // set the initial condition of the model
        ParentType::init();

        this->model().globalStorage(storageLastTimestep_);
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex for which the boundary type is set
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar time = this->timeManager().time();

        values.setAllNeumann();

        if(onUpperBoundary_(globalPos))// && globalPos[0] > runUpDistanceX_)// && !onRightBoundary_(globalPos))//!onLeftBoundary_(globalPos))
        {
            values.setAllMortarCoupling();
//            values.setNeumann(energyEqIdx);
        }

        if(onLowerBoundary_(globalPos))
            values.setDirichlet(temperatureIdx, energyEqIdx);
//////
////        if(onLowerBoundary_(globalPos))// && (globalPos[0] < eps_ || globalPos[0] > 0.25 - eps_))
////            values.setOutflow(contiTotalMassIdx);//values.setDirichlet(pressureIdx, contiTotalMassIdx);
//        if(onLowerBoundary_(globalPos))// && (globalPos[0] < eps_ || globalPos[0] > 0.25 - eps_))
//            values.setDirichlet(switchIdx, transportEqIdx);
//
//        if(onUpperBoundary_(globalPos))
//        {
//            values.setMortarCoupling(contiTotalMassIdx);
//            values.setMortarCoupling(transportEqIdx);
////            values.setOutflow(transportEqIdx);
//        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex for which the boundary type is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param is The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition globalPos = element.geometry().corner(scvIdx);
        values = 0.;

//        if(onLowerBoundary_(globalPos) && !onLeftBoundary_(globalPos) && !onRightBoundary_(globalPos))
//        {
//            //water comes in, air flows out
//            values[contiTotalMassIdx] = -4677;//3082;//-0.1272;//passt zu gradp = -480 Pa/m//-0.0080504;//-3e-5;//-0.00805;
//            values[transportEqIdx] = -4677;//-0.1272;//passt zu gradp = -480 Pa/m//-0.0080504;//-3e-5;// -0.00805;
//
////            values[contiTotalMassIdx] = -4.664e-4 + 4.146e-4;//kg/(m^2 s)
////            values[transportEqIdx] = -4.664e-4; //kg/(m^2 s)
//        }

        if(onLowerBoundary_(globalPos))
        {
            values[contiTotalMassIdx] = (-20.286 + 12.288) * 3.5e-5;//TIMO//- source_ * 3.5e-5;//kg/(m^2 s)
            values[transportEqIdx] = -20.286 * 3.5e-5;//TIMO//- source_ * 3.5e-5; //kg/(m^2 s)
//            values[energyEqIdx] = - 1.46e8 * 3.5e-5;//TIMO;//J/(sm^2)
        }
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {
        values = 0.;

//        const GlobalPosition globalPos = element.geometry().corner(scvIdx);
//        if(onLowerBoundary_(globalPos))
//        {
//            values[contiTotalMassIdx] = source_;
//            values[transportEqIdx] = source_;
//        }
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local vertex index
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        values = 0.;

        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vert The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vert,
                             const int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }

    void postTimeStep()
    { }

    //necessary for calculation of boundary fluxes at corners of the coupling interface
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    {
       if ((onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
           (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
           return true;
       else
           return false;

    }

    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }
    // \}
private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = refPressure_;//-4800*globalPos[1] + refPressure_+ 4800*bboxMax_[1];
        values[switchIdx] = initialSw_;
        values[temperatureIdx] = refTemperature_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bboxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bboxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bboxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bboxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
    	return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
    			|| onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    static constexpr Scalar eps_ = 1e-8;

    GlobalPosition bboxMin_;
    GlobalPosition bboxMax_;
    Scalar xMaterialInterface_;

    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;

    Scalar refTemperature_;
    Scalar refPressure_;
    Scalar initialSw_;
    Scalar sw_;
    Scalar source_;

    Scalar runUpDistanceX_;
    Scalar initializationTime_;
    std::ofstream outfile;
};
} //end namespace

#endif
