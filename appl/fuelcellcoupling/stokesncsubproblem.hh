/*****************************************************************************
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2007-2008 by Bernd Flemisch                               *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * @file
 * \ingroup StokesncProblems
 * @brief  Definition of an isothermal compositional Stokes problem
 * @author Timo Koch
 */
#ifndef DUMUX_STOKESNCSUBPROBLEM_HH
#define DUMUX_STOKESNCSUBPROBLEM_HH

//O: NON-ISOTHERMAL MODEL, 1: ISOTHERMAL MODEL defined in fuelcellcouplingproblem.hh

#if !ISOTHERMAL
#include <dumux/freeflow/stokesncni/stokesncnimodel.hh>
#include <dumux/multidomain/couplinglocalresiduals/stokesncnicouplinglocalresidual.hh>
#endif
#if ISOTHERMAL
#include <dumux/freeflow/stokesnc/stokesncmodel.hh>
#include <dumux/multidomain/couplinglocalresiduals/stokesnccouplinglocalresidual.hh>
#endif

namespace Dumux
{

template <class TypeTag>
class StokesNCSubProblem;

//////////
// Specify the properties for the Stokes problem
//////////
namespace Properties
{
#if ISOTHERMAL
NEW_TYPE_TAG(StokesNCSubProblem,
             INHERITS_FROM(BoxStokesnc, SubDomain, FuelCellCouplingSpatialParams));
#endif
#if !ISOTHERMAL
NEW_TYPE_TAG(StokesNCSubProblem,
			 INHERITS_FROM(BoxStokesncni, SubDomain, FuelCellCouplingSpatialParams));
#endif
	
// Set the problem property
SET_TYPE_PROP(StokesNCSubProblem, Problem, Dumux::StokesNCSubProblem<TypeTag>);

/*!
 * \brief Set the property for the material parameters by extracting
 *        it from the material law.
 */
SET_PROP(StokesNCSubProblem, MaterialLawParams)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
 public:
    typedef typename MaterialLaw::Params type;
};

//! Use the StokesncCouplingLocalResidual for the computation of the local residual in the Stokes domain
#if ISOTHERMAL
SET_TYPE_PROP(StokesNCSubProblem,
              LocalResidual,
              StokesncCouplingLocalResidual<TypeTag>);
#endif
#if !ISOTHERMAL
SET_TYPE_PROP(StokesNCSubProblem,
			  LocalResidual,
			  StokesncniCouplingLocalResidual<TypeTag>);
#endif
	
SET_TYPE_PROP(StokesNCSubProblem, Constraints,
        Dune::PDELab::NonoverlappingConformingDirichletConstraints);

SET_PROP(StokesNCSubProblem, GridOperator)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ConstraintsTrafo) ConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dumux::PDELab::MultiDomainLocalOperator<TypeTag> LocalOperator;

    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};
 public:
    typedef Dune::PDELab::GridOperator<GridFunctionSpace,
        GridFunctionSpace, LocalOperator,
        Dune::PDELab::ISTLBCRSMatrixBackend<numEq, numEq>,
        Scalar, Scalar, Scalar,
        ConstraintsTrafo, ConstraintsTrafo,
        true> type;
};

SET_PROP(StokesNCSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};
	
SET_PROP(StokesNCSubProblem, PhaseIdx)
{
private:
	typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
	typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
	static const int value = FluidSystem::nPhaseIdx;
};

// Disable gravity
SET_BOOL_PROP(StokesNCSubProblem, ProblemEnableGravity, false);

// switch inertia term on or off
SET_BOOL_PROP(StokesNCSubProblem, EnableNavierStokes, false);

//!< Define that mass fractions are used in the balance equations
SET_BOOL_PROP(StokesNCSubProblem, UseMoles, true);

}
	
/*!
 * \ingroup StokesncBoxProblems
 * \brief Stokesnc problem with air flowing
 *        from the left to the right.
 *
* This sub problem uses the \ref StokesncModel. It is part of the ncstokes2pnc model and
 * is combined with the 2pncsubproblem for the Darcy domain.
 *
 */
template <class TypeTag>
class StokesNCSubProblem : public StokesProblem<TypeTag>
{
    typedef StokesNCSubProblem<TypeTag> ThisType;
    typedef StokesProblem<TypeTag> ParentType;
    
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    // soil parameters for beavers & joseph
    typedef typename GET_PROP_TYPE(TypeTag, SpatialParams) SpatialParams;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
	typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
	
	enum {
		
        // Number of equations and grid dimension
        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
		
        // copy some indices for convenience
        massBalanceIdx = Indices::massBalanceIdx,
		
        momentumXIdx = Indices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, //!< Index of the z-component of the momentum balance
		energyEqIdx = dim + FluidSystem::numComponents,
        conti0EqIdx = Indices::conti0EqIdx,
				
		// primary variable indices
		pressureIdx = Indices::pressureIdx,
		velocityXIdx = Indices::velocityXIdx,
		velocityYIdx = Indices::velocityYIdx,
		velocityZIdx = Indices::velocityZIdx,
		H2OmoleFracIdx = Indices::massOrMoleFracIdx,
        N2moleFracIdx = Indices::massOrMoleFracIdx+1,
		O2moleFracIdx = Indices::massOrMoleFracIdx+2,
		temperatureIdx = energyEqIdx
		
		};
	
	typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;
	
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;

	
	enum {
		phaseIdx = Indices::phaseIdx,
		numComponents = FluidSystem::numComponents,
		transportCompIdx = Indices::transportCompIdx, //!< water component index
        phaseCompIdx = Indices::phaseCompIdx          //!< air component index
		
	};
    	

public:
    StokesNCSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView),
          spatialParams_(gridView)
    {
            try
            {
                bBoxMin_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMin);
                bBoxMax_[0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMax);
                bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
                bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, YMax);

                refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
                refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure);
                vxMax_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, VxMax);
                bjSlipVel_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, BeaversJosephSlipVel);
                sinusVelVar_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelVar);

                xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, MaterialInterfaceX);
                runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX); // first part of the interface without coupling
                initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

                alphaBJ1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ1);
                alphaBJ2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ2);
                alphaBJ3_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, AlphaBJ3);
				
				nTemperature_       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.NTemperature);
				nPressure_          = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.NPressure);
				pressureLow_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureLow);
				pressureHigh_       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureHigh);
				temperatureLow_     = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TemperatureLow);
				temperatureHigh_    = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TemperatureHigh);
				temperature_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitialTemperature);
				name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
				
				pO2Inlet_			= GET_RUNTIME_PARAM(TypeTag, Scalar, FuelCell.pO2Inlet); //set in input according to FreeFlow.refPressure
                initXH2Og_			= GET_RUNTIME_PARAM(TypeTag, Scalar, FreeFlow.InitialxH20g);

				
            }
            catch (Dumux::ParameterException &e) {
                std::cerr << e << ". Abort!\n";
                exit(1) ;
            }
            catch (...) {
                std::cerr << "Unknown exception thrown!\n";
                exit(1);
            }
		
		// initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
		
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Vtk, NameFF); }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    {
        return refTemperature_; 
    };

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex (entity) on the boundary for which the
     *               conditions needs to be specified
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
		const GlobalPosition &globalPos = 
        		vertex.geometry().center();
        const Scalar time = this->timeManager().time();
        
        values.setAllDirichlet();
        
        if (onUpperBoundary_(globalPos))
        {
            for(int eqIdx=conti0EqIdx; eqIdx < energyEqIdx; eqIdx++)
                if(eqIdx != massBalanceIdx)
                    values.setNeumann(eqIdx);
#if !ISOTHERMAL
            values.setNeumann(energyEqIdx);
#endif
        }
        // Left inflow boundaries should be Neumann, otherwise the
		// evaporative fluxes are much more grid dependent
        if (onLeftBoundary_(globalPos))
        {
#if ISOTHERMAL
        /*    for(int eqIdx=conti0EqIdx; eqIdx < energyEqIdx; eqIdx++)
                if(eqIdx != massBalanceIdx)
                    values.setNeumann(eqIdx);*/
#endif
            if (onUpperBoundary_(globalPos)) // corner point
                values.setAllDirichlet();
        }
        
        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();
            
            if (onUpperBoundary_(globalPos)) // corner point
                values.setAllDirichlet();
        }

        if (onLowerBoundary_(globalPos))
        {
			if(time>=initializationTime_)
			{
				values.setAllCouplingOutflow();
				values.setOutflow(massBalanceIdx);
			} 
			else 
			{ 
				values.setAllDirichlet();
			}

		}
        
        values.setOutflow(massBalanceIdx);
        
        if (onRightBoundary_(globalPos))
        {
            if (!onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
                values.setDirichlet(pressureIdx, massBalanceIdx);
        }

		/*
		//DEBUG Indices
		std::cout<<"Indices"<<std::endl;
		std::cout<<"numEq/soll5:  "<<numEq<<std::endl;
		std::cout<<"dim"<<dim<<std::endl;
		std::cout<<"massBalanceIdx"<<massBalanceIdx<<std::endl;
		std::cout<<"momentumXIdx"<<momentumXIdx<<std::endl;
		std::cout<<"momentumYIdx"<<momentumYIdx<<std::endl;
		std::cout<<"momentumZIdx"<<momentumZIdx<<std::endl;
		std::cout<<"pressureIdx"<<pressureIdx<<std::endl;
		std::cout<<"velocityXIdx"<<velocityXIdx<<std::endl;
		std::cout<<"velocityYIdx"<<velocityYIdx<<std::endl;
		std::cout<<"velocityZIdx"<<velocityZIdx<<std::endl;
		std::cout<<"H2OmoleFracIdx"<<H2OmoleFracIdx<<std::endl;
		std::cout<<"O2moleFracIdx"<<O2moleFracIdx<<std::endl;
		std::cout<<"temperatureIdx"<<temperatureIdx<<std::endl;
		std::cout<<"energyEqIdx"<<energyEqIdx<<std::endl;
		 */
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        sub-control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex representing the "half volume on the boundary"
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {

        const GlobalPosition globalPos = vertex.geometry().center();
        values = 0.0;

        initial_(values, globalPos);

        values[velocityXIdx] = xVelocity_(globalPos); //velocity profile
        values[velocityYIdx] = 0.0;
        values[pressureIdx] = refPressure_;
        values[H2OmoleFracIdx] = initXH2Og_;//H20
		values[O2moleFracIdx] = pO2Inlet_/refPressure_;//O2
#if !ISOTHERMAL
		values[temperatureIdx] = refTemperature_;
#endif
		
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =
        fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;
        
        values = 0.0;
        
        FluidState fluidState;
        updateFluidStateForBC_(fluidState);
        
        const Scalar density = FluidSystem::density(fluidState, phaseIdx);
        const Scalar molarDensity = fluidState.molarDensity(phaseIdx);
        const Scalar enthalpy = FluidSystem::enthalpy(fluidState, phaseIdx);
        const Scalar xVelocity = xVelocity_(globalPos);
        
        Scalar moleFraction[numComponents];
        moleFraction[H2OmoleFracIdx] = initXH2Og_;
        moleFraction[O2moleFracIdx] = pO2Inlet_/refPressure_;
        
        if (onLeftBoundary_(globalPos)
            && globalPos[1] > bBoxMin_[1] && globalPos[1] < bBoxMax_[1])
        {
#if ISOTHERMAL
        for(int eqIdx=conti0EqIdx; eqIdx < energyEqIdx; eqIdx++)
            if(eqIdx != massBalanceIdx)
                values[eqIdx] = -xVelocity*molarDensity*moleFraction[eqIdx-conti0EqIdx];
#endif
#if !ISOTHERMAL
            values[energyEqIdx] = -xVelocity*density*enthalpy;
#endif
        }
    }

    /*!
     * \brief Evaluate the Beavers-Joseph coefficient
     *        at the center of a given intersection
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeff(const Element &element,
                              const FVElementGeometry &fvGeometry,
                              const Intersection &is,
                              const int scvIdx,
                              const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =
                fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        if (onLowerBoundary_(globalPos))
			return alphaBJ2_;
        else
			return 0.0;
    }

    /*!
     * \brief Evaluate the intrinsic permeability
     *        at the corner of a given element
     *
     * \return permeability in x-direction
     */
    Scalar permeability(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const Intersection &is,
                        const int scvIdx,
                        const int boundaryFaceIdx) const
    {
        Tensor K = spatialParams_.intrinsicPermeability(element,
                                                        fvGeometry,
                                                        scvIdx);
        Scalar Kxx = K[0][0];
        return Kxx;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &,
                const int scvIdx) const
    {
        // ATTENTION: The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos
            = element.geometry().corner(scvIdx);

        initial_(values, globalPos);
    }
    // \}


    /*!
     * \brief Evaluate the intrinsic permeability
     *        at the corner of a given element
     *
     * \return permeability in x-direction
     */
    Scalar permeability(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        Tensor K = spatialParams_.intrinsicPermeability(element,
                                                        fvGeometry,
                                                        scvIdx);
        Scalar Kxx = K[0][0];
        return Kxx;
    }

    /*!
     * \brief Determine if we are on a corner of the grid
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }

    // required in case of mortar coupling
    // otherwise it should return false
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { return false; }

    /*!
     * \brief Returns the spatial parameters object.
     */
    SpatialParams &spatialParams()
    { return spatialParams_; }
    const SpatialParams &spatialParams() const
    { return spatialParams_; }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
		values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.;
        values[pressureIdx] = refPressure_;
        values[H2OmoleFracIdx] = initXH2Og_;
		values[O2moleFracIdx] = pO2Inlet_/refPressure_;
#if !ISOTHERMAL
		values[temperatureIdx] = refTemperature_;
#endif

    }

    const Scalar refTemperature() const
    { return refTemperature_; }
    
    const Scalar refPressure() const
    { return refPressure_; }
    
    void updateFluidStateForBC_(FluidState& fluidState) const
    {
        fluidState.setTemperature(refTemperature());
        fluidState.setPressure(phaseIdx, refPressure());
        
        fluidState.setMoleFraction(phaseIdx, H2OmoleFracIdx, initXH2Og_);
        fluidState.setMoleFraction(phaseIdx, O2moleFracIdx, pO2Inlet_/refPressure_);
        fluidState.setMoleFraction(phaseIdx, N2moleFracIdx, 1 - (pO2Inlet_/refPressure_) - initXH2Og_);
    }


    const Scalar xVelocity_(const GlobalPosition &globalPos) const
    {
        const Scalar vmax = vxMax_;
      
        // parabolic profile
        return  vmax*(globalPos[1] - bBoxMin_[1])*(bBoxMax_[1] - globalPos[1])
                / (0.25*height_()*height_()) + bjSlipVel_;
        // logarithmic profile
//        return 0.1*vmax*log((relativeHeight+1e-3)/1e-3) + bjSlipVel_;
    }


    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    const Scalar height_() const
    { return bBoxMax_[1] - bBoxMin_[1]; }

    // spatial parameters
    SpatialParams spatialParams_;

    static constexpr Scalar eps_ = 1e-6;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    
    Scalar refPressure_;
    Scalar refTemperature_;

    Scalar vxMax_;
    Scalar bjSlipVel_;
    Scalar sinusVelVar_;
    Scalar alphaBJ1_;
    Scalar alphaBJ2_;
    Scalar alphaBJ3_;
	
	Scalar nTemperature_;    
	Scalar nPressure_;         
	Scalar pressureLow_;        
	Scalar pressureHigh_;       
	Scalar temperatureLow_;     
	Scalar temperatureHigh_;    
	Scalar temperature_;  
	
	Scalar pO2Inlet_;
    Scalar initXH2Og_;
	
	std::string name_; 	

    Scalar xMaterialInterface_;
    Scalar runUpDistanceX_;
    Scalar initializationTime_;
};
} //end namespace

#endif
