// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2010 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FUELCELL_COUPLING_SPATIALPARAMS_HH
#define DUMUX_FUELCELL_COUPLING_SPATIALPARAMS_HH

#define ACOSTA 1 //1: use Acosta measurement data for pc-sw curve

#include <dune/grid/io/file/vtk/common.hh>
#include "dumux/material/spatialparameters/gstatrandompermeability.hh"

#include <dumux/material/spatialparams/implicitspatialparams.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>

#if !ACOSTA
//#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/philtophoblaw.hh>
#endif
#if ACOSTA
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedacosta.hh>
#endif

#include <dumux/io/plotfluidmatrixlaw.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class FuelCellCouplingSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(FuelCellCouplingSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(FuelCellCouplingSpatialParams, SpatialParams,
        FuelCellCouplingSpatialParams<TypeTag>);

// Set the material Law
SET_PROP(FuelCellCouplingSpatialParams, MaterialLaw)
{
private:
    // define the material law which is parameterized by effective
    // saturations
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
#if !ACOSTA	
	typedef RegularizedVanGenuchten<Scalar> EffMaterialLaw;
//  typedef RegularizedBrooksCorey<Scalar> EffMaterialLaw;
#endif
#if ACOSTA
	typedef RegularizedAcosta<Scalar> EffMaterialLaw;
#endif
	
public:
    // define the material law parameterized by absolute saturations
#if !ACOSTA	
	//typedef EffToAbsLaw<EffMaterialLaw> type;
	typedef PhilToPhobLaw<EffMaterialLaw> type;
#endif
#if ACOSTA
    typedef EffToAbsLaw<EffMaterialLaw> type;
#endif
};
}


/** \todo Please doc me! */
template<class TypeTag>
class FuelCellCouplingSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;
    typedef Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld> Tensor;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef std::vector<Scalar> PermeabilityType;
    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

public:
	typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;


    FuelCellCouplingSpatialParams(const GridView& gridView)
        : ParentType(gridView),
          indexSet_(gridView.indexSet())
    {
        try
        {
            soilType_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, SoilType);

            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, MaterialInterfaceX);

			// intrinsic permeabilities
            permeability_[0][1] = 0.0;
            permeability_[1][0] = 0.0;
            permeability_[0][0] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, PermeabilityX);
            permeability_[1][1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, PermeabilityY);
			
            // porosities
			porosity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Porosity);
			
			//thermalconductivity
			lambdaSolid_ = 15.6; //[W/(m*K)] Acosta
			
			// residual saturations
			materialLawParams_.setSwr(0.00);//here water, see philtophoblaw
			materialLawParams_.setSnr(0.00);
			
			// parameters for the Brooks-Corey law
			//materialParams_.setPe(1.02e4);
			//materialParams_.setLambda(1.59);
#if !ACOSTA
			//parameters for the vanGenuchten law
			materialLawParams_.setVgAlpha(6.66e-5); //alpha=1/pcb
			materialLawParams_.setVgn(3.652);
#endif
#if ACOSTA
			//parameters for the Acosta saturation capillary pressure relation
			materialLawParams_.setAcA(-1168.75); //imbition -1168.75; drainage -600;
			materialLawParams_.setAcB(8.5); //imbition 8.5; drainage 25;
			materialLawParams_.setAcC(-0.2); //imbition -0.2; drainage -16;
			materialLawParams_.setAcD(-700); //imbition -700; drainage -3300;
			materialLawParams_.setAcE(0.0); //imbition 0; drainage 800;
#endif
			eps_ = 1e-6;
        }
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }
    }

    ~FuelCellCouplingSpatialParams()
    {}

    /*!
     * \brief Apply the intrinsic permeability tensor to a pressure
     *        potential gradient.
     *
     * \param element       The current finite element
     * \param fvGeometry    The current finite volume geometry of the element
     * \param scvIdx       The index sub-control volume face where the
     *                      intrinsic velocity ought to be calculated.
     */
    const Tensor intrinsicPermeability(const Element &element,
                                 	   const FVElementGeometry &fvGeometry,
                                       const int scvIdx) const
    	{
			return permeability_;
    	}

    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    	{
			const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
            Scalar reactionLayerWidth = GET_RUNTIME_PARAM(TypeTag, Scalar, PorousMedium.ReactionLayerWidth);

			if (globalPos[1]<reactionLayerWidth)
				return 0.07; //reaction layer
			else
				return porosity_;
    	}

   /* double solidity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        int scvIdx) const
        {
			const GlobalPosition &globalPos = fvGeometry.subContVol[scvIdx].global;
			
			if (globalPos[1]<eps_)
				return 1 - porosity_;
			else
				return 0.8;
        }

	double theta(const Element &element,
			const FVElementGeometry &fvGeometry,
			int scvIdx) const
		{
			return 10.0;
		}*/

 
    /*!
     * \brief return the parameters for the material law depending on the position
     *
    * \param element The current finite element
    * \param fvGeometry The current finite volume geometry of the element
     * \param scvIdx The index of the sub-control volume
    */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    	{
			return materialLawParams_;
        }


    /*!
     * \brief Returns the heat capacity \f$[J/m^3 K]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume where
     *                    the heat capacity needs to be defined
     */
    Scalar heatCapacity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        return
            710 // specific heat capacity
            * 1430 // density of porous medium
            * (1 - porosity(element, fvGeometry, scvIdx));
    }

    const unsigned checkSoilType(const GlobalPosition &globalPos) const
    {
        return soilType_;
    }

    void loadIntrinsicPermeability(const GridView& gridView)
    {
	
	}

    // this is called from the coupled problem
    // and creates a gnuplot output of the Pc-Sw curve
    void plotMaterialLaw()
    {

    	
			PlotFluidMatrixLaw<MaterialLaw> porousMatrixLaw_;
			porousMatrixLaw_.plotpC(materialLawParams_,
			/*Swr=*/0.0,
			1.0 - /*Snr=*/0.0);
	}

    Scalar thermalConductivitySolid(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        return lambdaSolid_;
    }

private:
	const IndexSet& indexSet_;
	unsigned soilType_;
	Scalar xMaterialInterface_;
    Scalar porosity_;
	Scalar eps_;
    MaterialLawParams materialLawParams_;
	Scalar lambdaSolid_;
	Tensor permeability_;
	
};

} // end namespace
#endif

