// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2013 by Timo Koch										 *
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Porous-medium subproblem with coupling at the top boundary.
 */
#ifndef DUMUX_2PNCSUB_PROBLEM_HH
#define DUMUX_2PNCSUB_PROBLEM_HH

//O: NON-ISOTHERMAL MODEL, 1: ISOTHERMAL MODEL defined in fuelcellcouplingproblem

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#if ISOTHERMAL
#include <dumux/implicit/2pnccoupling/2pnccouplinglocalresidual.hh>
#endif
#if !ISOTHERMAL
#include <dumux/implicit/2pncnicoupling/2pncnicouplinglocalresidual.hh>
#endif

#include <dumux/modelcoupling/common/multidomainboxlocaloperator.hh>

#include <dumux/material/chemistry/electrochemistry/electrochem.hh>


namespace Dumux
{
template <class TypeTag>
class TwoPNCSubProblem;

namespace Properties
{
#if ISOTHERMAL
	NEW_TYPE_TAG(TwoPNCSubProblem,
				 INHERITS_FROM(BoxTwoPNC, SubDomain, FuelCellCouplingSpatialParams));
#endif
#if !ISOTHERMAL
	NEW_TYPE_TAG(TwoPNCSubProblem,
				 INHERITS_FROM(BoxTwoPNCNI, SubDomain, FuelCellCouplingSpatialParams));
#endif

// Set the problem property
SET_TYPE_PROP(TwoPNCSubProblem, Problem,
              Dumux::TwoPNCSubProblem<TTAG(TwoPNCSubProblem)>);

SET_INT_PROP(TwoPNCSubProblem, Formulation, TwoPNCFormulation::pgSl);


// the gas component balance (nitrogen) is replaced by the total mass balance
SET_PROP(TwoPNCSubProblem, ReplaceCompEqIdx)
{
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    static const int value = Indices::contiNEqIdx;
};

// set the constraints for multidomain / pdelab // not required here
SET_TYPE_PROP(TwoPNCSubProblem, Constraints,
        Dune::PDELab::NonoverlappingConformingDirichletConstraints);

// set the grid operator
SET_PROP(TwoPNCSubProblem, GridOperator)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, ConstraintsTrafo) ConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, GridFunctionSpace) GridFunctionSpace;
    typedef Dumux::PDELab::MultiDomainBoxLocalOperator<TypeTag> LocalOperator;

    enum
	{			
		numEq = GET_PROP_VALUE(TypeTag, NumEq),
		replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx)
	};
	
 public:
    typedef Dune::PDELab::GridOperator<GridFunctionSpace,
        GridFunctionSpace, LocalOperator,
        Dune::PDELab::ISTLBCRSMatrixBackend<numEq, numEq>,
        Scalar, Scalar, Scalar,
        ConstraintsTrafo,
        ConstraintsTrafo,
        true> type;
};

// the fluidsystem is set in the coupled problem
SET_PROP(TwoPNCSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};

// enable/disable velocity output
SET_BOOL_PROP(TwoPNCSubProblem, VtkAddVelocity, true);
	
	
// enable/disable electrochemistry output
SET_BOOL_PROP(TwoPNCSubProblem, useElectrochem, true);

// Enable gravity
SET_BOOL_PROP(TwoPNCSubProblem, ProblemEnableGravity, false);
}

template <class TypeTag = TTAG(TwoPNCSubProblem) >
class TwoPNCSubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    typedef TwoPNCSubProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    
    // the type tag of the coupled problem
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    // Declaration of fluid system
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
	typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
	typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
	typedef typename Dumux::ElectroChem<TypeTag> ElectroChem;


    enum { 	// number of equations
    		numEq = GET_PROP_VALUE(TypeTag, NumEq),
			numComponents = FluidSystem::numComponents,
			replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),


    		//Indices of the components
			wCompIdx = FluidSystem::H2OIdx,
			secCompIdx = FluidSystem::O2Idx,
			nCompIdx = FluidSystem::N2Idx,
		
			//Indices of the components
			H2OIdx = FluidSystem::H2OIdx,
			O2Idx = FluidSystem::O2Idx,
			N2Idx = FluidSystem::N2Idx,

			//Indices of the phases
			wPhaseIdx = FluidSystem::wPhaseIdx,
			nPhaseIdx = FluidSystem::nPhaseIdx,

			// the equation indices
			conti0EqIdx 	  = Indices::conti0EqIdx,
			contiWEqIdx       =	conti0EqIdx + FluidSystem::H2OIdx,
			contiTotalMassIdx = conti0EqIdx + FluidSystem::N2Idx, // TODO: conti of the total mass in the PM.
			contiO2EqIdx    =	conti0EqIdx + FluidSystem::O2Idx,
			energyEqIdx       = conti0EqIdx + FluidSystem::numComponents,

			// the indices of the primary variables
			pressureIdx   = Indices::pressureIdx,
			switchIdx     = Indices::switchIdx,
			temperatureIdx = FluidSystem::numComponents,

			// the indices for the phase presence
			wPhaseOnly = Indices::wPhaseOnly,
			nPhaseOnly = Indices::nPhaseOnly,
			bothPhases = Indices::bothPhases,

			// grid and world dimension
			dim 	 = GridView::dimension,
			dimWorld = GridView::dimensionworld
    	};

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    TwoPNCSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
    	try
    	{
			initializationTime_ = GET_RUNTIME_PARAM(TypeTag, Scalar, TimeManager.InitTime);
    		refTemperature_ 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperaturePM);
    					
			pO2Inlet_			= GET_RUNTIME_PARAM(TypeTag, Scalar, FuelCell.pO2Inlet); 
			
			nTemperature_       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.NTemperature);
            nPressure_          = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.NPressure);
            pressureLow_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureLow);
            pressureHigh_       = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.PressureHigh);
            temperatureLow_     = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TemperatureLow);
            temperatureHigh_    = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.TemperatureHigh);
            temperature_        = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.InitialTemperature);
            name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
			
			initPressure_       = GET_RUNTIME_PARAM(TypeTag, Scalar, PorousMedium.InitialPressure);
			initSaturation_     = GET_RUNTIME_PARAM(TypeTag, Scalar, PorousMedium.InitialSaturation);
			initXO2l_			= GET_RUNTIME_PARAM(TypeTag, Scalar, PorousMedium.InitialMoleFractionO2l);

			
            bBoxMin_[0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.XMin);
            bBoxMin_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.YMin);
			
            bBoxMax_[0] = GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.XMax);
            bBoxMax_[1] = GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.InterfacePos);
			
			eps_ = 1e-6;

			    					
    	}
    	catch (Dumux::ParameterException &e) {
    	    std::cerr << e << ". Abort!\n";
    	    exit(1) ;
    	}
    	catch (...) {
    	     std::cerr << "Unknown exception thrown!\n";
    	     exit(1);
    	}
    
		
        // initialize the tables of the fluid system
        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
	
	}

    ~TwoPNCSubProblem()
    {
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Vtk, NamePM); }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    /*void init()
    {
        // set the initial condition of the model
        ParentType::init();

        this->model().globalStorage(storageLastTimestep_);
    
        const int blModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FreeFlow, UseBoundaryLayerModel);
        std::cout << "Using boundary layer model " << blModel << std::endl;
    }*/

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 36 degrees Celsius.
     */
    Scalar boxTemperature(const Element &element,
                          const FVElementGeometry &fvGeometry,
                          int scvIdx) const
    {
        return refTemperature_;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex for which the boundary type is set
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar time = this->timeManager().time();
        
        values.setAllNeumann();

        if (onUpperBoundary_(globalPos))
        {
			if(time>=initializationTime_){
				values.setCouplingInflow(contiWEqIdx);
				values.setCouplingInflow(contiTotalMassIdx);
				values.setCouplingInflow(contiO2EqIdx);
#if !ISOTHERMAL
				values.setCouplingInflow(energyEqIdx);
#endif			
			} else {
				values.setAllDirichlet();
			}
			
        }
		/*
		//DEBUG Indices
		std::cout<<"Indices"<<std::endl;
		std::cout<<"numEq/soll4:  "<<numEq<<std::endl;
		std::cout<<"pressureIdx"<<pressureIdx<<std::endl;
		std::cout<<"switchIdx"<<switchIdx<<std::endl;
		std::cout<<"temperatureIdx"<<temperatureIdx<<std::endl;
		std::cout<<"energyEqIdx"<<energyEqIdx<<std::endl;
		 */
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex for which the boundary type is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
		const GlobalPosition globalPos = vertex.geometry().center();
		
        initial_(values, globalPos);
		
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param is The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {	
		values = Scalar(0.0);
	}

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */
    void source(PrimaryVariables &source,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx,
                const VolumeVariables &volVars) const
    {
		source = 0.0;
    	
    }
	
	void solDependentSource(PrimaryVariables &values,
							const Element &element,
							const FVElementGeometry &fvGeometry,
							const int scvIdx,
							const ElementVolumeVariables &elemVolVars) const
    {
        const GlobalPosition globalPos = element.geometry().corner(scvIdx);
        const VolumeVariables &volVars = elemVolVars[scvIdx];
		
		values = 0.0;
		
        //reaction sources from electro chemistry
        Scalar reactionLayerWidth = GET_RUNTIME_PARAM(TypeTag, Scalar, PorousMedium.ReactionLayerWidth);
        
        if(globalPos[1]<reactionLayerWidth)
        {
			ElectroChem electroChem;
			electroChem.reactionSource(values,
									   volVars);
			
			if(replaceCompEqIdx<numComponents)
				values[contiTotalMassIdx] += values[contiWEqIdx]+values[contiO2EqIdx];
		
            //influence factor, normally to 1 to have regular influence
            Scalar infFactor = GET_RUNTIME_PARAM(TypeTag, Scalar, FuelCell.infFactor);
            values[contiO2EqIdx] = values[contiO2EqIdx]*infFactor;
            values[contiTotalMassIdx] = values[contiTotalMassIdx]*infFactor;
            values[contiWEqIdx] = values[contiWEqIdx]*infFactor;
#if !ISOTHERMAL
            values[energyEqIdx] = values[energyEqIdx]*infFactor;
#endif
        
        
        }
    }
	

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local vertex index
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        values = Scalar(0);

        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vert The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vert,
                             const int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }

    void preTimeStep()
    {

    }

    void postTimeStep()
    {
	
	}


    /*!
     * \brief Determine if we are on a corner of the grid
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }

    // required in case of mortar coupling
    // otherwise it should return false
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { 
		return false; 
	}

    // \}
private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
    	// the internal method for the initial condition
		
		/*
		 *if bothPhases, set initial values for pg (pressureIdx), Sl (switchIdx), xO2l [mol/m3] (switchIdx+1)
		 *if nPhaseOnly, set initial values for pg (pressureIdx), xH2Og (switchIdx), xO2g [mol/m3] (switchIdx+1)
		 *if wPhaseOnly, set initial values for pg (pressureIdx), xN2l (switchIdx), xO2l [mol/m3] (switchIdx+1)
		 */
		
		Scalar pg = initPressure_;
		values[pressureIdx] = pg;
		values[switchIdx] = initSaturation_;//Sl for bothPhases
		//values[switchIdx] = 0.8*7.4e3/pg; //xgH2O for nPhaseOnly
		Scalar henry = Dumux::BinaryCoeff::H2O_O2::henry(refTemperature_);
		values[switchIdx+1] = pO2Inlet_/henry; //4.315e9; //moleFraction xlO2 for bothPhases   4.259e4; //FluidSystem::fugacityCoefficient(fluidState,g,02);
		
//		values[switchIdx+1] = initXO2l_;//4.315e9; //moleFraction xlO2 for bothPhases TODO: Henry coeff from fluidsystem
		//values[switchIdx+1] = pO2Inlet_/pg; //moleFraction xgO2 for nPhaseOnly
#if !ISOTHERMAL
		values[temperatureIdx] = refTemperature_;
#endif	
		
    }

    
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

	Scalar eps_;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar xMaterialInterface_;

	Scalar refTemperature_;
	
	Scalar nTemperature_;    
	Scalar nPressure_;         
	Scalar pressureLow_;        
	Scalar pressureHigh_;       
	Scalar temperatureLow_;     
	Scalar temperatureHigh_;    
	Scalar temperature_; 
	
	Scalar initPressure_;
	Scalar initSaturation_;
	Scalar initXO2l_;
	
	std::string name_; 
	
	Scalar pO2Inlet_;			
    Scalar runUpDistanceX_;
    Scalar initializationTime_;
};
} //end namespace

#endif
