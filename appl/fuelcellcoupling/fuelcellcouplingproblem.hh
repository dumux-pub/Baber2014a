/*****************************************************************************
 *   Copyright (C) 2013 by Timo Koch                                         *
 *   Copyright (C) 2013 by Vishal Jambhekar                                  *
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FUELCELL_COUPLING_PROBLEM_HH
#define DUMUX_FUELCELL_COUPLING_PROBLEM_HH

//O: NON-ISOTHERMAL MODEL, 1: ISOTHERMAL MODEL
#define ISOTHERMAL 0

#include <numeric> // required for std::partial_sum used in multidomainmcmgmapper.hh
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/multidomain/common/multidomainproblem.hh>
#include <dumux/multidomain/common/multidomainassembler.hh>
#include <dumux/modelcoupling/ncstokes2pnc/ncstokes2pncnewtoncontroller.hh>
#if ISOTHERMAL
#include <dumux/modelcoupling/ncstokes2pnc/ncstokes2pnclocaloperator.hh>
#endif
#if !ISOTHERMAL
#include <dumux/modelcoupling/ncnistokes2pncni/ncnistokes2pncnilocaloperator.hh>
#endif
#include "fuelcellcouplingspatialparams.hh"
#include <dumux/material/fluidsystems/h2on2o2fluidsystem.hh>

#include "stokesncsubproblem.hh"
#include "2pncsubproblem.hh"

namespace Dumux
{
template <class TypeTag>
class FuelCellCouplingProblem;

namespace Properties
{
NEW_TYPE_TAG(FuelCellCouplingProblem, INHERITS_FROM(MultiDomain));

// Set the grid type
SET_PROP(FuelCellCouplingProblem, Grid)
{
 public:
    typedef typename Dune::UGGrid<2> type;
};

// Specify the multidomain gridview
SET_PROP(FuelCellCouplingProblem, GridView)
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
 public:
    typedef typename MultiDomainGrid::LeafGridView type;
};

// Set the global problem
SET_TYPE_PROP(FuelCellCouplingProblem, Problem, Dumux::FuelCellCouplingProblem<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(FuelCellCouplingProblem, SubDomain1TypeTag, TTAG(StokesNCSubProblem));
SET_TYPE_PROP(FuelCellCouplingProblem, SubDomain2TypeTag, TTAG(TwoPNCSubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(StokesNCSubProblem, MultiDomainTypeTag, TTAG(FuelCellCouplingProblem));
SET_TYPE_PROP(TwoPNCSubProblem, MultiDomainTypeTag, TTAG(FuelCellCouplingProblem));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(StokesNCSubProblem, OtherSubDomainTypeTag, TTAG(TwoPNCSubProblem));
SET_TYPE_PROP(TwoPNCSubProblem, OtherSubDomainTypeTag, TTAG(StokesNCSubProblem));

SET_PROP(StokesNCSubProblem, SpatialParams)
{ typedef Dumux::FuelCellCouplingSpatialParams<TypeTag> type; };
SET_PROP(TwoPNCSubProblem, SpatialParams)
{ typedef Dumux::FuelCellCouplingSpatialParams<TypeTag> type; };

// Set the fluid system
SET_PROP(FuelCellCouplingProblem, FluidSystem)
{
private:
	typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
	static const bool useComplexRelations = true;
public:
    typedef Dumux::FluidSystems::H2ON2O2<Scalar, useComplexRelations> type;

};

SET_TYPE_PROP(FuelCellCouplingProblem, JacobianAssembler, Dumux::MultiDomainAssembler<TypeTag>);

#if !ISOTHERMAL
SET_TYPE_PROP(FuelCellCouplingProblem, MultiDomainCouplingLocalOperator, Dumux::NCNIStokesTwoPNCNILocalOperator<TypeTag>);
#endif
#if ISOTHERMAL
SET_TYPE_PROP(FuelCellCouplingProblem, MultiDomainCouplingLocalOperator, Dumux::NCStokesTwoPNCLocalOperator<TypeTag>);
#endif

// newton parameters
SET_TYPE_PROP(FuelCellCouplingProblem, NewtonController, Dumux::NCStokesTwoPNCNewtonController<TypeTag>);	

SET_TYPE_PROP(FuelCellCouplingProblem, LinearSolver, SuperLUBackend<TypeTag>);

// set this to one here (must fit to the structure of the coupled matrix which has block length 1)
SET_INT_PROP(FuelCellCouplingProblem, NumEq, 1);

SET_PROP(FuelCellCouplingProblem, SolutionVector)
{
 private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator) MultiDomainGridOperator;
 public:
    typedef typename MultiDomainGridOperator::Traits::Domain type;
};

// Write the solutions of individual newton iterations?
SET_BOOL_PROP(FuelCellCouplingProblem, NewtonWriteConvergence, false);

}

/*!
 * \brief The problem class for the coupling of an isothermal/non-isothermal n-component Stokes
 *        and an isothermal/non-isothermal two-phase n-component Darcy model.
 *
 *        The problem class for the coupling of an non-/isothermal n-component Stokes (stokesnc)
 *        and an isothermal two-phase n-component Darcy model (2pnc).
 *        It uses the 2pncCoupling model and the Stokesnccoupling model and provides
 *        the problem specifications for common parameters of the two submodels.
 *        The initial and boundary conditions of the submodels are specified in the two subproblems,
 *        2pncsubproblem.hh and stokesncsubproblem.hh, which are accessible via the coupled problem.
 */
template <class TypeTag = TTAG(FuelCellCouplingProblem) >
class FuelCellCouplingProblem : public MultiDomainProblem<TypeTag>
{
    template<int dim>
    struct VertexLayout
    {
        bool contains(Dune::GeometryType gt) {
            return gt.dim() == 0;
        }
    };

    typedef FuelCellCouplingProblem<TypeTag> ThisType;
    typedef MultiDomainProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Model;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) NewtonController;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) StokesncTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPNCTypeTag;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Problem) StokesNCSubProblem;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Problem) TwoPNCSubProblem;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Model) StokesncModel;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Model) TwoPNCModel;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, GridView) StokesncGridView;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, GridView) TwoPNCGridView;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, PrimaryVariables) StokesncPrimaryVariables;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, PrimaryVariables) TwoPNCPrimaryVariables;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, FluxVariables) BoundaryVariables1;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;

    typedef typename StokesncGridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename TwoPNCGridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename SubDomainGrid::template Codim<0>::EntityPointer SubDomainElementPointer;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Indices) StokesncIndices;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Indices) TwoPNCIndices;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, FluidSystem) FluidSystem;

    enum { dim = StokesncGridView::dimension };
	enum { // indices in the Stokes domain
        momentumXIdx1 = StokesncIndices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx1 = StokesncIndices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx1 = StokesncIndices::momentumZIdx, //!< Index of the z-component of the momentum balance
        lastMomentumIdx1 = StokesncIndices::lastMomentumIdx, //!< Index of the last component of the momentum balance
        massBalanceIdx1 = StokesncIndices::massBalanceIdx, //!< Index of the mass balance
        transportEqIdx1 = StokesncIndices::transportEqIdx, //!< Index of the first contiequation
	};
	enum { // indices in the Darcy domain
        massBalanceIdx2 = TwoPNCIndices::pressureIdx,
        switchIdx2 = TwoPNCIndices::switchIdx,
        contiTotalMassIdx2 = TwoPNCIndices::contiNEqIdx,
        contiWEqIdx2 = TwoPNCIndices::contiWEqIdx,

        wCompIdx2 = FluidSystem::wCompIdx,
        nCompIdx2 = FluidSystem::nCompIdx,

        wPhaseIdx2 = TwoPNCIndices::wPhaseIdx,
        nPhaseIdx2 = TwoPNCIndices::nPhaseIdx

    };
    enum { transportCompIdx1 = StokesncIndices::transportCompIdx };
    enum { phaseIdx = GET_PROP_VALUE(StokesncTypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(StokesncTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(TwoPNCTypeTag, NumEq)
    };

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> FieldVector;

    ////////////
    typedef typename MultiDomainGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator SubDomainInterfaceIterator;
    typedef typename MultiDomainGridView::IndexSet::IndexType IndexType;
    ////////////

public:
    FuelCellCouplingProblem(MultiDomainGrid &mdGrid,
                              TimeManager &timeManager)
        : ParentType(mdGrid, timeManager)
    {
        try
        {
            // define location of the interface
            interface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            noDarcyX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, NoDarcyX);
            episodeLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
			
			// define output options
			freqRestart_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
            freqOutput_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
		}
        catch (Dumux::ParameterException &e) {
            std::cerr << e << ". Abort!\n";
            exit(1) ;
        }
        catch (...) {
            std::cerr << "Unknown exception thrown!\n";
            exit(1);
        }

        Stokesnc_ = this->sdID1();
        TwoPNC_ = this->sdID2();

        mdGrid.startSubDomainMarking();
        
        // subdivide grid in two subdomains
        ElementIterator eendit = mdGrid.template leafend<0>();
        for (ElementIterator elementIt = mdGrid.template leafbegin<0>();
             elementIt != eendit; ++elementIt)
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (elementIt->partitionType() != Dune::InteriorEntity)
                continue;
            
            GlobalPosition globalPos = elementIt->geometry().center();
            
            if (globalPos[1] > interface_)
                mdGrid.addToSubDomain(Stokesnc_,*elementIt);
            else
                if(globalPos[0] > noDarcyX_)
                    mdGrid.addToSubDomain(TwoPNC_,*elementIt);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();
        
        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());
        
        this->sdProblem1().spatialParams().loadIntrinsicPermeability(this->sdGridView1());
        this->sdProblem2().spatialParams().loadIntrinsicPermeability(this->sdGridView2());
        // initialize the tables of the fluid system
        //FluidSystem::init();
		
		/*
        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);*/
    }

    ~FuelCellCouplingProblem()
    {
    };

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();
	}
    
    /*!
     * \brief Called by the time manager after the time integration.
     */
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    { //this->timeManager().startNextEpisode(episodeLength_); 
	}

    
	
    /*!
     * \brief Returns true if a restart file should be written to disk.
     */
    bool shouldWriteRestartFile() const
    {
       // if ((this->timeManager().timeStepIndex() > 0 &&
       //      (this->timeManager().timeStepIndex() % freqRestart_ == 0))
            // also write a restart file at the end of each episode
            //|| this->timeManager().episodeWillBeOver())
            return true;
        return false;
    }


    /*!
     * \brief Returns true if a VTK output file should be written.
     */
    bool shouldWriteOutput() const
    {
        //if (this->timeManager().timeStepIndex() % freqOutput_ == 0
        //    || this->timeManager().episodeWillBeOver())
          return true;
        //return false;
    }

    
   
    StokesNCSubProblem& StokesncProblem()
    { return this->sdProblem1(); }
    const StokesNCSubProblem& StokesncProblem() const
    { return this->sdProblem1(); }

    TwoPNCSubProblem& TwoPNCProblem()
    { return this->sdProblem2(); }
    const TwoPNCSubProblem& TwoPNCProblem() const
    { return this->sdProblem2(); }

private:
    typename MultiDomainGrid::SubDomainType Stokesnc_;
    typename MultiDomainGrid::SubDomainType TwoPNC_;

	unsigned counter_;
    unsigned freqRestart_;
    unsigned freqOutput_;
    
    Scalar interface_;
    Scalar episodeLength_;
    Scalar noDarcyX_;
    Scalar initializationTime_;

    template <int numEq>
    struct InterfaceFluxes
    {
        unsigned count;
        unsigned interfaceVertex;
        unsigned globalIdx;
        Scalar xCoord;
        Scalar yCoord;
        Dune::FieldVector<Scalar, numEq> defect;

        InterfaceFluxes()
        {
            count = 0;
            interfaceVertex = 0;
            globalIdx = 0;
            xCoord = 0.0;
            yCoord = 0.0;
            defect = 0.0;
        }
    };
};

} //end namespace

#endif
