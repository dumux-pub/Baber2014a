#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Please run this script preferably in an empty directory."
  echo "Aborting."
  exit 1
fi

# DUNE modules
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.3 https://gitlab.dune-project.org/core/dune-localfunctions.git

# DUNE multidomain
git clone git://github.com/smuething/dune-multidomain.git
cd dune-multidomain
git checkout deac3cecfc6697c1f5316d55c0fadd74f51d92bc
cd ..
git clone -b releases/2.3 git://github.com/smuething/dune-multidomaingrid.git

# DUNE pdelab
git clone -b releases/1.1 https://gitlab.dune-project.org/pdelab/dune-pdelab.git

# DuMuX
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout 831d016b18efdc24a9c81449bebb4e2e40a901df
cd ..
git clone -b master https://git.iws.uni-stuttgart.de/dumux-pub/Baber2014a.git

# apply patches
cd dune-pdelab
patch -p1 <../dumux/patches/pdelab-1.1.0.patch
cd ..
cd dune-multidomain
patch -p1 <../Baber2014a/appl/fuelcelldropsni/couplinggfs.patch
cd ..

# external modules: UG is required
mkdir external
cd external
wget https://gitlab.dune-project.org/staging/dune-uggrid/-/archive/v3.11.0/dune-uggrid-v3.11.0.tar.gz
tar zxvf dune-uggrid-v3.11.0.tar.gz
cd dune-uggrid-v3.11.0
autoreconf -is
OPTIM_FLAGS="-O3 -DNDEBUG -march=native -finline-functions -funroll-loops"
CFLAGS="$OPTIM_FLAGS"
CXXFLAGS="$OPTIM_FLAGS -std=c++0x -fno-strict-aliasing"
OPTS="--enable-dune --prefix=$PWD --without-mpi"
./configure CC=g++ CFLAGS="$CFLAGS" CXXFLAGS="$CXXFLAGS" $OPTS
make
make install
cd ../..

# run dunecontrol
pwd > temp.txt
sed -i 's/\//\\\//g' temp.txt
EXTPATH=`cat temp.txt`
/usr/bin/rm temp.txt
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" Baber2014a/optim.opts >optim.opts
sed "s/EXTDIR=.*/EXTDIR=$EXTPATH\/external/" Baber2014a/debug.opts >debug.opts
cd dune-common && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-common all
cd dune-geometry && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-geometry all
cd dune-grid && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-grid all
cd dune-istl && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-istl all
cd dune-localfunctions && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-localfunctions all
cd dune-pdelab && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-pdelab all
cd dune-multidomaingrid && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-multidomaingrid all
cd dune-multidomain && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dune-multidomain all
cd dumux && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=dumux all
cd Baber2014a && libtoolize && cd ..
./dune-common/bin/dunecontrol --opts=optim.opts --only=Baber2014a all

