/*****************************************************************************
 *   Copyright (C) 2011 by Klaus Mosthaf                                     *
 *   Copyright (C) 2008 by Bernd Flemisch                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Supplements the TwoPNCLocalResidual by the required functions for a coupled application.
 *
 */
#ifndef DUMUX_2PNC_COUPLING_LOCAL_RESIDUAL_HH
#define DUMUX_2PNC_COUPLING_LOCAL_RESIDUAL_HH

#include <dumux/implicit/2pnc/2pnclocalresidual.hh>

#define VELOCITY_OUTPUT 0 // uncomment this line if an output of the velocity is needed

namespace Dumux
{

/*!
 * \ingroup TwoPNCModel
 * \brief Supplements the TwoPNCModel by the required functions for a coupled application.
 */
template<class TypeTag>
class TwoPNCCouplingLocalResidual : public TwoPNCLocalResidual<TypeTag>
{
    typedef TwoPNCCouplingLocalResidual<TypeTag> ThisType;
    typedef TwoPNCLocalResidual<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum {
    	   dim = GridView::dimension,
    	   numEq = GET_PROP_VALUE(TypeTag, NumEq),
    	   numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
		   numComponents = FluidSystem::numComponents,

           pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,

           wPhaseIdx = FluidSystem::wPhaseIdx,
           nPhaseIdx = FluidSystem::nPhaseIdx,

    	   replaceCompEqIdx = GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx),

    	   wCompIdx = FluidSystem::wCompIdx,
    	   nCompIdx = FluidSystem::nCompIdx,

           conti0EqIdx    = Indices::conti0EqIdx,
           contiWEqIdx    = conti0EqIdx + wCompIdx,
           massBalanceIdx = conti0EqIdx + replaceCompEqIdx,

           phaseIdx = nPhaseIdx // index of the phase for the phase flux calculation STILL: just non-wetting phase, as in Stokes just non-wetting single phase
           //compIdx = wCompIdx // index of the component for the phase flux calculation NOW: more compontens
        };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef Dune::BlockVector<Dune::FieldVector<Scalar,1> > ElementFluxVector;

    typedef Dune::FieldVector<Scalar, dim> DimVector;

public:
    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    TwoPNCCouplingLocalResidual()
    { };

    void evalBoundary_()
    {
        ParentType::evalBoundary_();

        typedef Dune::GenericReferenceElements<Scalar, dim> ReferenceElements;
        typedef Dune::GenericReferenceElement<Scalar, dim> ReferenceElement;
        const ReferenceElement &refElement = ReferenceElements::general(this->element_().geometry().type());

        // evaluate Dirichlet-like coupling conditions
        for (int idx = 0; idx < this->fvGeometry_().numScv; idx++)
        {
            // evaluate boundary conditions for the intersections of
            // the current element
            IntersectionIterator isIt = this->gridView_().ibegin(this->element_());
            const IntersectionIterator &endIt = this->gridView_().iend(this->element_());
            for (; isIt != endIt; ++isIt)
            {
                // handle only intersections on the boundary
                if (!isIt->boundary())
                    continue;

                // assemble the boundary for all vertices of the current face
                const int faceIdx = isIt->indexInInside();
                const int numFaceVertices = refElement.size(faceIdx, 1, dim);

                // loop over the single vertices on the current face
                for (int faceVertIdx = 0; faceVertIdx < numFaceVertices; ++faceVertIdx)
                {
                    const int boundaryFaceIdx = this->fvGeometry_().boundaryFaceIndex(faceIdx, faceVertIdx);
                    const int elemVertIdx = refElement.subEntity(faceIdx, 1, faceVertIdx, dim);
                    // only evaluate, if we consider the same face vertex as in the outer
                    // loop over the element vertices
                    if (elemVertIdx != idx)
                        continue;
					
                    if (boundaryHasCoupling_(this->bcTypes_(idx)))
                        evalCouplingVertex_(idx); //also calculate residual for corner points first					

                    //for the corner points, the boundary flux across the vertical non-coupling boundary faces
                    //has to be calculated to fulfill the mass balance
                    //convert suddomain intersection into multidomain intersection and check whether it is an outer boundary
                    if(!GridView::Grid::multiDomainIntersection(*isIt).neighbor()
                            && this->boundaryHasNeumann_(this->bcTypes_(idx)))
                    {
                        const DimVector& globalPos = this->fvGeometry_().subContVol[idx].global;
                        //problem specific function, in problem orientation of interface is known
                        if(this->problem_().isInterfaceCornerPoint(globalPos))
                        {
                            PrimaryVariables values(0.0);

                            //calculate the actual boundary fluxes and add to residual
                            this->asImp_()->computeFlux(values, boundaryFaceIdx, true /*on boundary*/);
                            for (int equationIdx = 0; equationIdx < numEq; ++equationIdx)
                            {
                                if(this->bcTypes_(idx).isNeumann(equationIdx))
                                    continue;
                                this->residual_[idx][equationIdx] += values[equationIdx];
                            }
                        }
                    }
                }
            }
        }
    }

	//just set for one phase/comp where is it used? ->only in output
    Scalar evalPhaseStorage(const int scvIdx) const
    {
        Scalar phaseStorage = computePhaseStorage(scvIdx, false);
        Scalar oldPhaseStorage = computePhaseStorage(scvIdx, true);;
        Valgrind::CheckDefined(phaseStorage);
        Valgrind::CheckDefined(oldPhaseStorage);

        phaseStorage -= oldPhaseStorage;
        phaseStorage *= this->fvGeometry_().subContVol[scvIdx].volume
            / this->problem_().timeManager().timeStepSize()
            * this->curVolVars_(scvIdx).extrusionFactor();
        Valgrind::CheckDefined(phaseStorage);

        return phaseStorage;
    }

	//just set for one phase/comp where is it used? -> only in output
    Scalar computePhaseStorage(const int scvIdx, bool usePrevSol) const
    {
        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_()
            : this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];

        // compute storage term of all components within all phases
        Scalar phaseStorage = volVars.molarDensity(phaseIdx)
            * volVars.saturation(phaseIdx)
            * volVars.fluidState().moleFraction(phaseIdx, wCompIdx);
        phaseStorage *= volVars.porosity();

        return phaseStorage;
    }

    /*!
     * \brief Compute the fluxes within the different fluid phases. This is
     *        merely for the computation of flux output.
     */
    void evalPhaseFluxes()
    {
        elementFluxes_.resize(this->fvGeometry_().numScv);
        elementFluxes_ = 0.;

        Scalar flux(0.);

        // calculate the mass flux over the faces and subtract
        // it from the local rates
        for (int faceIdx = 0; faceIdx < this->fvGeometry_().numScvf; faceIdx++)
        {
            FluxVariables vars(this->problem_(),
                               this->element_(),
                               this->fvGeometry_(),
                               faceIdx,
                               this->curVolVars_());

            int i = this->fvGeometry_().subContVolFace[faceIdx].i;
            int j = this->fvGeometry_().subContVolFace[faceIdx].j;

            const Scalar extrusionFactor =
                (this->curVolVars_(i).extrusionFactor()
                 + this->curVolVars_(j).extrusionFactor())
                / 2;
            flux = computeAdvectivePhaseFlux(vars) +
                computeDiffusivePhaseFlux(vars);
            flux *= extrusionFactor;

            elementFluxes_[i] += flux;
            elementFluxes_[j] -= flux;
        }
    }

    /*!
     * \brief Compute the advective fluxes within the different phases.
     */
    Scalar computeAdvectivePhaseFlux(const FluxVariables &fluxVars) const
    {
        Scalar advectivePhaseFlux = 0.;
        const Scalar massUpwindWeight = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);

        // data attached to upstream and the downstream vertices
        // of the current phase
        const VolumeVariables &up =
                this->curVolVars_(fluxVars.upstreamIdx(phaseIdx));
        const VolumeVariables &dn =
                this->curVolVars_(fluxVars.downstreamIdx(phaseIdx));
        if (massUpwindWeight > 0.0)
            // upstream vertex
            advectivePhaseFlux += massUpwindWeight
                * fluxVars.KmvpNormal(phaseIdx)
                * up.mobility(phaseIdx)
                * up.molarDensity(phaseIdx)
                * up.fluidState().moleFraction(phaseIdx, wCompIdx);
        if (massUpwindWeight < 1.0)
            // downstream vertex
            advectivePhaseFlux +=(1 - massUpwindWeight)
                * fluxVars.KmvpNormal(phaseIdx)
				* up.mobility(phaseIdx)
                * dn.molarDensity(phaseIdx)
                * dn.fluidState().moleFraction(phaseIdx, wCompIdx);

        return advectivePhaseFlux;
    }
    
    /*!
     * \brief Compute the diffusive fluxes within the different phases.
     */
    Scalar computeDiffusivePhaseFlux(const FluxVariables &fluxVars) const
    {	
		Scalar diffusivePhaseFlux;
		
		for(int compIdx; compIdx<numComponents;compIdx++)
		{	
			if(compIdx!=replaceCompEqIdx) {
				
				// add diffusive flux of gas component in liquid phase
				diffusivePhaseFlux = fluxVars.moleFractionGrad(phaseIdx,compIdx)*fluxVars.face().normal;
				diffusivePhaseFlux *= -fluxVars.porousDiffCoeff(phaseIdx, compIdx) *
							fluxVars.molarDensity(phaseIdx);
			}
		}

        return diffusivePhaseFlux;
    }

    /*!
     * \brief Set the Dirichlet-like conditions for the coupling
     *        and replace the existing residual for n components
     *
     * \param idx vertex index for the coupling condition
     */
    void evalCouplingVertex_(const int scvIdx)
    {
        const VolumeVariables &volVars = this->curVolVars_()[scvIdx];
	
        // set pressure as part of the momentum coupling and set the mole fractions
        // for other components in the non-wetting phase
        for (int compIdx = 0; compIdx < numComponents; compIdx++) //Air, H2O, Salt
		{
				
				int eqIdx = conti0EqIdx + compIdx; 
				
				if (replaceCompEqIdx == eqIdx)//total mass balance
				{
					// (nPhaseIdx = massBalanceIdx = replaceCompEqIdx)
					if (this->bcTypes_(scvIdx).isCouplingOutflow(massBalanceIdx))
						this->residual_[scvIdx][massBalanceIdx] = volVars.pressure(nPhaseIdx);
				}
				else
				{
					if (this->bcTypes_(scvIdx).isCouplingOutflow(eqIdx))
						this->residual_[scvIdx][eqIdx] = volVars.fluidState().moleFraction(nPhaseIdx, compIdx);
				}
		}
	}

    /*!
     * \brief Check if one of the boundary conditions is coupling.
     */
    bool boundaryHasCoupling_(const BoundaryTypes& bcTypes) const
    {
        for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
            if (bcTypes.isCouplingInflow(eqIdx) || bcTypes.isCouplingOutflow(eqIdx))
                return true;
        return false;
    }
    /*!
     * \brief Check if one of the boundary conditions is Neumann.
     */
    bool boundaryHasNeumann_(const BoundaryTypes& bcTypes) const
    {
        bool hasNeumann = false;
        for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
            if (bcTypes.isNeumann(eqIdx) || bcTypes.isNeumann(eqIdx))
                hasNeumann = true;
        return hasNeumann;
    }

    Scalar elementFluxes(const int scvIdx)
    {
        return elementFluxes_[scvIdx];
    }

protected:
    ElementFluxVector elementFluxes_;
};

}

#endif
