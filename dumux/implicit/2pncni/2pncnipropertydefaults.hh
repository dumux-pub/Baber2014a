// $Id: 2pncniproperties.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPNCNIModel
 */
/*!
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        2pncNI box model.
 */
#ifndef DUMUX_2PNCNI_PROPERTY_DEFAULTS_HH
#define DUMUX_2PNCNI_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/2pnc/2pncpropertydefaults.hh>

#include "2pncnimodel.hh"
#include "2pncniindices.hh"
#include "2pncnifluxvariables.hh"
#include "2pncnivolumevariables.hh"
#include "2pncniproperties.hh"
#include "2pncnilocalresidual.hh"

#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

namespace Dumux
{

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of components.
 *
 * We just forward the number from the fluid system
 *
 */

SET_PROP(BoxTwoPNCNI, NumEq)
{	
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;
	
public:
    static const int value = FluidSystem::numComponents /*+ FluidSystem::numSPhases*/ + /*temperature*/1;
};

//! Use the 2p2cni local jacobian operator for the 2p2cni model
SET_TYPE_PROP(BoxTwoPNCNI,
              LocalResidual,
              TwoPNCNILocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(BoxTwoPNCNI, Model, TwoPNCNIModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(BoxTwoPNCNI, VolumeVariables, TwoPNCNIVolumeVariables<TypeTag>);


//! the FluxVariables property
SET_TYPE_PROP(BoxTwoPNCNI, FluxVariables, TwoPNCNIFluxVariables<TypeTag>);

//! The indices required by the non-isothermal 2pnc model
SET_PROP(BoxTwoPNCNI, Indices)
{ private:
    enum { formulation = GET_PROP_VALUE(TypeTag, Formulation) };
 public:
    typedef TwoPNCNIIndices<TypeTag, 0> type;
};

//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(BoxTwoPNCNI, ThermalConductivityModel)
{ private :
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  public:
    typedef ThermalConductivitySomerton<Scalar> type;
};

SET_BOOL_PROP(BoxTwoPNCNI, Acosta, false);

}
}
#endif
