// $Id: 2pncnilocalresidual.hh 3795 2010-06-25 16:08:04Z melanie $
/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Bernd Flemisch                               *
 *   Copyright (C) 2009-2010 by Andreas Lauser                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the non-isothermal two-phase n-component box model.
 */

#ifndef DUMUX_2PNCNI_LOCAL_RESIDUAL_BASE_HH
#define DUMUX_2PNCNI_LOCAL_RESIDUAL_BASE_HH

#include <dumux/implicit/2pnc/2pnclocalresidual.hh>

#include "2pncnivolumevariables.hh"
#include "2pncnifluxvariables.hh"
#include "2pncniproperties.hh"
#include "2pncniindices.hh"



//#include <iostream>
//#include <vector>

//#define VELOCITY_OUTPUT 1 // uncomment this line if an output of the velocity is needed

namespace Dumux
{
/*!
 * \ingroup TwoPNCNIModel
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the non-isothermal two-phase n-component box model.
 *
 * This class is used to fill the gaps in BoxLocalResidual for the two-phase n-component flow.
 */
template<class TypeTag>
class TwoPNCNILocalResidual: public TwoPNCLocalResidual<TypeTag>
{
protected:
    typedef TwoPNCNILocalResidual<TypeTag> ThisType;
    typedef TwoPNCLocalResidual<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef CompositionalFluidState<Scalar, FluidSystem> FluidState;

    enum
    {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        numEq = GET_PROP_VALUE(TypeTag, NumEq),
        numPhases = GET_PROP_VALUE(TypeTag, NumPhases),
        //numSPhases = GET_PROP_VALUE(TypeTag, NumSPhases),
        numComponents = FluidSystem::numComponents,
        conti0EqIdx = Indices::conti0EqIdx,

        energyEqIdx = Indices::energyEqIdx, 
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,

        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases,


        plSg = TwoPNCFormulation::plSg,
        pgSl = TwoPNCFormulation::pgSl,
        formulation = GET_PROP_VALUE(TypeTag, PTAG(Formulation))
    };

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(VolumeVariables)) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(ElementVolumeVariables)) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(ElementBoundaryTypes)) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluxVariables)) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(SpatialParams)) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FVElementGeometry)) FVElementGeometry;


    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::ctype CoordScalar;

//    static constexpr Scalar massUpwindWeight_ = GET_PROP_VALUE(TypeTag, massUpwindWeight_);

public:

    /*!
     * \brief Constructor. Sets the upwind weight.
     */
    TwoPNCNILocalResidual()
    {
        // retrieve the upwind weight for the mass conservation equations. Use the value
        // specified via the property system as default, and overwrite
        // it by the run-time parameter from the Dune::ParameterTree
        massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
    };

     /*!
     * \brief Evaluate the amount all conservation quantities
     *        (e.g. phase mass) within a sub-control volume.
     *
     * The result should be averaged over the volume (e.g. phase mass
     * inside a sub control volume divided by the volume)
     *
     *  \param result The mass of the component within the sub-control volume
     *  \param scvIdx The SCV (sub-control-volume) index
     *  \param usePrevSol Evaluate function with solution of current or previous time step
     */
    void computeStorage(PrimaryVariables &storage, int scvIdx, bool usePrevSol) const
    {
    	// compute the storage term for phase mass
    	   ParentType::computeStorage(storage, scvIdx, usePrevSol);

        // if flag usePrevSol is set, the solution from the previous
        // time step is used, otherwise the current solution is
        // used. The secondary variables are used accordingly.  This
        // is required to compute the derivative of the storage term
        // using the implicit euler method.
        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_(): this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];

        storage[energyEqIdx] = 0.0;

        for (int phaseIdx = 0; phaseIdx < numPhases /*+ numSPhases*/; ++phaseIdx)
        {
        	//if (phaseIdx < numPhases)
            	// compute energy storage term of all fluis phases
				storage[energyEqIdx] += volVars.porosity()
									  * volVars.density(phaseIdx)
									  * volVars.internalEnergy(phaseIdx)
									  * volVars.saturation(phaseIdx);

            /*if(phaseIdx>=numPhases)
              // compute storage term of all precipitated solid phases
			  storage[energyEqIdx] += volVars.heatCapacity(phaseIdx)
			  	  	  	  	  	  	 * volVars.temperature();*/
        }
        // heat capacity is already multiplied by the density
	    // of the porous material and the porosity in the problem file
        storage[energyEqIdx] += volVars.temperature()* volVars.heatCapacity(0 /*SolidMatrix*/);

        Valgrind::CheckDefined(storage);
	}
    /*!
     * \brief Evaluates the advective mass flux and the heat flux
     * over a face of a subcontrol volume and writes the result in
     * the flux vector.
     *
     * \param flux The advective flux over the SCV (sub-control-volume) face for each component
     * \param fluxVars The flux variables at the current SCV face
     *
     * This method is called by compute flux (base class)
     */

    void computeAdvectiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
	// advective mass flux
	ParentType::computeAdvectiveFlux(flux, fluxVars);

	// advective heat flux in all phases
	flux[energyEqIdx] = 0;
	for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx) {
		// vertex data of the upstream and the downstream vertices
		const VolumeVariables &up = this->curVolVars_(fluxVars.upstreamIdx(phaseIdx));
		const VolumeVariables &dn = this->curVolVars_(fluxVars.downstreamIdx(phaseIdx));

		flux[energyEqIdx] +=
			fluxVars.volumeFlux(phaseIdx) * (massUpwindWeight_ * // upstream vertex
											 (  up.density(phaseIdx) *
												up.enthalpy(phaseIdx))
											 +
											 (1-massUpwindWeight_) * // downstream vertex
											 (  dn.density(phaseIdx) *
												dn.enthalpy(phaseIdx)) );
        }

    }


    /*!
     * \brief Adds the diffusive mass flux of all components over
     *        a face of a subcontrol volume.
     *
     * \param flux The diffusive flux over the sub-control-volume face for each component
     * \param vars The flux variables at the current SCV
     */
    void computeDiffusiveFlux(PrimaryVariables &flux, const FluxVariables &fluxVars) const
    {
    	// diffusive mass flux
        ParentType::computeDiffusiveFlux(flux, fluxVars);

        // diffusive heat flux
        flux[energyEqIdx] += fluxVars.normalMatrixHeatFlux();
        Valgrind::CheckDefined(flux);
    }

private:
Scalar massUpwindWeight_;

};

} // end namespace

#endif
