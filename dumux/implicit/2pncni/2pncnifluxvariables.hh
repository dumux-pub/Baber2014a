// $Id: 2pncnifluxvariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2009 by Andreas Lauser                               *
 *   Copyright (C) 2008 by Bernd Flemisch                                    *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   This file contains the data which is required to calculate
 *          all fluxes of components over a face of a finite volume for
 *          the non-isothermal two-phase, two-component model.
 */
/*!
 * \ingroup TwoPNCNIModel
 */
#ifndef DUMUX_2PNCNI_FLUX_VARIABLES_HH
#define DUMUX_2PNCNI_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>
#include <dumux/common/spline.hh>
#include <dumux/implicit/2pnc/2pncfluxvariables.hh>

namespace Dumux
{

/*!
 * \brief This template class contains the data which is required to
 *        calculate all fluxes of components over a face of a finite
 *        volume for the non-isothermal two-phase, two-component model.
 *
 * This means pressure and concentration gradients, phase densities at
 * the integration point, etc.
 */
template <class TypeTag>
class TwoPNCNIFluxVariables: public TwoPNCFluxVariables<TypeTag>
{
	typedef TwoPNCFluxVariables<TypeTag> parentType;
    typedef TwoPNCNIFluxVariables<TypeTag> thisType;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridView)) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Problem)) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(VolumeVariables)) VolumeVariables;

    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(ElementVolumeVariables)) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel) ThermalConductivityModel;

    enum {
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        numPhases = GET_PROP_VALUE(TypeTag, PTAG(NumPhases)),
        numComponents = GET_PROP_VALUE(TypeTag, PTAG(NumComponents)),
    };
    
    enum {
        acostaConstantThermalConductivity = GET_PROP_VALUE(TypeTag, Acosta)
    };

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FVElementGeometry)) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(SpatialParams)) SpatialParams;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;
    typedef typename FVElementGeometry::SubControlVolume SCV;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;

    typedef Dune::FieldVector<CoordScalar, dimWorld> DimVector;
    typedef Dune::FieldMatrix<CoordScalar, dim, dim> DimMatrix;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    
    enum {
        wPhaseIdx = FluidSystem::wPhaseIdx,
        nPhaseIdx = FluidSystem::nPhaseIdx,
        wCompIdx = FluidSystem::H2OIdx,
    };

public:
    /*
     * \brief The constructor
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     */
    TwoPNCNIFluxVariables(const Problem &problem,
                     const Element &element,
                     const FVElementGeometry &fvGeometry,
                     const int faceIdx,
                     const ElementVolumeVariables &elemVolVars,
                     const bool onBoundary = false)
        : parentType(problem, element, fvGeometry, faceIdx, elemVolVars, onBoundary)
    {
    	faceIdx_ = faceIdx;
    	calculateValues_(problem, element, elemVolVars);
    };

    /*!
     * \brief The total heat flux \f$\mathrm{[J/s]}\f$ due to heat conduction
     *        of the rock matrix over the sub-control volume face in
     *        direction of the face normal.
     */
    Scalar normalMatrixHeatFlux() const
    { return normalMatrixHeatFlux_; }

    DimVector temperatureGradient() const
    { return temperatureGrad_; }

    Scalar effThermalConductivity() const
    { return lambdaEff_; }

protected:
    void calculateValues_(const Problem &problem,
                          const Element &element,
                          const ElementVolumeVariables &elemVolVars)
    {
        // calculate temperature gradient using finite element
        // gradients
        temperatureGrad_ = 0;
        DimVector tmp(0.0);
        for (int idx = 0; idx < this->face().numFap; idx++)
        {
            tmp = this->face().grad[idx];

            // index for the element volume variables
            int volVarsIdx = this->face().fapIndices[idx];

            tmp *= elemVolVars[volVarsIdx].temperature();
            temperatureGrad_ += tmp;
        }

        if(acostaConstantThermalConductivity)
        {
            lambdaEff_ = 15.6;
        }
        else
        {
            lambdaEff_ = 0;
            calculateEffThermalConductivity_(problem, element, elemVolVars);
        }
		// project the heat flux vector on the face's normal vector
		normalMatrixHeatFlux_ = temperatureGrad_* this->face().normal;
		normalMatrixHeatFlux_ *= -lambdaEff_;
    }

void calculateEffThermalConductivity_(const Problem &problem,
									  const Element &element,
									  const ElementVolumeVariables &elemVolVars)
{
    const unsigned i = this->face().i;
    const unsigned j = this->face().j;
    Scalar lambdaI, lambdaJ;

    if (GET_PROP_VALUE(TypeTag, ImplicitIsBox))
    {
        lambdaI =
            ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[i].saturation(wPhaseIdx),
                                                               elemVolVars[i].thermalConductivity(wPhaseIdx),
                                                               elemVolVars[i].thermalConductivity(nPhaseIdx),
                                                               problem.spatialParams().thermalConductivitySolid(element, this->fvGeometry_, i),
                                                               problem.spatialParams().porosity(element, this->fvGeometry_, i));
        lambdaJ =
            ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[j].saturation(wPhaseIdx),
                                                               elemVolVars[j].thermalConductivity(wPhaseIdx),
                                                               elemVolVars[j].thermalConductivity(nPhaseIdx),
                                                               problem.spatialParams().thermalConductivitySolid(element, this->fvGeometry_, j),
                                                               problem.spatialParams().porosity(element, this->fvGeometry_, j));
    }
    else
    {
        const Element& elementI = *this->fvGeometry_.neighbors[i];
        FVElementGeometry fvGeometryI;
        fvGeometryI.subContVol[0].global = elementI.geometry().center();

        lambdaI =
            ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[i].saturation(wPhaseIdx),
                                                               elemVolVars[i].thermalConductivity(wPhaseIdx),
                                                               elemVolVars[i].thermalConductivity(nPhaseIdx),
                                                               problem.spatialParams().thermalConductivitySolid(elementI, fvGeometryI, 0),
                                                               problem.spatialParams().porosity(elementI, fvGeometryI, 0));

        const Element& elementJ = *this->fvGeometry_.neighbors[j];
        FVElementGeometry fvGeometryJ;
        fvGeometryJ.subContVol[0].global = elementJ.geometry().center();

        lambdaJ =
            ThermalConductivityModel::effectiveThermalConductivity(elemVolVars[j].saturation(wPhaseIdx),
                                                               elemVolVars[j].thermalConductivity(wPhaseIdx),
                                                               elemVolVars[j].thermalConductivity(nPhaseIdx),
                                                               problem.spatialParams().thermalConductivitySolid(elementJ, fvGeometryJ, 0),
                                                               problem.spatialParams().porosity(elementJ, fvGeometryJ, 0));
    }

    // -> harmonic mean
    lambdaEff_ = harmonicMean(lambdaI, lambdaJ);
}

private:
    Scalar lambdaEff_;
    Scalar normalMatrixHeatFlux_;
    DimVector temperatureGrad_;
    int faceIdx_;
};

} // end namespace

#endif
