// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This local operator extends the ncstokes2pnclocaloperator
 *        by non-isothermal conditions.
 */
#ifndef DUMUX_NCNISTOKES_2PNCNI_LOCALOPERATOR_HH
#define DUMUX_NCNISTOKES_2PNCNI_LOCALOPERATOR_HH

#include <dumux/modelcoupling/ncstokes2pnc/ncstokes2pnclocaloperator.hh>

#include <dumux/freeflow/stokesncni/stokesncnimodel.hh>
#include <dumux/implicit/2pncnicoupling/2pncnicouplingmodel.hh>

namespace Dumux {

template<class TypeTag>
class NCNIStokesTwoPNCNILocalOperator : public NCStokesTwoPNCLocalOperator<TypeTag>
{
public:
	typedef NCStokesTwoPNCLocalOperator<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;

    // Get the TypeTags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) StokesncniTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPNCNITypeTag;

    typedef typename GET_PROP_TYPE(StokesncniTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPNCNITypeTag, FluxVariables) BoundaryVariables2;

    // Multidomain Grid and Subgrid types
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;

    typedef typename GET_PROP_TYPE(StokesncniTypeTag, GridView) StokesncniGridView;
    typedef typename GET_PROP_TYPE(TwoPNCNITypeTag, GridView) TwoPNCNIGridView;

    typedef typename StokesncniGridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename TwoPNCNIGridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename GET_PROP_TYPE(StokesncniTypeTag, Indices) StokesncniIndices;
    typedef typename GET_PROP_TYPE(TwoPNCNITypeTag, Indices) TwoPNCNIIndices;

    enum { dim = MultiDomainGrid::dimension,
           energyEqIdx1 = StokesncniIndices::energyEqIdx,      //!< Index of the energy balance equation

           numPhases2 = GET_PROP_VALUE(TwoPNCNITypeTag, NumPhases), // indices in the Darcy domain

           // equation index
           energyEqIdx2 = TwoPNCNIIndices::energyEqIdx,      //!< Index of the energy balance equation

           wPhaseIdx2 = TwoPNCNIIndices::wPhaseIdx,          //!< Index for the liquid phase
           nPhaseIdx2 = TwoPNCNIIndices::nPhaseIdx           //!< Index for the gas phase
    	};

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;       //!< A field vector with dim entries

    NCNIStokesTwoPNCNILocalOperator(GlobalProblem& globalProblem)
        : ParentType(globalProblem)
    { }

    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

    /*!
     * \brief Evaluation of the coupling from Stokes to Darcy.
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
    void evalCoupling12(const LFSU1& lfsu_s, const LFSU2& lfsu_n,
                        const int vertInElem1, const int vertInElem2,
                        const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,
                        const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                        const CParams &cParams,
                        RES1& couplingRes1, RES2& couplingRes2) const
    {
        const DimVector& globalPos1 = cParams.fvGeometry1.subContVol[vertInElem1].global;
        const DimVector& bfNormal1 = boundaryVars1.face().normal;
        const Scalar normalFlux1 = boundaryVars1.normalVelocity() *
            cParams.elemVolVarsCur1[vertInElem1].density();
        GlobalProblem& globalProblem = this->globalProblem();

        // evaluate coupling of mass and momentum balances
        ParentType::evalCoupling12(lfsu_s, lfsu_n,
                                   vertInElem1, vertInElem2,
                                   sdElement1, sdElement2,
                                   boundaryVars1, boundaryVars2,
                                   cParams,
                                   couplingRes1, couplingRes2);

        if (cParams.boundaryTypes2.isCouplingInflow(energyEqIdx2))
        {
            if (globalProblem.sdProblem1().isCornerPoint(globalPos1))
            {
                const Scalar convectiveFlux =
                    normalFlux1 *
                    cParams.elemVolVarsCur1[vertInElem1].enthalpy();
                const Scalar conductiveFlux =
                    bfNormal1 *
                    boundaryVars1.temperatureGrad() *
                    boundaryVars1.thermalConductivity();

                couplingRes2.accumulate(lfsu_n.child(energyEqIdx2), vertInElem2,
                                        -(convectiveFlux - conductiveFlux));
            }
            else
            {
                // the energy flux from the stokes domain
                couplingRes2.accumulate(lfsu_n.child(energyEqIdx2), vertInElem2,
                                        globalProblem.localResidual1().residual(vertInElem1)[energyEqIdx1]);
            }
        }
        if (cParams.boundaryTypes2.isCouplingOutflow(energyEqIdx2))
        {
            // set residualDarcy[energyEqIdx2] = T in 2p2cnilocalresidual.hh
            couplingRes2.accumulate(lfsu_n.child(energyEqIdx2), vertInElem2,
                                    -cParams.elemVolVarsCur1[vertInElem1].temperature());
        }
    }

    /*!
     * \brief Evaluation of the coupling from Darcy to Stokes.
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
    void evalCoupling21(const LFSU1& lfsu_s, const LFSU2& lfsu_n,
                        const int vertInElem1, const int vertInElem2,
                        const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,
                        const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                        const CParams &cParams,
                        RES1& couplingRes1, RES2& couplingRes2) const
    {
        GlobalProblem& globalProblem = this->globalProblem();

        // evaluate coupling of mass and momentum balances
        ParentType::evalCoupling21(lfsu_s, lfsu_n,
                                   vertInElem1, vertInElem2,
                                   sdElement1, sdElement2,
                                   boundaryVars1, boundaryVars2,
                                   cParams,
                                   couplingRes1, couplingRes2);

        // TODO: this should be done only once
        const DimVector& globalPos2 = cParams.fvGeometry2.subContVol[vertInElem2].global;
        //        const DimVector& bfNormal2 = boundaryVars2.face().normal;
        DimVector normalFlux2(0.);

        // velocity*normal*area*rho
        for (int phaseIdx=0; phaseIdx<numPhases2; ++phaseIdx)
            normalFlux2[phaseIdx] = -boundaryVars2.volumeFlux(phaseIdx)*
                cParams.elemVolVarsCur2[vertInElem2].density(phaseIdx);
        ////////////////////////////////////////

        if (cParams.boundaryTypes1.isCouplingOutflow(energyEqIdx1))
        {
            // set residualStokes[energyIdx1] = T in Stokesncnilocalresidual.hh
            couplingRes1.accumulate(lfsu_s.child(energyEqIdx1), vertInElem1,
                                    -cParams.elemVolVarsCur2[vertInElem2].temperature());
        }
        if (cParams.boundaryTypes1.isCouplingInflow(energyEqIdx1))
        {
            if (globalProblem.sdProblem2().isCornerPoint(globalPos2))
            {
                const Scalar convectiveFlux = normalFlux2[nPhaseIdx2] *
                    cParams.elemVolVarsCur2[vertInElem2].enthalpy(nPhaseIdx2)
                    +
                    normalFlux2[wPhaseIdx2] *
                    cParams.elemVolVarsCur2[vertInElem2].enthalpy(wPhaseIdx2);
                const Scalar conductiveFlux = boundaryVars2.normalMatrixHeatFlux();

                couplingRes1.accumulate(lfsu_s.child(energyEqIdx1), vertInElem1,
                                        -(convectiveFlux - conductiveFlux));
            }
            else
            {
                couplingRes1.accumulate(lfsu_s.child(energyEqIdx1), vertInElem1,
                                        globalProblem.localResidual2().residual(vertInElem2)[energyEqIdx2]);
            }
        }
    }
};
} // end namespace Dumux

#endif // DUMUX_NCNISTOKES2PNCNILOCALOPERATOR_HH
