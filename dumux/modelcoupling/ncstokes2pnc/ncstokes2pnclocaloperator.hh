/*****************************************************************************
 *   Copyright (C) 2013 by Vishal Jambhekar,  					 			 *
 *   Copyright (C) 2012 Katherina Baber, Klaus Mosthaf  					 *
 *   Copyright (C) 2009-2010 by Bernd Flemisch                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The local operator for the coupling of a n-component Stokes model
 *        and a two-phase n-component Darcy model under isothermal conditions.
 */
#ifndef DUMUX_NCSTOKES_TWOPNC_LOCALOPERATOR_HH
#define DUMUX_NCSTOKES_TWOPNC_LOCALOPERATOR_HH

#include <dune/pdelab/multidomain/couplingutilities.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/idefault.hh>

#include <dumux/freeflow/stokesnc/stokesncmodel.hh>
#include <dumux/implicit/2pnc/2pncmodel.hh>
#include <iostream>

namespace Dumux {

template<class TypeTag>
class NCStokesTwoPNCLocalOperator :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<NCStokesTwoPNCLocalOperator<TypeTag>>,
        public Dune::PDELab::MultiDomain::FullCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
 public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCouplingLocalOperator) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) StokesncTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPNCTypeTag;

    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Problem) StokesncProblem;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Problem) TwoPNCProblem;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Model) StokesncModel;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Model) TwoPNCModel;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, FluxVariables) BoundaryVariables2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, FVElementGeometry) FVElementGeometry2;

    // Multidomain Grid and Subgrid types
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;

    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, GridView) StokesncGridView;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, GridView) TwoPNCGridView;

    typedef typename StokesncGridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename TwoPNCGridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename GET_PROP_TYPE(StokesncTypeTag, Indices) StokesncIndices;
    typedef typename GET_PROP_TYPE(TwoPNCTypeTag, Indices) TwoPNCIndices;

    enum { dim = MultiDomainGrid::dimension };
    enum {
        numEq1 = GET_PROP_VALUE(StokesncTypeTag, NumEq),

        // indices in the Stokes domain
        momentumXIdx1 = StokesncIndices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx1 = StokesncIndices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx1 = StokesncIndices::momentumZIdx, //!< Index of the z-component of the momentum balance
        lastMomentumIdx1 = StokesncIndices::lastMomentumIdx, //!< Index of the last component of the momentum balance
        massBalanceIdx1 = StokesncIndices::massBalanceIdx, //!< Index of the mass balance
        transportEqIdx1 = StokesncIndices::transportEqIdx, //!< Index of the transport equation NOW: equations

        nPhaseIdx1 = StokesncIndices::phaseIdx,               //!< Index of the used phase of the fluidsystem
        transportCompIdx1 = StokesncIndices::transportCompIdx, //!< Index of vapor NOW: more components
        phaseCompIdx1 = StokesncIndices::phaseCompIdx          //!< Index of air (single phase)
    };
    enum {
        // indices in the Darcy domain
        numEq2 = GET_PROP_VALUE(TwoPNCTypeTag, NumEq),
        numPhases2 = GET_PROP_VALUE(TwoPNCTypeTag, NumPhases),
		numComponents = FluidSystem::numComponents,

		conti0EqIdx2 = TwoPNCIndices::conti0EqIdx,    //!< Reference Index of the continuity equations 
        contiWEqIdx2 = TwoPNCIndices::contiWEqIdx,    //!< Index of the continuity equation for water component NOW: not needed because all components need to be treated
        massBalanceIdx2 = TwoPNCIndices::contiNEqIdx, //!< Index of the total mass balance

        wCompIdx2 = FluidSystem::wCompIdx,          //!< Index of the liquids main component: here H2O
        nCompIdx2 = FluidSystem::nCompIdx,          //!< Index of the main component of the gas: here N2

        wPhaseIdx2 = FluidSystem::wPhaseIdx,        //!< Index for the liquid phase
        nPhaseIdx2 = FluidSystem::nPhaseIdx          //!< Index for the gas phase
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename StokesncGridView::template Codim<dim>::EntityPointer VertexPointer1;
    typedef typename TwoPNCGridView::template Codim<dim>::EntityPointer VertexPointer2;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;

    NCStokesTwoPNCLocalOperator(GlobalProblem& globalProblem)
        : globalProblem_(globalProblem)
    { }

    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

    /*!
     * \brief Do the coupling. The unknowns are transferred from dune-multidomain.
     *        Based on them, a coupling residual is calculated and added at the
     *        respective positions in the matrix.
     *
     */
    template<typename IntersectionGeom, typename LFSU1, typename LFSU2,
        typename X, typename LFSV1, typename LFSV2,typename RES>
        void alpha_coupling (const IntersectionGeom& intersectionGeometry,
                             const LFSU1& lfsu_s, const X& unknowns1, const LFSV1& lfsv_s,
                             const LFSU2& lfsu_n, const X& unknowns2, const LFSV2& lfsv_n,
                             RES& couplingRes1, RES& couplingRes2) const
    {
        const MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();

        const MultiDomainElement& mdElement1 = *mdElementPointer1;
        const MultiDomainElement& mdElement2 = *mdElementPointer2;

        // the subodmain elements
        const SubDomainElement1& sdElement1 = *(globalProblem_.sdElementPointer1(mdElement1));
        const SubDomainElement2& sdElement2 = *(globalProblem_.sdElementPointer2(mdElement2));

        // a container for the parameters on each side of the coupling interface (see below)
        CParams cParams;

        // update fvElementGeometry and the element volume variables
        updateElemVolVars(lfsu_s, lfsu_n,
                          unknowns1, unknowns2,
                          sdElement1, sdElement2,
                          cParams);

        // first element
        const int faceIdx1 = intersectionGeometry.indexInInside();
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
            Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
        const int numVerticesOfFace = referenceElement1.size(faceIdx1, 1, dim);

        // second element
        const int faceIdx2 = intersectionGeometry.indexInOutside();
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
            Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());

        // TODO: assumes same number of vertices on a coupling face
        for (int vertexInFace = 0; vertexInFace < numVerticesOfFace; ++vertexInFace)
        {
            const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, vertexInFace, dim);
            const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, vertexInFace, dim);

            const int boundaryFaceIdx1 = cParams.fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace);
            const int boundaryFaceIdx2 = cParams.fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace);

            // obtain the boundary types
            const VertexPointer1 vPtr1 = sdElement1.template subEntity<dim>(vertInElem1);
            const VertexPointer2 vPtr2 = sdElement2.template subEntity<dim>(vertInElem2);

            globalProblem_.sdProblem1().boundaryTypes(cParams.boundaryTypes1, *vPtr1);
            globalProblem_.sdProblem2().boundaryTypes(cParams.boundaryTypes2, *vPtr2);

            const BoundaryVariables1 boundaryVars1(globalProblem_.sdProblem1(),
                                                   sdElement1,
                                                   cParams.fvGeometry1,
                                                   boundaryFaceIdx1,
                                                   cParams.elemVolVarsCur1,
                                                   /*onBoundary=*/true);
            const BoundaryVariables2 boundaryVars2(globalProblem_.sdProblem2(),
                                                   sdElement2,
                                                   cParams.fvGeometry2,
                                                   boundaryFaceIdx2,
                                                   cParams.elemVolVarsCur2,
                                                   /*onBoundary=*/true);

            asImp_()->evalCoupling12(lfsu_s, lfsu_n, // local function spaces
                           vertInElem1, vertInElem2,
                           sdElement1, sdElement2,
                           boundaryVars1, boundaryVars2,
                           cParams,
                           couplingRes1, couplingRes2);
            asImp_()->evalCoupling21(lfsu_s, lfsu_n, // local function spaces
                           vertInElem1, vertInElem2,
                           sdElement1, sdElement2,
                           boundaryVars1, boundaryVars2,
                           cParams,
                           couplingRes1, couplingRes2);
        }
    }

    /*!
     * \brief Update the volume variables of the element and extract the unknowns from dune-pdelab vectors
     *        and bring them into a form which fits to dumux.
     */
    template<typename LFSU1, typename LFSU2, typename X, typename CParams>
        void updateElemVolVars (const LFSU1& lfsu_s, const LFSU2& lfsu_n,
                                const X& unknowns1, const X& unknowns2,
                                const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,
                                CParams &cParams) const
    {
        cParams.fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);
        cParams.fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);

        const int numVertsOfElem1 = sdElement1.template count<dim>();
        const int numVertsOfElem2 = sdElement2.template count<dim>();

        //bring the local unknowns x_s into a form that can be passed to elemVolVarsCur.update()
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol1(0.);
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol2(0.);
        elementSol1.resize(unknowns1.size());
        elementSol2.resize(unknowns2.size());

        for (int idx=0; idx<numVertsOfElem1; ++idx)
        {
            for (int eqIdx1=0; eqIdx1<numEq1; ++eqIdx1)
                elementSol1[eqIdx1*numVertsOfElem1+idx] = unknowns1(lfsu_s.child(eqIdx1),idx);
            for (int eqIdx2=0; eqIdx2<numEq2; ++eqIdx2)
                elementSol2[eqIdx2*numVertsOfElem2+idx] = unknowns2(lfsu_n.child(eqIdx2),idx);
        }
        for (unsigned int i = 0; i < elementSol1.size(); i++)
            Valgrind::CheckDefined(elementSol1[i]);
        for (unsigned int i = 0; i < elementSol2.size(); i++)
            Valgrind::CheckDefined(elementSol2[i]);

        // evaluate the local residual with the PDELab solution
        globalProblem_.localResidual1().evalPDELab(sdElement1, cParams.fvGeometry1, elementSol1,
                                                   cParams.elemVolVarsPrev1, cParams.elemVolVarsCur1);
        globalProblem_.localResidual2().evalPDELab(sdElement2, cParams.fvGeometry2, elementSol2,
                                                   cParams.elemVolVarsPrev2, cParams.elemVolVarsCur2);

        return;
    }

    /*!
     * \brief Evaluation of the coupling from Stokes to Darcy.
	 * 
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
        void evalCoupling12(const LFSU1& lfsu_s, const LFSU2& lfsu_n,
                            const int vertInElem1, const int vertInElem2,
                            const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,
                            const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                            const CParams &cParams,
                            RES1& couplingRes1, RES2& couplingRes2) const
    {
        const DimVector& globalPos1 = cParams.fvGeometry1.subContVol[vertInElem1].global;
        const DimVector& bfNormal1 = boundaryVars1.face().normal;
        const Scalar normalFlux1 = boundaryVars1.normalVelocity() *
            cParams.elemVolVarsCur1[vertInElem1].molarDensity();

        //rho*v*n as NEUMANN condition for porous medium (set, if B&J defined as NEUMANN condition)
        if (cParams.boundaryTypes2.isCouplingInflow(massBalanceIdx2))
        {
            if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
            {
                couplingRes2.accumulate(lfsu_n.child(massBalanceIdx2), vertInElem2,
                                        -normalFlux1);
            }
            else
            {
                couplingRes2.accumulate(lfsu_n.child(massBalanceIdx2), vertInElem2,
                                        globalProblem_.localResidual1().residual(vertInElem1)[massBalanceIdx1]);
            }
        }
        if (cParams.boundaryTypes2.isCouplingOutflow(massBalanceIdx2))
        {

            //TODO what happens at the corners? Idea: set only pressure in corners, so that residual is not needed there
            //pi-tau
            //        if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
            //        {
            //            couplingRes2[getIndex_<LFSU2,massBalanceIdx2> (lfsu_n,vertInElem2)] -= cParams.elemVolVarsCur1[vertInElem1].pressure;
            //        }
            //        else
            //        {
            //            // n.(pI-tau)n as dirichlet condition for Darcy p (set, if B&J defined as Dirichlet condition)
            // set residualDarcy[massBalance] = p in 2p2clocalresidual.hh
            couplingRes2.accumulate(lfsu_n.child(massBalanceIdx2), vertInElem2,
                                    globalProblem_.localResidual1().residual(vertInElem1)[momentumYIdx1]
                                    -cParams.elemVolVarsCur1[vertInElem1].pressure());
        }
		
		//loop over all components for coupling of the continuity equations with the Stokes' transport equations
		for (int compIdx; compIdx<numComponents; compIdx++)
		{	
			int eqIdx2 = conti0EqIdx2 + compIdx;
			if(eqIdx2!=massBalanceIdx2) //coupling for total mass balace is set above
			{
				
			if (cParams.boundaryTypes2.isCouplingInflow(eqIdx2))
			{
				if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
				{
					const Scalar advectiveFlux =
						normalFlux1 *
						cParams.elemVolVarsCur1[vertInElem1].fluidState().moleFraction(nPhaseIdx1, compIdx);
					const Scalar diffusiveFlux =
						bfNormal1 *
						boundaryVars1.moleFractionGrad(compIdx) *
						boundaryVars1.diffusionCoeff(compIdx) *
						boundaryVars1.molarDensity(); 

					couplingRes2.accumulate(lfsu_n.child(eqIdx2), vertInElem2,
                                        -(advectiveFlux - diffusiveFlux));
				}
				else
				{
					//Boundary layer model
					if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FreeFlow, UseBoundaryLayerModel))
					{
						DUNE_THROW(NumericalProblem, "Boundary layer model not implemented ");

					}
					else
					{	
						// the main component mass flux from the stokes domain
						if (compIdx<2)
						{
						couplingRes2.accumulate(lfsu_n.child(eqIdx2), vertInElem2,
												globalProblem_.localResidual1().residual(vertInElem1)[transportEqIdx1]);
						}
						// the secondary components' mass fluxes from the stokes domain
						else
						{
							int eqIdx1 = transportEqIdx1 - 1 + compIdx;
							couplingRes2.accumulate(lfsu_n.child(eqIdx2), vertInElem2,
                                            globalProblem_.localResidual1().residual(vertInElem1)[eqIdx1]);
						}
					}
				}
			}
		
			/*
			if (cParams.boundaryTypes2.isCouplingOutflow(eqIdx2))
				DUNE_THROW(NumericalProblem, "CouplingOutflow boundary type not implemented for 2pnc");
			 //{NOT IMPLEMENTED
				// set residualDarcy[contiWEqIdx2] = X in 2p2clocalresidual.hh?
				couplingRes2.accumulate(lfsu_n.child(eqIdx2), vertInElem2,
                                    -cParams.elemVolVarsCur1[vertInElem1].fluidState().moleFraction(nPhaseIdx1, compIdx));
			}*/
		
			}
		}
		
        return;
    }

    /*!
     * \brief Evaluation of the coupling from Darcy to Stokes.
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
        void evalCoupling21(const LFSU1& lfsu_s, const LFSU2& lfsu_n,
                            const int vertInElem1, const int vertInElem2,
                            const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,
                            const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                            const CParams &cParams,
                            RES1& couplingRes1, RES2& couplingRes2) const
    {
        const DimVector& globalPos2 = cParams.fvGeometry2.subContVol[vertInElem2].global;
        const DimVector& bfNormal2 = boundaryVars2.face().normal;
        DimVector normalFlux2(0.);

        // velocity*normal*area*rho
        for (int phaseIdx=0; phaseIdx<numPhases2; ++phaseIdx)
            normalFlux2[phaseIdx] = -boundaryVars2.volumeFlux(phaseIdx)*
                cParams.elemVolVarsCur2[vertInElem2].molarDensity(phaseIdx);

        //p*n as NEUMANN condition for free flow (set, if B&J defined as NEUMANN condition)
        if (cParams.boundaryTypes1.isCouplingOutflow(momentumYIdx1))
        {
            //p*A*n as NEUMANN condition for free flow (set, if B&J defined as NEUMANN condition)
            //pressure correction in stokeslocalresidual.hh
            couplingRes1.accumulate(lfsu_s.child(momentumYIdx1), vertInElem1,
                                    cParams.elemVolVarsCur2[vertInElem2].pressure(nPhaseIdx2) *
                                    bfNormal2.two_norm());
        }
        if (cParams.boundaryTypes1.isCouplingInflow(momentumYIdx1))
        {

            // v.n as Dirichlet condition for the Stokes domain
            // set residualStokes[momentumYIdx1] = vy in stokeslocalresidual.hh
            if (globalProblem_.sdProblem2().isCornerPoint(globalPos2))
            {
                couplingRes1.accumulate(lfsu_s.child(momentumYIdx1), vertInElem1,
                                        -((normalFlux2[nPhaseIdx2] + normalFlux2[wPhaseIdx2])
                                          / cParams.elemVolVarsCur1[vertInElem1].molarDensity()));
            }
            else
            {
                // v.n as DIRICHLET condition for the Stokes domain (negative sign!)
                couplingRes1.accumulate(lfsu_s.child(momentumYIdx1), vertInElem1,
                                        globalProblem_.localResidual2().residual(vertInElem2)[massBalanceIdx2]
                                        / cParams.elemVolVarsCur1[vertInElem1].molarDensity());
                // TODO: * bfNormal2.two_norm());
            }
        }

        //coupling residual is added to "real" residual
        //here each node is passed twice, hence only half of the dirichlet condition has to be set
        //TODO what to do at boundary nodes which appear only once?(vertInElem1)[transportEqIdx1];
		
		//coupling for transported components
		for (int compIdx=0; compIdx<numComponents; compIdx++)
		{
            int eqIdx1 = dim + compIdx;
            if (eqIdx1 != massBalanceIdx1) {
				if (cParams.boundaryTypes1.isCouplingOutflow(eqIdx1))
				{
					// set residualStokes[transportEqIdx1+compIdx] = x in stokes2clocalresidual.hh
					couplingRes1.accumulate(lfsu_s.child(eqIdx1), vertInElem1,
                                    -cParams.elemVolVarsCur2[vertInElem2].fluidState().moleFraction(nPhaseIdx2, compIdx));
				}
            }
		}
		return;
    }

protected:

    GlobalProblem& globalProblem() const
    { return globalProblem_; }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

private:
    // a struct that contains data of FF and PM
    // including boundary types, volume variables in both subdomains
    // and geometric information
    struct CParams
    {
        BoundaryTypes1 boundaryTypes1;
        BoundaryTypes2 boundaryTypes2;
        ElementVolumeVariables1 elemVolVarsPrev1;
        ElementVolumeVariables1 elemVolVarsCur1;
        ElementVolumeVariables2 elemVolVarsPrev2;
        ElementVolumeVariables2 elemVolVarsCur2;
        FVElementGeometry1 fvGeometry1;
        FVElementGeometry2 fvGeometry2;
    };

    GlobalProblem& globalProblem_;
};

} // end namespace Dumux

#endif // DUMUX_NCSTOKES2PNCLOCALOPERATOR_HH
