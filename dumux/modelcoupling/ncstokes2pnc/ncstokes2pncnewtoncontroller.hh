/****************************************************************************
 *   Copyright (C) 2010 by Klaus Mosthaf                                     *
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2010 by Bernd Flemisch                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Reference implementation of a newton controller for coupled ncStokes - 2pnc problems.
 */
#ifndef DUMUX_NCSTOKES_2PNC_NEWTON_CONTROLLER_HH
#define DUMUX_NCSTOKES_2PNC_NEWTON_CONTROLLER_HH

#include <dumux/multidomain/common/multidomainnewtoncontroller.hh>

/*!
 * \file
 */
namespace Dumux
{

/*!
 * \brief Implementation of a newton controller for ncstokes2pnc problems.
 */
template <class TypeTag>
class NCStokesTwoPNCNewtonController : public MultiDomainNewtonController<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;

    typedef MultiDomainNewtonController<TypeTag> ParentType;

    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;

public:
    NCStokesTwoPNCNewtonController(const Problem &problem)
        : ParentType(problem)
    {  }

    /*!
     * \brief Indicates that one Newton iteration was finished.
     */
    void newtonEndStep(SolutionVector &uCurrentIter, SolutionVector &uLastIter)
    {
        ParentType::newtonEndStep(uCurrentIter, uLastIter);

        this->model_().sdModel2().updateStaticData(this->model_().sdModel2().curSol(),
                                                   this->model_().sdModel2().prevSol());
    }
};

} // namespace Dumux


#endif
