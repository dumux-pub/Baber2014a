/*****************************************************************************
 *   Copyright (C) 2012 by Katherina Baber                                   *
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version, as long as this copyright notice    *
 *   is included in its original form.                                       *
 *                                                                           *
 *   This program is distributed WITHOUT ANY WARRANTY.                       *
 *****************************************************************************/
/*!
 * \file
 * \brief Coupling operator for coupling Stokes2c with two-phase Darcy2c using mortar elements
 * and accounting for drop formation at the interface.
 */
#ifndef DUMUX_MORTAR2CSTOKES2P2CLOCALOPERATOR_HH
#define DUMUX_MORTAR2CSTOKES2P2CLOCALOPERATOR_HH

#include <dune/pdelab/multidomain/couplingutilities.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/idefault.hh>

#include <dumux/freeflow/stokesnc/stokesncmodel.hh>
#include <dumux/multidomain/couplinglocalresiduals/2p2ccouplinglocalresidual.hh>
#include <dumux/modelcoupling/common/multidomainenrichedintegrationpointgeometry.hh>
#include <dumux/modelcoupling/common/coupledmortardropsproblem.hh>
#include <iostream>

namespace Dumux {
/*!
 * \brief Coupling operator for coupling Stokes2c with two-phase Darcy2c using mortar elements
 * and accounting for drop formation at the interface.
 */
template<class TypeTag>
class MortarTwoCStokesTwoPTwoCDropsLocalOperator :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianEnrichedCouplingToFirstSubProblem<MortarTwoCStokesTwoPTwoCDropsLocalOperator<TypeTag> >,
        public Dune::PDELab::MultiDomain::NumericalJacobianEnrichedCouplingToSecondSubProblem<MortarTwoCStokesTwoPTwoCDropsLocalOperator<TypeTag> >,
        public Dune::PDELab::MultiDomain::NumericalJacobianEnrichedCoupling<MortarTwoCStokesTwoPTwoCDropsLocalOperator<TypeTag> >,
        public Dune::PDELab::MultiDomain::FullEnrichedCouplingFirstSubProblemPattern,
        public Dune::PDELab::MultiDomain::FullEnrichedCouplingSecondSubProblemPattern,
        public Dune::PDELab::MultiDomain::FullEnrichedCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>

{
public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Stokes2cDropsTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCDropsTypeTag;

    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, FluxVariables) BoundaryVariables2;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, MaterialLawParams) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, MaterialLaw) MaterialLaw;

    // Multidomain Grid and Subgrid types
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) MultiDomainGridView;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;
    typedef typename MultiDomainGrid::LeafGridView::IndexSet::IndexType IndexType;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, GridView) Stokes2cDropsGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, GridView) TwoPTwoCDropsGridView;

    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename SubDomainGrid::Traits::template Codim<0>::EntityPointer SubDomainElementPointer;
    typedef typename Stokes2cDropsGridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename TwoPTwoCDropsGridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, Indices) Stokes2cDropsIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, Indices) TwoPTwoCDropsIndices;

    typedef typename GET_PROP_TYPE(Stokes2cDropsTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPTwoCDropsTypeTag, BoundaryTypes) BoundaryTypes2;

    enum{ // common indices
        dim = MultiDomainGrid::dimension,
        dimWorld = MultiDomainGrid::dimensionworld,

        numEq1 = GET_PROP_VALUE(Stokes2cDropsTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(TwoPTwoCDropsTypeTag, NumEq)
    };
    //indices in the Stokes domain
    enum{
        // equation indices
        momentumXIdx1 = Stokes2cDropsIndices::momentumXIdx,        //!< Index of the x-component of the momentum balance
        momentumYIdx1 = Stokes2cDropsIndices::momentumYIdx,        //!< Index of the y-component of the momentum balance
        momentumZIdx1 = Stokes2cDropsIndices::momentumZIdx,        //!< Index of the z-component of the momentum balance
        lastMomentumIdx1 = Stokes2cDropsIndices::lastMomentumIdx,  //!< Index of the last component of the momentum balance

        massBalanceIdx1 = Stokes2cDropsIndices::massBalanceIdx,    //!< Index of the mass balance
        transportEqIdx1 = Stokes2cDropsIndices::transportEqIdx,        //!< Index of the transport equation
    };
    enum {
        //phases and components
        nPhaseIdx1 = Stokes2cDropsIndices::phaseIdx,               //!< Index of the gas phase
        transportCompIdx1 = Stokes2cDropsIndices::transportCompIdx, //!< Index of the transport equation
    };
    enum{
        // primary variable indices
        velocityXIdx1 = Stokes2cDropsIndices::velocityXIdx,
        velocityYIdx1 = Stokes2cDropsIndices::velocityYIdx,
        pressureIdx1 = Stokes2cDropsIndices::pressureIdx,
        massOrMoleFracIdx1 = Stokes2cDropsIndices::massOrMoleFracIdx
    };
    // indices in the  Darcy domain
    enum{
        // equation indices
        transportEqIdx = TwoPTwoCDropsIndices::contiWEqIdx,
        massBalanceIdx2 = GET_PROP_VALUE(TwoPTwoCDropsTypeTag, ReplaceCompEqIdx),
    };
    enum{
        // primary variable indices
        pressureIdx2 = TwoPTwoCDropsIndices::pnIdx,
        switchIdx2 = TwoPTwoCDropsIndices::swOrXIdx
    };
    enum{
        //phases and components
        numPhases2 = GET_PROP_VALUE(TwoPTwoCDropsTypeTag, NumPhases),

        wCompIdx2 = TwoPTwoCDropsIndices::wCompIdx,
        nCompIdx2 = TwoPTwoCDropsIndices::nCompIdx,

        wPhaseIdx2 = TwoPTwoCDropsIndices::wPhaseIdx,
        nPhaseIdx2 = TwoPTwoCDropsIndices::nPhaseIdx
    };
    enum { // the indices for the phase presence
        wPhaseOnly = TwoPTwoCDropsIndices::wPhaseOnly,
        nPhaseOnly = TwoPTwoCDropsIndices::nPhaseOnly,
        bothPhases = TwoPTwoCDropsIndices::bothPhases
    };
    // indices on the mortar elements
#if COUPLINGCONCEPT // with storage term
    enum{
    //        lambdaBJ= 0, //!< horizontal free flow velocity used for B&J
    lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
    lambdaP = 1, //!< pressure in porous medium used for Neumann coupling condition for normal Stokes flux
    lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
    rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume
};
#else // with pore velocity
    enum{
        //        lambdaBJ= 0, //!< horizontal free flow velocity used for B&J
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neumann coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume
        pDrop = 4 //!< lagrange multiplier: drop water pressure
    };
#endif

    //defines which of the component-balance equations in the pm is replaced by the total mass balance
    static constexpr unsigned int replaceCompEqIdx =
            GET_PROP_VALUE(TypeTag, ReplaceCompEqIdx);

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;

    typedef typename Stokes2cDropsGridView::template Codim<dim>::EntityPointer VertexPointer1;
    typedef typename TwoPTwoCDropsGridView::template Codim<dim>::EntityPointer VertexPointer2;

    //type definitions needed access geometry information of IPs store in a map connecting a vector with the intersection index
    typedef std::vector<IntegrationPointGeometry> IntegrationPointGeometryVector;
    typedef std::vector<IntegrationPointGeometryCoupling> IntegrationPointGeometryVectorCoupling;
    typedef std::map<IndexType, IntegrationPointGeometryVector> IntegrationPointGeometryMap;
    typedef std::map<IndexType, IntegrationPointGeometryVectorCoupling> IntegrationPointGeometryMapCoupling;
    typedef typename std::pair<IndexType,IntegrationPointGeometryVector> IntegrationPointGeometryIntersectionPair;
    typedef typename IntegrationPointGeometryMap::const_iterator IntegrationPointGeometryMapIterator;
    typedef typename IntegrationPointGeometryMapCoupling::const_iterator IntegrationPointGeometryMapCouplingIterator;


    //type definitions for drop information
    typedef typename GET_PROP_TYPE(TypeTag, DropMap) DropMap;
    typedef typename DropMap::const_iterator DropMapIterator;

    MortarTwoCStokesTwoPTwoCDropsLocalOperator(GlobalProblem& globalProblem)
    : globalProblem_(globalProblem), refPressure_(100000)
    {
        tubeModelThickness_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FuelCell, TubeModelThickness);
    }

    static const bool doAlphaEnrichedCouplingToSubProblems = true;
    static const bool doAlphaEnrichedCoupling = true;
    static const bool doPatternEnrichedCouplingToSubProblems = true;
    static const bool doPatternEnrichedCoupling = true;

    /*!
     * \brief Do the coupling from 1st problem to mortar elements.
     *        The unknowns are transferred from dune-multidomain.
     *        Based on them, a coupling residual is calculated and added at the
     *        respective positions in the matrix.
     *        To couple Stokes and Darcy, Stokes needs conditions for
     *        vx (BeaversJoseph),
     *        vy (MomentumBoundaryFlux is substituted by with porous medium pressure
     *        which is now claculated on the mortar elements),
     *        and x (coupled like before via lagrange multiplier and x1=x2).
     */
    template<typename IntersectionGeom, typename LFSU1, typename X, typename LFSV1,
    typename LFSU_C, typename LFSV_C,typename RES>
    void alpha_enriched_coupling_first (const IntersectionGeom& intersectionGeometry,
            const LFSU1& lfsu_s, const X& unknowns1, const LFSV1& lfsv_s,
            const LFSU_C& lfsu_c, const X& lambda, const LFSV_C& lfsv_c,
            RES& couplingRes1C, RES& couplingResC1) const //
    {
        //type definition for xvelocity space
        typedef typename LFSU1::template Child<velocityXIdx1>::Type LFSU1_VelocityX;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU1_VelocityX::Traits::FiniteElementType> LFSU1_FESwitch_VelocityX;
        typedef Dune::BasisInterfaceSwitch<typename LFSU1_FESwitch_VelocityX::Basis> LFSU1_BasisSwitch_VelocityX;
        //type definition for yvelocity space
        typedef typename LFSU1::template Child<velocityYIdx1>::Type LFSU1_VelocityY;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU1_VelocityY::Traits::FiniteElementType> LFSU1_FESwitch_VelocityY;
        typedef Dune::BasisInterfaceSwitch<typename LFSU1_FESwitch_VelocityY::Basis> LFSU1_BasisSwitch_VelocityY;
        //type definition for mass fraction space
        typedef typename LFSU1::template Child<massOrMoleFracIdx1>::Type LFSU1_MassFraction;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU1_MassFraction::Traits::FiniteElementType> LFSU1_FESwitch_MassFraction;
        //type definition for gas pressure space
        typedef typename LFSU1::template Child<pressureIdx1>::Type LFSU1_GasPressure;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU1_GasPressure::Traits::FiniteElementType> LFSU1_FESwitch_GasPressure;

        //type definitions for mortar xvelocity space
        //        typedef typename LFSU_C::template Child<lambdaBJ>::Type LFSU_C_velocityX;
        //        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_velocityX::Traits::FiniteElementType> LFSU_C_FESwitch_velocityX;
        //type definitions for mortar yvelocity space
        typedef typename LFSU_C::template Child<lambdaP>::Type LFSU_C_lambdaP;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_lambdaP::Traits::FiniteElementType> LFSU_C_FESwitch_lambdaP;
        //type definitions for mortar component flux space
        typedef typename LFSU_C::template Child<lambdaX>::Type LFSU_C_MassFraction;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_MassFraction::Traits::FiniteElementType> LFSU_C_FESwitch_MassFraction;

        //common type definitions
        typedef typename LFSU1_BasisSwitch_VelocityY::DomainField DomainFieldType;
        typedef typename LFSU1_BasisSwitch_VelocityY::DomainField RangeFieldType;
        typedef typename LFSU1_BasisSwitch_VelocityY::Range RangeType;
        typedef typename LFSU1::Traits::SizeType SizeType;

        for(int i=0; i<unknowns1.size(); i++) Valgrind::CheckDefined(unknowns1(lfsu_s,i));
        for(int i=0; i<lambda.size(); i++) Valgrind::CheckDefined(lambda(lfsu_c,i));

        /*********************************                   prepare coupling                   ******************************************************************/
        const MultiDomainGridView& mdGridView = globalProblem_.mdGridView();
        //neighboring element of intersection in subdomain1
        const MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        const MultiDomainElement& mdElement1 = *mdElementPointer1;
        const SubDomainElementPointer sdElementPointer1 = (globalProblem_.sdElementPointer1(mdElement1));
        const SubDomainElement1& sdElement1 = *sdElementPointer1;
        BoundaryTypes1 boundaryTypes1;
        int faceIdx1 = intersectionGeometry.indexInInside();

        // update fvElementGeometry and the element volume variables
        FVElementGeometry1 fvElemGeom1;
        ElementVolumeVariables1 elemVolVarsCur1;
        updateElemVolVarsFirst(lfsu_s,
                unknowns1,
                mdElement1,
                sdElement1,
                fvElemGeom1,
                elemVolVarsCur1);

        //type definitions to store integration-point-geometry information determined in mortarGeometry
        int vertexInElement, vertexInFace; //vertex at which coupling information is written in matrix
        Scalar weight; //fraction of the interface area corresponding to the integration point
        DimVector integrationPointLocalInElement; //evaluate shape functions in element
        Dune::FieldVector<DomainFieldType, dim-1> integrationPointLocalInFace; //evaluate shape function on intersection

        //get global index of the current intersection
        IndexType globalIntersectionIndex;
        if((*intersectionGeometry.inside()).level() == (*intersectionGeometry.outside()).level() || (*intersectionGeometry.inside()).level() > (*intersectionGeometry.outside()).level())
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.inside(), intersectionGeometry.indexInInside(), 1);
        else
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.outside(), intersectionGeometry.indexInOutside(), 1);

        //loop over vertices of face / intersection
        for (int vertexInIntersection = 0; vertexInIntersection < lfsu_c.child(massBalanceIdx1).size(); ++vertexInIntersection)
        {
            //get integration point geometry from map using the global intersection index
            const IntegrationPointGeometryMap& integrationPointGeometryMapFirst = globalProblem_.integrationPointGeometryMapFirst();
            const IntegrationPointGeometryMapIterator mapIt =  integrationPointGeometryMapFirst.find(globalIntersectionIndex);
            if (mapIt == globalProblem_.integrationPointGeometryMapFirst().end()) {
                std::cerr << "Geometry information of IPs for domain one not found in coupling operator\n";
            }
            vertexInElement = mapIt->second[vertexInIntersection].vertexInElement;
            vertexInFace = mapIt->second[vertexInIntersection].vertexInFace;
            weight = mapIt->second[vertexInIntersection].weight;
            integrationPointLocalInElement = mapIt->second[vertexInIntersection].integrationPointLocalInElement;
            integrationPointLocalInFace = mapIt->second[vertexInIntersection].integrationPointLocalInFace;

            // obtain the boundary types
            const VertexPointer1 vPtr1 = sdElement1.template subEntity<dim>(vertexInElement);
            globalProblem_.sdProblem1().boundaryTypes(boundaryTypes1, *vPtr1);

            //get drop information for intersection from dropmap (filled in the coupled problem)
            IndexType globalVertexIndex = mdGridView.indexSet().subIndex(mdElement1, vertexInElement, dim);
            const DropMap& dropMap = globalProblem_.dropMap();
            const DropMapIterator dropMapIt =  dropMap.find(globalVertexIndex);
            if (dropMapIt == globalProblem_.dropMap().end()) {
                std::cerr << "drop information of current vertex "<< globalVertexIndex<<" not found in coupling operator enriched coupling first\n";
            }
            const DropInformation& dropInfo = dropMapIt->second;

            /*********************************              do coupling: mortar to Stokes (1C)         ******************************************************************/
            //Integration using midpoint rule -> evaluation of Mortar-Ansatzfunctions at boundaryIP=integrationPointLocalInFace
            //            std::vector<RangeType> phiVx(lfsu_c.child(lambdaBJ).size());
            std::vector<RangeType> phiP(lfsu_c.child(lambdaP).size());
            std::vector<RangeType> phiX(lfsu_c.child(lambdaX).size());

            // evaluate Mortar Ansatzfunctions at IP in intersection
            //            LFSU_C_FESwitch_velocityX::basis(lfsu_c.child(lambdaBJ).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiVx);
            LFSU_C_FESwitch_lambdaP::basis(lfsu_c.child(lambdaP).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiP);
            LFSU_C_FESwitch_MassFraction::basis(lfsu_c.child(lambdaX).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiX);
            //Lagrange multiplier lambda * mortar Basis functions phi, weighting function = 1 due to box method

            RangeFieldType lambdaVxPhi(0.), lambdaPPhi(0.), lambdaXPhi(0.);

            //            for(SizeType i=0; i<lfsu_c.child(lambdaBJ).size(); i++)
            //                lambdaVxPhi += lambda(lfsu_c.child(lambdaBJ),i)*phiVx[i]; //lambda = Beaver-Joseph velocity

            for(SizeType i=0; i<lfsu_c.child(lambdaP).size(); i++)
                lambdaPPhi += lambda(lfsu_c.child(lambdaP),i)*phiP[i]; //lambda = p_mortar = p_Darcy = pPM

            for(SizeType i=0; i<lfsu_c.child(lambdaX).size(); i++)
                lambdaXPhi += lambda(lfsu_c.child(lambdaX),i)*phiX[i];  //lambda = Lagrange multiplier for component flux

            //multiply lambdaPhi with area of scvf / fraction of intersectionArea for midpoint rule
            //            lambdaVxPhi *= weight;
            lambdaPPhi *= weight;
            lambdaXPhi *= weight;

            //fill coupling residual in matrix: 1C (top right) = Stokes-Residual
            //            couplingRes1C.accumulate(lfsv_s.child(momentumXIdx1), vertexInElement, lambdaVxPhi);//Beavers-Joseph condition
            if(boundaryTypes1.isMortarCoupling(momentumYIdx1))
                couplingRes1C.accumulate(lfsv_s.child(momentumYIdx1), vertexInElement, lambdaPPhi);  //Mortar-pressure as condition for Stokes-y
            //lambda includes normal vector and minus sign
            if(boundaryTypes1.isMortarCoupling(transportEqIdx1))
                couplingRes1C.accumulate(lfsv_s.child(transportEqIdx1), vertexInElement, lambdaXPhi); //Lagrange multiplier to couple component fluxes

            /*********************************              do coupling: Stokes to mortar (C1)         ******************************************************************/
            //to be consistent with the rest, apply BoxMethod also on mortar elements
            //Hence weighting function phi=1 and no quadrature rule is needed
            //evaluate Ansatz functions on element at IP
            std::vector<RangeType> Nvx(lfsu_s.child(velocityXIdx1).size());
            std::vector<RangeType> Nvy(lfsu_s.child(velocityYIdx1).size());
            std::vector<RangeType> Nx(lfsu_s.child(massOrMoleFracIdx1).size());
            std::vector<RangeType> Np(lfsu_s.child(pressureIdx1).size());
            //evaluate Ansatzfunctions on Domain1 elements at IP
            LFSU1_FESwitch_VelocityX::basis(lfsu_s.child(velocityXIdx1).finiteElement()).evaluateFunction(integrationPointLocalInElement,Nvx);
            LFSU1_FESwitch_VelocityY::basis(lfsu_s.child(velocityYIdx1).finiteElement()).evaluateFunction(integrationPointLocalInElement,Nvy);
            LFSU1_FESwitch_MassFraction::basis(lfsu_s.child(massOrMoleFracIdx1).finiteElement()).evaluateFunction(integrationPointLocalInElement,Nx);
            LFSU1_FESwitch_GasPressure::basis(lfsu_s.child(pressureIdx1).finiteElement()).evaluateFunction(integrationPointLocalInElement,Np);

            //Stokes primary varibales times Ansatzfunctions, weighting function = 1 due to box method
            RangeFieldType velocityXN(0.), velocityYN(0.), moleFractionN(0.), gasPressureN(0.), massFlux(0), dropFlux(0.);

            //x-Velocity -> Beavers-Joseph condition
            for(SizeType i=0; i<lfsu_s.child(velocityXIdx1).size(); i++)
                velocityXN += unknowns1(lfsu_s.child(velocityXIdx1),i)*Nvx[i]; //needed for Beavers-Joseph condition

            //y-velocity -> total mass flux for pm; outflow-condition for ff must be modified to account for increasd flux due to drop surface evaporation
            for(SizeType i=0; i<lfsu_s.child(velocityYIdx1).size(); i++)
                velocityYN += unknowns1(lfsu_s.child(velocityYIdx1),i)*Nvy[i];

            //            //implement Beavers-Joseph condition (condition for momentumXIdx)
            //            Scalar beaversJosephCoeff = globalProblem_.sdProblem1().beaversJosephCoeff(sdElement1,
            //                                                                                       fvElemGeom1,
            //                                                                                       vertexInElement,
            //                                                                                       intersectionGeometry.indexInInside()/*right boundaryFaceIdx?*/);
            //
            //            const Scalar Kxx = globalProblem_.sdProblem1().permeability(sdElement1,
            //                                                                       fvElemGeom1,
            //                                                                       vertexInElement);
            //            beaversJosephCoeff /= sqrt(Kxx);
            //
            //unit outer normal of intersection always points from inside-Element (1) to outside-Element (2) -> is the right direction
            const DimVector& unitInsideNormal = intersectionGeometry.intersection().centerUnitOuterNormal();
            DimVector velocity; //filled with velocities at integration point calculated above
            velocity[0] = velocityXN;
            velocity[1] = velocityYN;
            //            const Scalar normalComp = velocity*elementUnitNormal;
            //            DimVector normalVelocity = elementUnitNormal;
            //            normalVelocity *= normalComp; // v*n*n
            //            // v_tau = v - (v.n)n
            //            const DimVector tangentialVelocity = velocity - normalVelocity;
            //
            //            Scalar velocityBJ(0.);
            //            for (int dimIdx=0; dimIdx < dim; ++dimIdx)
            //            {
            //               velocityBJ += beaversJosephCoeff *
            //                           tangentialVelocity[dimIdx] *
            //                           elemVolVarsCur1[vertexInElement].viscosity(); //ToDO use boundaryVars
            ////                velocityBJ += elemVolVarsCur1[vertexInElement].pressure()*elementUnitNormal[dimIdx];
            //            }
            //            //apply midpoint rule for integration and multiply with bf-area / fraction of intersectionArea
            ////            velocityBJ *= weight;

            //mole/massfraction -> additional constraint: continuity of mass fractions
            for(SizeType i=0; i<lfsu_s.child(massOrMoleFracIdx1).size(); i++)
                moleFractionN += unknowns1(lfsu_s.child(massOrMoleFracIdx1),i)/*massfraction!*/ *Nx[i];
            //transform mass to mole fractions
            moleFractionN *= elemVolVarsCur1[vertexInElement].fluidState().averageMolarMass(nPhaseIdx1)/FluidSystem::molarMass(transportCompIdx1);
            //on the drop, Raoult is assumed for chemical equilibrium: xwgFF * pgFF = psat * xwwPM. Assemble the left part here.
            for(SizeType i=0; i<lfsu_s.child(pressureIdx1).size(); i++)
                gasPressureN += unknowns1(lfsu_s.child(pressureIdx1),i)*Np[i];

#if !COUPLINGCONCEPT //with pore velocity
            //calculate internal drop pressure: pw = pg + 2gamma/r
            Scalar surfaceTension = FluidSystem::surfaceTension(elemVolVarsCur1[vertexInElement].fluidState());
            Scalar dropWaterPressure = 0.0;
            if(!dropInfo.detached)
                dropWaterPressure =  gasPressureN + 2*surfaceTension / dropInfo.dropRadius * dropInfo.dropSurface/ dropInfo.areaDrop;//sign is positve because internal drop pressure should be bigger than outside pressure (22.2.13)
#endif

            //on the drop, Raoult is assumed for chemical equilibrium: xwgFF * pgFF = psat * xwwPM. Next to the drop, xwg couples directy.
            //Assemble the left part here. The right part is assembled in alpha_enriched_coupling_second(...).C2
            if(globalProblem_.sdProblem2().model().phasePresence(globalVertexIndex, /*isOldSol=*/false) != wPhaseOnly)
            {
              moleFractionN *= (dropInfo.areaGas + gasPressureN / refPressure_ * dropInfo.dropSurface);
            }
            else  //if the gas phase disappears in the porous medium, couple to the water phase by equilibrium
            {
              moleFractionN *= gasPressureN / refPressure_ * (dropInfo.areaGas + dropInfo.dropSurface);
            }
            moleFractionN *= weight; //weak formulation: integration over area

            //flux continuity condition for mass balance in porous medium: v*n*rho*A
            massFlux = velocity * unitInsideNormal * elemVolVarsCur1[vertexInElement].density() * weight;
            //            std::cout<<"mass flux from drop into ff: "<<massFlux<<std::endl;

            //dropflux -> evaporative flux from drop = flux into ff (rho * vy)
            const int boundaryFaceIdx = fvElemGeom1.boundaryFaceIndex(faceIdx1, vertexInFace);
            BoundaryVariables1 boundaryVars1(globalProblem_.sdProblem1(),
                    sdElement1,
                    fvElemGeom1,
                    boundaryFaceIdx,
                    elemVolVarsCur1,
                    true/*onBoundary*/);

            //Todo check signs: the calculated flux should be positiv and is then substracted from the matrix/drop

            //component flux (per m)
            Scalar waterDensity = 1000;/* approximate value for water density*/ //ToDo use FluidSystem (complicated since information about water is not available)
            Scalar kelvinVaporPressure = FluidSystem::kelvinVaporPressure(elemVolVarsCur1[vertexInElement].fluidState(),
                                                                     waterDensity/* approximate value for water density*/, //ToDo use FluidSystem
                                                                     wPhaseIdx2, wCompIdx2,
                                                                     dropInfo.dropRadius);
            Scalar xH20Sat = kelvinVaporPressure / gasPressureN * 1; //assume mole fraction of water in drop water xww to be one
            Scalar xH20inf = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialMassFraction); //ToDo use massFraction at GC ceiling
            xH20inf *= elemVolVarsCur1[vertexInElement].fluidState().averageMolarMass(nPhaseIdx1) / FluidSystem::molarMass(transportCompIdx1);
            Scalar gasChannelHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            Scalar dropHeight = dropInfo.dropRadius * (1 - std::cos(globalProblem_.contactAngle()));
            //3D spherical drop from literature
            //            Scalar f = - 0.00008957 + 0.633 * globalProblem_.contactAngle() + 0.166 * std::pow(globalProblem_.contactAngle(),2.0) - 0.08878 * std::pow(globalProblem_.contactAngle(),3.0)
            //                        + 0.01033 * std::pow(globalProblem_.contactAngle(),4.0); //after Pickett and Bexon 1977
            //            Scalar f = - cos(globalProblem_.contactAngle())/ (2 * log(1 - cos(globalProblem_.contactAngle()))); //Shanahan et al 1995 //ToDo develop 2D factor
            //            Scalar f = (1 - cos(globalProblem_.contactAngle()) / 2;//Rowan et al. (constant contact angle)
            //total evaporative flux for drop
            //            dropFlux = 4 * M_PI * boundaryVars1.diffusionCoeff()
            //                        * elemVolVarsCur1[vertexInElement].molarDensity() * dropInfo.dropRadius * (xH20Sat - xH20inf) * f;
            //                dropFlux *= FluidSystem::molarMass(transportCompIdx1);
            //                dropFlux /= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FuelCell, NodesPerDrop);
            //                dropFlux *= 0.5;

            //2D zylindrical drop (same reasoning as in literature)
            //                Scalar f = 1 / log(gasChannelHeight/dropInfo.dropRadius);
            //                //total evaporative flux for drop
            //                dropFlux = -2 * globalProblem_.contactAngle() * boundaryVars1.diffusionCoeff()
            //                * (xH20inf - xH20Sat) * f * elemVolVarsCur1[vertexInElement].molarDensity();
            //                //divide by number of cells per drop and multiply with 0.5 so that it is not assembled twice
            //                dropFlux *= FluidSystem::molarMass(transportCompIdx1);
            //                dropFlux /= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FuelCell, NodesPerDrop);
            //                dropFlux *= 0.5;

            //2D: derived by integrating component transport flux
            Scalar advection = 2 * dropInfo.dropRadius * elemVolVarsCur1[vertexInElement].molarDensity() * xH20Sat
                    *(velocity[0] * (1 - cos(globalProblem_.contactAngle())) + velocity[1] * sin(globalProblem_.contactAngle())) /* *1m */;
            //approximate gradient in x and y direction the same way (Rainer suggested that as well)
            Scalar molarGradient = (xH20inf - xH20Sat) / (gasChannelHeight - dropHeight);
            Scalar diffusion = 2 * dropInfo.dropRadius * elemVolVarsCur1[vertexInElement].molarDensity() *  boundaryVars1.diffusionCoeff(transportCompIdx1) * molarGradient
                    *((1 - std::cos(globalProblem_.contactAngle())) + std::sin(globalProblem_.contactAngle()));
            dropFlux += (advection - diffusion);
            dropFlux *= FluidSystem::molarMass(transportCompIdx1);
            dropFlux /= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FuelCell, NodesPerDrop);
            dropFlux *= 0.5;

            //flux from drop can only be positive, since gas cannot flow into the drop and can only occur if drop is present
//            if (dropFlux < 0.0 || dropInfo.detached)
                dropFlux = 0.0;

            //fill coupling residual in matrix: C1 (bottom left) = mortar-residual
            //            couplingResC1.accumulate(lfsv_c.child(lambdaBJ), vertexInIntersection, velocityBJ); //vxmortar=BJ-velocity
            //always specify equation for lambdaVy, if it is used is decided in the porous-medium section
            couplingResC1.accumulate(lfsv_c.child(lambdaVy), vertexInIntersection, massFlux); //rho*vy1=rho*vyFF (Stokes velocity = velocity on mortar elements)
            //only couple the mass-fractions if the coupling condition is set
            if(boundaryTypes1.isMortarCoupling(transportEqIdx1))
                couplingResC1.accumulate(lfsv_c.child(lambdaX), vertexInIntersection, moleFractionN); //x1g = (x2g + pvap/pgFF x2w) (condition for mole fractions of subdomains)
            else
                couplingResC1.accumulate(lfsv_c.child(lambdaX), vertexInIntersection, 0.0); //x1g = (x2g + pvap/pgFF x2w) (condition for mole fractions of subdomains)
            //always specify equation for lambdaVy, if it is used is decided in the porous-medium section
            couplingResC1.accumulate(lfsv_c.child(rhoVdrop), vertexInIntersection, - dropFlux);//evaporative flux from drop = flux into ff (rho * vy)*dropsurface
#if !COUPLINGCONCEPT //with pore velocity
            couplingResC1.accumulate(lfsv_c.child(pDrop), vertexInIntersection, dropWaterPressure);//internal drop pressure: pw = pg + 2gamma/r
#endif
        }
    }

    /*!
     * \brief Do the coupling from 2nd problem to mortar elements.
     *        The unknowns are transferred from dune-multidomain.
     *        Based on them, a coupling residual is calculated and added at the
     *        respective positions in the matrix.
     *        To couple Stokes and Darcy, Darcy needs conditions for
     *        p (vertical Stokes velocity as Neumann coupling condition)
     *        and x (coupled like before via lagrange multiplier and x1=x2).
     */
    template<typename IntersectionGeom, typename LFSU2, typename X, typename LFSV2,
    typename LFSU_C, typename LFSV_C,typename RES>
    void alpha_enriched_coupling_second (const IntersectionGeom& intersectionGeometry,
            const LFSU2& lfsu_n, const X& unknowns2, const LFSV2& lfsv_n,
            const LFSU_C& lfsu_c, const X& lambda, const LFSV_C& lfsv_c,
            RES& couplingRes2C, RES& couplingResC2) const
    {
        //type definition for pressure space
        typedef typename LFSU2::template Child<pressureIdx2>::Type LFSU2_Pressure;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU2_Pressure::Traits::FiniteElementType> LFSU2_FESwitch_Pressure;
        typedef Dune::BasisInterfaceSwitch<typename LFSU2_FESwitch_Pressure::Basis> LFSU2_BasisSwitch_Pressure;
        //type definition for mass fraction space
        typedef typename LFSU2::template Child<switchIdx2>::Type LFSU2_MassFraction;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU2_MassFraction::Traits::FiniteElementType> LFSU2_FESwitch_MassFraction;

        //type definitions for mortar y-velocity space
        typedef typename LFSU_C::template Child<lambdaVy>::Type LFSU_C_veloxityY;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_veloxityY::Traits::FiniteElementType> LFSU_C_FESwitch_velocityY;
        //type definitions for mortar component flux space
        typedef typename LFSU_C::template Child<lambdaX>::Type LFSU_C_MassFraction;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_MassFraction::Traits::FiniteElementType> LFSU_C_FESwitch_MassFraction;
#if !COUPLINGCONCEPT //with pore velocity
        //type definitions for drop pressure space
        typedef typename LFSU_C::template Child<pDrop>::Type LFSU_C_DropPressure;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_DropPressure::Traits::FiniteElementType> LFSU_C_FESwitch_DropPressure;
#endif

        //common type definitions
        typedef typename LFSU2_BasisSwitch_Pressure::DomainField DomainFieldType;
        typedef typename LFSU2_BasisSwitch_Pressure::DomainField RangeFieldType;
        typedef typename LFSU2_Pressure::Traits::FiniteElementType::
                Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename LFSU2_BasisSwitch_Pressure::Range RangeType;
        typedef typename LFSU2::Traits::SizeType SizeType;

        for(int i=0; i<unknowns2.size(); i++) Valgrind::CheckDefined(unknowns2(lfsu_n,i));
        for(int i=0; i<lambda.size(); i++) Valgrind::CheckDefined(lambda(lfsu_c,i));

        /*********************************                   prepare coupling                   ******************************************************************/
        const MultiDomainGridView& mdGridView = globalProblem_.mdGridView();
        //neighboring element of intersection in subdomain2
        const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
        const MultiDomainElement& mdElement2 = *mdElementPointer2;
        const SubDomainElementPointer sdElementPointer2 = (globalProblem_.sdElementPointer2(mdElement2));
        const SubDomainElement1& sdElement2 = *sdElementPointer2;
        int faceIdx2 = intersectionGeometry.indexInOutside();
        BoundaryTypes2 boundaryTypes2;

        // update fvElementGeometry and the element volume variables
        FVElementGeometry2 fvElemGeom2;
        ElementVolumeVariables2 elemVolVarsCur2, elemVolVarsPrev2;
        updateElemVolVarsSecond(lfsu_n,
                unknowns2,
                mdElement2,
                sdElement2,
                fvElemGeom2,
                elemVolVarsCur2,
                elemVolVarsPrev2);

        //type definitions to store integration point geometry information determined in mortarGeometry
        int vertexInElement, vertexInFace; //vertex at which coupling information is written in matrix
        Scalar weight; //fraction of the interface area corresponding to the integration point
        DimVector integrationPointLocalInElement; //evaluate shape functions in element
        Dune::FieldVector<DomainFieldType, dim-1> integrationPointLocalInFace; //evaluate shape function intersection

        //get global index of current intersection
        IndexType globalIntersectionIndex;
        if((*intersectionGeometry.inside()).level() == (*intersectionGeometry.outside()).level()
                || (*intersectionGeometry.inside()).level() > (*intersectionGeometry.outside()).level())
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.inside(), intersectionGeometry.indexInInside(), 1);
        else
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.outside(), intersectionGeometry.indexInOutside(), 1);

        //loop over vertices of face
        for (int vertexInIntersection = 0; vertexInIntersection < lfsu_c.child(massBalanceIdx2).size(); ++vertexInIntersection)
        {
            //get integration point geometry from map using the global intersection index
            const IntegrationPointGeometryMap& integrationPointGeometryMapSecond = globalProblem_.integrationPointGeometryMapSecond();
            const IntegrationPointGeometryMapIterator mapIt =  integrationPointGeometryMapSecond.find(globalIntersectionIndex);
            if (mapIt == globalProblem_.integrationPointGeometryMapSecond().end()) {
                std::cerr << "Information of IPs for domain one not found in coupling operator\n";
            }
            vertexInElement = mapIt->second[vertexInIntersection].vertexInElement;
            vertexInFace = mapIt->second[vertexInIntersection].vertexInFace;
            weight = mapIt->second[vertexInIntersection].weight;
            integrationPointLocalInElement = mapIt->second[vertexInIntersection].integrationPointLocalInElement;
            integrationPointLocalInFace = mapIt->second[vertexInIntersection].integrationPointLocalInFace;

            // obtain the boundary types
            const VertexPointer2 vPtr2 = sdElement2.template subEntity<dim>(vertexInElement);
            globalProblem_.sdProblem2().boundaryTypes(boundaryTypes2, *vPtr2);

            //get drop information for intersection from dropmap (filled in the coupled problem)
            IndexType globalVertexIndex = mdGridView.indexSet().subIndex(mdElement2, vertexInElement, dim);
            const DropMap& dropMap = globalProblem_.dropMap();
            const DropMapIterator dropMapIt =  dropMap.find(globalVertexIndex);
            if (dropMapIt == globalProblem_.dropMap().end()) {
                std::cerr << "drop information of current vertex"<< globalVertexIndex<<"  not found in coupling operator enrichedCouplingSecond\n";
            }
            const DropInformation& dropInfo = dropMapIt->second;

            /*********************************              do coupling: mortar to Darcy (2C)         ******************************************************************/
            //Integration using midpoint rule -> evaluation of Ansatzfunction at boundaryIP = integrationPointLocalInFace
            std::vector<RangeType> phiVy(lfsu_c.child(lambdaVy).size());
            std::vector<RangeType> phiX(lfsu_c.child(lambdaX).size());
#if !COUPLINGCONCEPT //with pore velocity
            std::vector<RangeType> phiPDrop(lfsu_c.child(pDrop).size());
#endif

            LFSU_C_FESwitch_velocityY::basis(lfsu_c.child(lambdaVy).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiVy);
            LFSU_C_FESwitch_MassFraction::basis(lfsu_c.child(lambdaX).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiX);
#if !COUPLINGCONCEPT //with pore velocity
            LFSU_C_FESwitch_DropPressure::basis(lfsu_c.child(pDrop).finiteElement()).evaluateFunction(integrationPointLocalInFace,phiPDrop);
#endif

            //Lagrange multiplier lambda * mortar Basis functions phi, weighting function = 1 due to box method
            RangeFieldType lambdaVyPhi(0.), lambdaXPhi(0.), lambdaPDropPhi(0.);

            for(SizeType i=0; i<lfsu_c.child(lambdaVy).size(); i++)
                lambdaVyPhi += lambda(lfsu_c.child(lambdaVy), i)*phiVy[i];

            for(SizeType i=0; i<lfsu_c.child(lambdaX).size(); i++)
                lambdaXPhi += lambda(lfsu_c.child(lambdaX), i)*phiX[i];

#if !COUPLINGCONCEPT //with pore velocity
            for(SizeType i=0; i<lfsu_c.child(pDrop).size(); i++)
                lambdaPDropPhi += lambda(lfsu_c.child(pDrop), i)*phiPDrop[i]; //LP for the internal drop pressure, used for drop flux calculation
#endif

            //multipliy lambdaPhi with area of scvf / fraction of intersectionArea for midpoint rule
            lambdaVyPhi *= weight;
            lambdaXPhi *= weight;

            //assemble drop storage (LP rhoVdrop is already the product of volume, density and massFraction of water in water)
            //evaluate storage at node (intersection-local coordinate = vertexInIntersection)
            Scalar dropStorageTransport = lambda(lfsu_c.child(rhoVdrop), vertexInIntersection);//current drop volume * rhow *Xww
            Scalar dropStorage =  dropStorageTransport / elemVolVarsCur2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2);

            Scalar prevDropStorageTransport = dropInfo.prevRhoVDrop;//filled with mortar dof from last time step in coupled problem
            Scalar prevDropStorage =  prevDropStorageTransport / elemVolVarsPrev2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2);

            dropStorage -= prevDropStorage;
            dropStorage *= 0.5;//multiply with 0.5 so that storage term is not assembled twice
            dropStorage /= globalProblem_.timeManager().timeStepSize();
            dropStorageTransport -= prevDropStorageTransport;
            dropStorageTransport *= 0.5;//multiply with 0.5 so that storage term is not assembled twice
            dropStorageTransport /= globalProblem_.timeManager().timeStepSize();
            //            std::cout<<"storage in drop: "<<dropStorage<< " component storage in drop: "<<dropStorageTransport<<std::endl;
            //DROP
            //            dropStorage = 0.0;
            //            dropStorageTransport = 0.0;

            //interface storage of water
            Scalar waterStorage = 0.0;
            Scalar waterStorageTransport = 0.0;
#if COUPLINGCONCEPT
            //assemble storage term of water in interface cells and add to drop balance if drop has formed
//            if(!dropInfo.detached)// && !qDropFormation)
//            {
//                waterStorageTransport = elemVolVarsCur2[vertexInElement].density(wPhaseIdx2) * elemVolVarsCur2[vertexInElement].saturation(wPhaseIdx2)
//                                                    * elemVolVarsCur2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2);
//                waterStorage = elemVolVarsCur2[vertexInElement].density(wPhaseIdx2) * elemVolVarsCur2[vertexInElement].saturation(wPhaseIdx2);
//                Scalar prevWaterStorageTransport = elemVolVarsPrev2[vertexInElement].density(wPhaseIdx2) * elemVolVarsPrev2[vertexInElement].saturation(wPhaseIdx2)
//                                                            * elemVolVarsPrev2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2);
//                Scalar prevWaterStorage = elemVolVarsPrev2[vertexInElement].density(wPhaseIdx2) * elemVolVarsPrev2[vertexInElement].saturation(wPhaseIdx2);
//
//                waterStorageTransport -= prevWaterStorageTransport;
//                waterStorage -= prevWaterStorage;
////                waterStorage *= 0.5; //multiply with 0.5 so that storage term is not assembled twice
////                std::cout << "water storage before: "<<waterStorage<< std::endl;
////                Scalar waterStorage2 = waterStorage * fvElemGeom2.subContVol[vertexInElement].volume;
//                waterStorageTransport *= fvElemGeom2.subContVol[vertexInElement].volume;// / 3.5e-5;
//                waterStorage *= fvElemGeom2.subContVol[vertexInElement].volume;
////                std::cout << "water storage after: "<<waterStorage<< " water storage * scv :" <<waterStorage2<< std::endl;
////                std::cout << "scv: "<< fvElemGeom2.subContVol[vertexInElement].volume << std::endl;
//                waterStorageTransport *= elemVolVarsCur2[vertexInElement].porosity();
//                waterStorage *= elemVolVarsCur2[vertexInElement].porosity();
//                waterStorageTransport /= globalProblem_.timeManager().timeStepSize();
//                waterStorage /= globalProblem_.timeManager().timeStepSize();
////                std::cout<<"storage term LOP: "<<waterStorageTransport<<" "<<waterStorage<<std::endl;
//            }
#endif
            //fill coupling residual in matrix: 2C (middle right) = Darcy-Residual
            if(boundaryTypes2.isMortarCoupling(massBalanceIdx2))
                couplingRes2C.accumulate(lfsv_n.child(massBalanceIdx2), vertexInElement,  - lambdaVyPhi - waterStorage + dropStorage);//Lagrange multiplier (rho*vyff) as flux condition for pm and storage in the drop
            if(boundaryTypes2.isMortarCoupling(transportEqIdx))
                couplingRes2C.accumulate(lfsv_n.child(transportEqIdx), vertexInElement, - lambdaXPhi - waterStorageTransport + dropStorageTransport); //LP for coupling of component fluxes = minus sign since opposite normal direction

            /*********************************              do coupling: Darcy to mortar (C2)         ******************************************************************/
            //to be consistent with the rest, apply BoxMethod also on mortar elements. Hence phi=1 and no quadrature rule is needed.
            //ansatzfunctions are evaluated at BIP = integrationPointLocalInElement
            std::vector<RangeType> Np(lfsu_n.child(pressureIdx2).size());
            std::vector<RangeType> Nx(lfsu_n.child(switchIdx2).size());
            LFSU2_FESwitch_Pressure::basis(lfsu_n.child(pressureIdx2).finiteElement()).evaluateFunction(integrationPointLocalInElement,Np);
            LFSU2_FESwitch_MassFraction::basis(lfsu_n.child(switchIdx2).finiteElement()).evaluateFunction(integrationPointLocalInElement,Nx);

//            // evaluate gradient of basis functions on reference element - pressure
//            std::vector<JacobianType> jp(lfsu_n.child(pressureIdx2).size());
//            LFSU2_FESwitch_Pressure::basis(lfsu_n.child(pressureIdx2).finiteElement()).evaluateJacobian(integrationPointLocalInElement,jp);
//            // extract gradient from jacobian and transform gradients from reference element to real element
//            const Dune::FieldMatrix<DomainFieldType,dim,dim> jac = sdElement2.geometry().jacobianInverseTransposed(integrationPointLocalInElement);
//            std::vector<Dune::FieldVector<RangeFieldType,dim> > gradNp(lfsu_n.child(pressureIdx2).size());
//            for (SizeType i=0; i<lfsu_n.child(pressureIdx2).size(); i++)
//                jac.mv(jp[i][0],gradNp[i]);

            //Darcy primary variables times Ansatzfunctions, weighting function = 1 due to box method
            RangeFieldType gasPressureN(0.), waterPressureN(0.), totalPressure(0.), moleFracxww(0.), moleFractionN(0.), dropFlux(0.);
//            Dune::FieldVector<RangeFieldType,dim> gradPw(0);
            /***************************              pressure               ********************************************************************/
            for(SizeType idx=0; idx<lfsu_n.child(pressureIdx2).size(); idx++)
            {
                //interaction ff - pm without drop
                gasPressureN += unknowns2(lfsu_n.child(pressureIdx2),idx)*Np[idx];

                //interaction ff - pm with drop
                const MaterialLawParams &materialParams = globalProblem_.sdProblem2().spatialParams().materialLawParams(sdElement2, fvElemGeom2, idx);
                Scalar pC = MaterialLaw::pc(materialParams, elemVolVarsCur2[idx].saturation(wPhaseIdx2));
                waterPressureN += (unknowns2(lfsu_n.child(pressureIdx2),idx) - pC)*Np[idx]; //pnSw formulation, water pressure on drop needed: add pc to primary variable pn=pgas: pwater = pn - (-pc)
            }
            //capillary pressure due to curvature (only if drop is present and not detached)
            Scalar surfaceTension = FluidSystem::surfaceTension(elemVolVarsCur2[vertexInElement].fluidState());
            if(dropInfo.detached)
                waterPressureN = 0.0;
            else
                waterPressureN -= 2*surfaceTension / dropInfo.dropRadius * dropInfo.dropSurface/ dropInfo.areaDrop;; //sign is negative because internal drop pressure should be bigger than outside pressure (22.2.13)

            //weighted sum of both pressure contributions DROP
#if COUPLINGCONCEPT //with storage term
            totalPressure =  gasPressureN * (weight * dropInfo.areaGas) + waterPressureN * (weight * dropInfo.areaDrop);
#else // with pore velocity
            totalPressure = gasPressureN * (weight);//only the gas pressure couples to the Stokes equation, the internal drop pressure is used to estimate the pore-scale water flux
#endif
            /***************************              mass fraction               ********************************************************************/
            //interaction ff - pm without drop. use mole fractions, since Raoult is used on the drop.
            for(SizeType idx=0; idx<lfsu_n.child(switchIdx2).size(); idx++)
                moleFractionN += elemVolVarsCur2[idx].fluidState().moleFraction(nPhaseIdx2, wCompIdx2)*Nx[idx];

            for(SizeType idx=0; idx<lfsu_n.child(switchIdx2).size(); idx++)
                moleFracxww += elemVolVarsCur2[idx].fluidState().moleFraction(wPhaseIdx2, wCompIdx2)*Nx[idx];

            Scalar kelvinVaporPressure = FluidSystem::kelvinVaporPressure(elemVolVarsCur2[vertexInElement].fluidState(),
                                wPhaseIdx2, wCompIdx2,
                                dropInfo.dropRadius);

            //on the drop, Raoult is assumed for chemical equilibrium: xwgFF * pgFF = psat * xwwPM. Assemble the right part here.
            if(globalProblem_.sdProblem2().model().phasePresence(globalVertexIndex, /*isOldSol=*/false) != wPhaseOnly)
            {
                moleFractionN *= dropInfo.areaGas;
                moleFractionN += kelvinVaporPressure/refPressure_ * moleFracxww * dropInfo.dropSurface;
            }
            else //if the gas phase dissapears in the porous medium, couple to the water phase by equilibrium
            {
                Scalar temperature = globalProblem_.sdProblem2().temperature();
                Scalar vaporPressure = FluidSystem::H2O::vaporPressure(temperature);
                moleFractionN = (vaporPressure/refPressure_ * dropInfo.areaGas + kelvinVaporPressure/refPressure_ * dropInfo.dropSurface) * moleFracxww;
            }
            moleFractionN *= weight; //weak formulation: integration over area

            /***************************              drop volume               ********************************************************************/
//            //calculate flux of the wetting phase from pm into the drop
//            DimMatrix K;
//            //averaging is only done to get a matrix
//            globalProblem_.sdProblem2().spatialParams().meanK(K,
//                    globalProblem_.sdProblem2().spatialParams().intrinsicPermeability(sdElement2,
//                            fvElemGeom2,
//                            vertexInElement),
//                    globalProblem_.sdProblem2().spatialParams().intrinsicPermeability(sdElement2,
//                            fvElemGeom2,
//                            vertexInElement));
            //unit outer normal of intersection always points from inside-Element (1) to outside-Element (2)
            const DimVector& intersectionUnitNormal = intersectionGeometry.intersection().centerUnitOuterNormal();
            DimVector unitOutsideNormal;
            unitOutsideNormal[0] = -1 * intersectionUnitNormal[0];
            unitOutsideNormal[1] = -1 * intersectionUnitNormal[1];

//            for(SizeType idx=0; idx<lfsu_n.child(pressureIdx2).size(); idx++)
//            {
//                //calculate gradient of water pressure
//                const MaterialLawParams &materialParams = globalProblem_.sdProblem2().spatialParams().materialLawParams(sdElement2, fvElemGeom2, idx);
//                Scalar pC = MaterialLaw::pc(materialParams, elemVolVarsCur2[idx].saturation(wPhaseIdx2));
//                DimVector tmp(gradNp[idx]);
//                tmp *= (unknowns2(lfsu_n.child(pressureIdx2),idx) - pC);//pnSw formulation, water pressure on drop needed: add pc to primary variable pn=pgas: pwater = pn - (-pc)
//                gradPw += tmp;
//            }

            const int boundaryFaceIdx = fvElemGeom2.boundaryFaceIndex(faceIdx2, vertexInFace);
            BoundaryVariables2 boundaryVars2(globalProblem_.sdProblem2(),
                    sdElement2,
                    fvElemGeom2,
                    boundaryFaceIdx,
                    elemVolVarsCur2,
                    true/*onBoundary*/);


            //sink due to drop formation (calculated in global Problem neglecting diffusion)
            Scalar qDropFormation = dropInfo.qDropFormation;
            qDropFormation *= 0.5;//multiply with 0.5 so that sink term is not assembled twice

#if COUPLINGCONCEPT //with storage term
            if(!qDropFormation)
            {

//                            //calculate water velocity
//                            DimVector velocityW;
//                            K.mv(gradPw, velocityW);
//                            velocityW *= - elemVolVarsCur2[vertexInElement].mobility(wPhaseIdx2);
//
//                            //calculate advective and diffusive flux
//                            Scalar advection =  velocityW * unitOutsideNormal;
//                            advection *= elemVolVarsCur2[vertexInElement].molarDensity(wPhaseIdx2) * moleFracxww * FluidSystem::molarMass(wCompIdx2);
//
                //porous medium water flux
                Scalar advection = boundaryVars2.volumeFlux(wPhaseIdx2);
                advection /= boundaryVars2.face().normal.two_norm(); //divide by scvf-area since flux occurs only across drop contact area
                advection *= elemVolVarsCur2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2) * elemVolVarsCur2[vertexInElement].density(wPhaseIdx2);
                //binary diffusion: molar diffusion of water in water = - diffusion of gas in water
                Scalar diffusion = boundaryVars2.moleFractionGrad(wPhaseIdx2) * unitOutsideNormal;
                diffusion *=  - elemVolVarsCur2[vertexInElement].molarDensity(wPhaseIdx2) * boundaryVars2.porousDiffCoeff(wPhaseIdx2) *  FluidSystem::molarMass(wCompIdx2);
                dropFlux = (advection - diffusion);// *  FluidSystem::molarMass(wCompIdx2);
                dropFlux *= weight * dropInfo.areaDrop;//DROP
//                std::cout<<"flow into drop from pm: "<<dropFlux<<std::endl;
            }

#else // with pore velocity
            //get geometry information for flux calculation
            int otherVertex;
            if (vertexInElement == 2)
                otherVertex = 0;
            else if (vertexInElement == 3)
                otherVertex = 1;
            else
                std::cerr<<"not considered orientation of element in alphaCouplingSecond\n";
            //check whether the two vertices are on top of each other
            assert(Dumux::arePointsEqual(sdElement2.geometry().corner(vertexInElement)[0],sdElement2.geometry().corner(otherVertex)[0]));

            //porous-medium water pressure in at the bottom of the porous medium -> use GDL thickness as distance for gradient
            Scalar boundaryGasPressure = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, InitialPressure);
            const MaterialLawParams &materialParams = globalProblem_.sdProblem2().spatialParams().materialLawParams(sdElement2, fvElemGeom2, vertexInElement);
            Scalar boundarypc = MaterialLaw::pc(materialParams, GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.PM, InitialSw));
            Scalar waterPressurePM = boundaryGasPressure - boundarypc;

            //porous-medium water pressure in the first interior node (should be evaluated at IP ???) -> use cell thickness as distance for gradient
            waterPressurePM = elemVolVarsCur2[otherVertex].pressure(wPhaseIdx2);
            //height of interface cell
            Scalar verticalCellHeight = sdElement2.geometry().corner(vertexInElement)[1] - sdElement2.geometry().corner(otherVertex)[1];

            //calculate water flux into drop if drop has already been formed
            if(!qDropFormation && !dropInfo.detached)
            {
                //pore scale water flux dependent on internal drop pressure
                Scalar gradP = (lambdaPDropPhi - waterPressurePM/*waterPressureN*/) / verticalCellHeight;//GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);//verticalCellHeight /*tubeModelThickness_*/; //todo Interface water pressure, interior pressure at node or IP??
                Scalar vPore = - elemVolVarsCur2[vertexInElement].porosity() * std::pow(globalProblem_.meanPoreRadius(),2.0) / (8 * elemVolVarsCur2[vertexInElement].fluidState().viscosity(wPhaseIdx2)) * gradP;
//                vPore /= 1e3;
//                std::cout<< "Porengeschwindigkeit: " << vPore <<std::endl;
                dropFlux = elemVolVarsCur2[vertexInElement].fluidState().massFraction(wPhaseIdx2, wCompIdx2) * elemVolVarsCur2[vertexInElement].density(wPhaseIdx2) * vPore;
                dropFlux *= weight * dropInfo.areaDrop;//DROP
            }
#endif

            //fill coupling residual in matrix: C2 (bottom middle) = mortar-residual
//            if(boundaryTypes2.isMortarCoupling(massBalanceIdx2))
            //always specify equation for lambdaP, if it is used is decided in the free-flow section
            couplingResC2.accumulate(lfsv_c.child(lambdaP), vertexInIntersection, totalPressure); //p2=pD (porous medium pressure = pressure on mortar elements)
            //only couple the mass-fractions if the coupling condition is set
            if(boundaryTypes2.isMortarCoupling(transportEqIdx))
                couplingResC2.accumulate(lfsv_c.child(lambdaX), vertexInIntersection, -moleFractionN); //minus sign since x1 -x2 = 0
            else
                couplingResC2.accumulate(lfsv_c.child(lambdaX), vertexInIntersection, 0.0);
            //always specify equation for rhoVdrop, if it is used is decided in the porous-medium section
            couplingResC2.accumulate(lfsv_c.child(rhoVdrop), vertexInIntersection, dropFlux + qDropFormation + waterStorageTransport);//water flux from pm into drop

        }

    }

    /*!
     * \brief Assembling of the mortar-to-mortar lower diagonal submatrix.
     *  This is needed for the conditions that subdomain dofs are equal to
     *  the mortar dofs (vxff = vxmortar, vyff=vymortar, ppm = pmortar).
     */
    template<typename IntersectionGeom, typename LFSU_C, typename X, typename LFSV_C, typename RES>
    void alpha_enriched_coupling(const IntersectionGeom& intersectionGeometry,
            const LFSU_C& lfsu_c, const X& lambda, const LFSV_C& lfsv_c,
            RES& couplingResCC) const
    {
        //needed type definitions
        //type definitions for mortar x-velocity space
        //        typedef typename LFSU_C::template Child<lambdaBJ>::Type LFSU_C_velocityX;
        //        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_velocityX::Traits::FiniteElementType> LFSU_C_FESwitch_velocityX;
        //type definitions for coupling y-velocit space
        typedef typename LFSU_C::template Child<lambdaVy>::Type LFSU_C_velocityY;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_velocityY::Traits::FiniteElementType> LFSU_C_FESwitch_velocityY;
        //type definitions for mortar pressure space
        typedef typename LFSU_C::template Child<lambdaP>::Type LFSU_C_lambdaP;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_lambdaP::Traits::FiniteElementType> LFSU_C_FESwitch_lambdaP;
        typedef Dune::BasisInterfaceSwitch<typename LFSU_C_FESwitch_lambdaP::Basis> LFSU_C_BasisSwitch_lambdaP;
        //type definitions for mortar drop volume space
        typedef typename LFSU_C::template Child<rhoVdrop>::Type LFSU_C_DropVolume;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_DropVolume::Traits::FiniteElementType> LFSU_C_FESwitch_DropVolume;
        typedef Dune::BasisInterfaceSwitch<typename LFSU_C_FESwitch_DropVolume::Basis> LFSU_C_BasisSwitch_DropVolume;
        //type definitions for LP of component flux space
        typedef typename LFSU_C::template Child<lambdaX>::Type LFSU_C_LambdaX;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_LambdaX::Traits::FiniteElementType> LFSU_C_FESwitch_LambdaX;
        typedef Dune::BasisInterfaceSwitch<typename LFSU_C_FESwitch_LambdaX::Basis> LFSU_C_BasisSwitch_LambdaX;
#if !COUPLINGCONCEPT
        //type definitions for drop pressure space
        typedef typename LFSU_C::template Child<pDrop>::Type LFSU_C_DropPressure;
        typedef Dune::FiniteElementInterfaceSwitch<typename LFSU_C_DropPressure::Traits::FiniteElementType> LFSU_C_FESwitch_DropPressure;
#endif


        //common type definitions
        typedef typename LFSU_C_BasisSwitch_lambdaP::DomainField DomainFieldType;
        typedef typename LFSU_C_BasisSwitch_lambdaP::DomainField RangeFieldType;
        typedef typename LFSU_C_BasisSwitch_lambdaP::Range RangeType;
        typedef typename LFSU_C::Traits::SizeType SizeType;

        for(int i=0; i<lambda.size(); i++) Valgrind::CheckDefined(lambda(lfsu_c,i));

        /************  prepare coupling **********************************************/
        const MultiDomainGridView& mdGridView = globalProblem_.mdGridView();
        const MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
        const MultiDomainElement& mdElement1 = *mdElementPointer1;
        const MultiDomainElement& mdElement2 = *mdElementPointer2;
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
        //reference element of neighboring element of intersection in subdomain1
        const SubDomainElementPointer sdElementPointer1 = (globalProblem_.sdElementPointer1(mdElement1));
        const SubDomainElement1& sdElement1 = *sdElementPointer1;
        //reference element of neighboring element of intersection in subdomain1
        const SubDomainElementPointer sdElementPointer2 = (globalProblem_.sdElementPointer2(mdElement2));
        const SubDomainElement1& sdElement2 = *sdElementPointer2;

        int faceIdx1 = intersectionGeometry.indexInInside();
        int faceIdx2 = intersectionGeometry.indexInOutside();

        BoundaryTypes1 boundaryTypes1;
        BoundaryTypes2 boundaryTypes2;
        //type definitions for integration-point geometry
        Dune::FieldVector<DomainFieldType, dim-1> integrationPointLocalInFace1, integrationPointLocalInFace2;
        Scalar weight1, weight2;

        //get current intersection index
        IndexType globalIntersectionIndex;
        if((*intersectionGeometry.inside()).level() == (*intersectionGeometry.outside()).level()
                || (*intersectionGeometry.inside()).level() > (*intersectionGeometry.outside()).level())
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.inside(), intersectionGeometry.indexInInside(), 1);
        else
            globalIntersectionIndex = mdGridView.indexSet().subIndex(*intersectionGeometry.outside(), intersectionGeometry.indexInOutside(), 1);

        //loop over vertices of face
        for (int vertexInIntersection = 0; vertexInIntersection < lfsu_c.child(lambdaP).size(); ++vertexInIntersection)
        {
            //get geometry of IPs from map for current intersection index
            const IntegrationPointGeometryMapCoupling& integrationPointGeometryMapCoupling = globalProblem_.integrationPointGeometryMapCoupling();
            const IntegrationPointGeometryMapCouplingIterator mapIt =  integrationPointGeometryMapCoupling.find(globalIntersectionIndex);
            if (mapIt == globalProblem_.integrationPointGeometryMapCoupling().end()) {
                std::cerr << "Information of IPs for domain one not found in coupling operator\n";
            }
            weight1 = mapIt->second[vertexInIntersection].weight1;
            weight2 = mapIt->second[vertexInIntersection].weight2;
            integrationPointLocalInFace1 = mapIt->second[vertexInIntersection].integrationPointLocalInFace1;
            integrationPointLocalInFace2 = mapIt->second[vertexInIntersection].integrationPointLocalInFace2;

            //determine global vetex Index
            IndexType globalVertexIndex;
            Dune::FieldVector<DomainFieldType, dim-1> intersectionVertexLocalInIntersection;
            if(mdElement1.level() == mdElement2.level() || mdElement1.level() > mdElement2.level())
            {
                //check if orientation of intersection and orientation of face in element match
                //local coordinates of vertex in intersection in Element1 (conform with interface)
                DimVector intersectionVertexLocalInElement1 =
                        intersectionGeometry.geometryInInside().global(vertexInIntersection);
                //local coordinates if 0th vertex in element face
                DimVector faceVertexLocalInElement1 =
                        referenceElement1.position(referenceElement1.subEntity(faceIdx1, 1, vertexInIntersection, dim), dim);

                int vertexInFace1;
                if((intersectionVertexLocalInElement1 - faceVertexLocalInElement1).two_norm() < 1e-5)
                    vertexInFace1 = vertexInIntersection;
                else
                    vertexInFace1 = abs(vertexInIntersection - 1);

                const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, vertexInFace1, dim);
                globalVertexIndex = mdGridView.indexSet().subIndex(mdElement1, vertInElem1, dim);
                intersectionVertexLocalInIntersection = intersectionGeometry.geometryInInside().local(intersectionVertexLocalInElement1);
                // obtain the boundary types
                const VertexPointer1 vPtr1 = sdElement1.template subEntity<dim>(vertInElem1);
                globalProblem_.sdProblem1().boundaryTypes(boundaryTypes1, *vPtr1);

            }
            else
            {
                //check if orientation of intersection and orientation of face in element match
                //local coordinates of vertex in intersection in Element1 (conform with interface)
                DimVector intersectionVertexLocalInElement2 =
                        intersectionGeometry.geometryInOutside().global(vertexInIntersection);
                //local coordinates if 0th vertex in element face
                DimVector faceVertexLocalInElement2 =
                        referenceElement2.position(referenceElement2.subEntity(faceIdx2, 1, vertexInIntersection, dim), dim);

                int vertexInFace2;
                if((intersectionVertexLocalInElement2 - faceVertexLocalInElement2).two_norm() < 1e-5)
                    vertexInFace2 = vertexInIntersection;
                else
                    vertexInFace2 = abs(vertexInIntersection - 1);

                const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, vertexInFace2, dim);
                globalVertexIndex = mdGridView.indexSet().subIndex(mdElement2, vertInElem2, dim);
                intersectionVertexLocalInIntersection = intersectionGeometry.geometryInOutside().local(intersectionVertexLocalInElement2);
                // obtain the boundary types
                const VertexPointer2 vPtr2 = sdElement2.template subEntity<dim>(vertInElem2);
                globalProblem_.sdProblem2().boundaryTypes(boundaryTypes2, *vPtr2);
            }
            //get drop information for globalVertexIndex from dropMap
            const DropMap& dropMap = globalProblem_.dropMap();
            const DropMapIterator dropMapIt =  dropMap.find(globalVertexIndex);
            if (dropMapIt == globalProblem_.dropMap().end()) {
                std::cerr << "Drop information of current intersection not found in coupling operator enriched coupling\n";
            }
            const DropInformation& dropInfo = dropMapIt->second;

            //Lagrange multiplier lambda * mortar Basis functions phi at respective boundaryIP = integrationPointLocalInFace1/2
            //evaluate lambda phi at boundaryIP
            //            std::vector<RangeType> phiVx(lfsu_c.child(lambdaBJ).size());
            std::vector<RangeType> phiVy(lfsu_c.child(lambdaVy).size());
            std::vector<RangeType> phiP(lfsu_c.child(lambdaP).size());
            std::vector<RangeType> phiVdrop(lfsu_c.child(rhoVdrop).size());
            std::vector<RangeType> phiLambdaX(lfsu_c.child(lambdaX).size());
#if !COUPLINGCONCEPT
            std::vector<RangeType> phiPDrop(lfsu_c.child(pDrop).size());
#endif

            //            LFSU_C_FESwitch_velocityX::basis(lfsu_c.child(lambdaBJ).finiteElement()).evaluateFunction(integrationPointLocalInFace1,phiVx);
            LFSU_C_FESwitch_velocityY::basis(lfsu_c.child(lambdaVy).finiteElement()).evaluateFunction(integrationPointLocalInFace1,phiVy);
            LFSU_C_FESwitch_lambdaP::basis(lfsu_c.child(lambdaP).finiteElement()).evaluateFunction(integrationPointLocalInFace2,phiP);
            //evaluate drop storage at node
            LFSU_C_FESwitch_DropVolume::basis(lfsu_c.child(rhoVdrop).finiteElement()).evaluateFunction(intersectionVertexLocalInIntersection,phiVdrop);
            LFSU_C_FESwitch_LambdaX::basis(lfsu_c.child(lambdaX).finiteElement()).evaluateFunction(intersectionVertexLocalInIntersection,phiLambdaX);
#if !COUPLINGCONCEPT
            LFSU_C_FESwitch_DropPressure::basis(lfsu_c.child(pDrop).finiteElement()).evaluateFunction(intersectionVertexLocalInIntersection,phiPDrop);
#endif

            //vertex in intersection and intersection-local coordinate of this vertex is always the same
            if(intersectionVertexLocalInIntersection != vertexInIntersection )
                std::cerr<<"KNOTEN NICHT GLEICH IN ALPHA COUPLING!\n";

            RangeFieldType  velocityXPhi(0.), lambdaVyPhi(0.), lambdaPPhi(0.), rhoDropVolume(0.), lambdaXPhi(0.), lambdaPDropPhi(0.);

            //            for(SizeType i=0; i<lfsu_c.child(lambdaBJ).size(); i++)
            //                velocityXPhi += lambda(lfsu_c.child(lambdaBJ), i)*phiVx[i];

            for(SizeType i=0; i<lfsu_c.child(lambdaVy).size(); i++)
                lambdaVyPhi += lambda(lfsu_c.child(lambdaVy), i)*phiVy[i];

            for(SizeType i=0; i<lfsu_c.child(lambdaP).size(); i++)
                lambdaPPhi += lambda(lfsu_c.child(lambdaP), i)*phiP[i];

            for(SizeType i=0; i<lfsu_c.child(rhoVdrop).size(); i++)
                rhoDropVolume += lambda(lfsu_c.child(rhoVdrop), i)*phiVdrop[i];

            for(SizeType i=0; i<lfsu_c.child(lambdaX).size(); i++)
                lambdaXPhi += lambda(lfsu_c.child(lambdaX), i)*phiLambdaX[i];
#if !COUPLINGCONCEPT
            for(SizeType i=0; i<lfsu_c.child(pDrop).size(); i++)
                lambdaPDropPhi += lambda(lfsu_c.child(pDrop), i)*phiPDrop[i];
#endif

            //multiply with 0.5 so that storage term is not assembled twice
            Scalar dropStorage = 0.5*(rhoDropVolume - dropInfo.prevRhoVDrop) / globalProblem_.timeManager().timeStepSize();
            //            std::cout<<"storage in drop (condition on mortar elements): "<<dropSt orage<<std::endl;

            //apply midpoint rule for integration and multiply with bf-area
            velocityXPhi *= weight1;
            lambdaVyPhi *= weight1;
            lambdaPPhi *= weight2;

            //fill coupling residual : CC (bottom right) = mortar residual
            //weighting function = 1 due to box method
            //            couplingResCC.accumulate(lfsv_c.child(lambdaBJ), vertexInIntersection, -velocityXPhi); //set mortar x-velocity equal to BJ-velocity (set in couplingFirst)
            couplingResCC.accumulate(lfsv_c.child(lambdaVy), vertexInIntersection, -lambdaVyPhi); //minus sign since vy1*rho -vymortar*rho = 0
            couplingResCC.accumulate(lfsv_c.child(lambdaP), vertexInIntersection, -lambdaPPhi); //minus sign since p2 -pmortar = 0
            //if transport equations are not coupled, include the Lagrange multiplicator in the matrix
            if(!boundaryTypes1.isMortarCoupling(transportEqIdx1) && !boundaryTypes2.isMortarCoupling(transportEqIdx))
                couplingResCC.accumulate(lfsv_c.child(lambdaX), vertexInIntersection, -lambdaXPhi); //TESTING
            if(dropInfo.detached)//if there is no drop, the lagrange multiplier for the drop volume has to be set to zero including it into the matrix
                couplingResCC.accumulate(lfsv_c.child(rhoVdrop), vertexInIntersection, - rhoDropVolume); //Lagrange multiplier for mass in drop volume: rho*Vdrop*xww
            else //if there is a drop, the time derivative of the lagrange multiplier for the drop volume is part of the coupling condition
                couplingResCC.accumulate(lfsv_c.child(rhoVdrop), vertexInIntersection, - dropStorage); //storage term of drop
#if !COUPLINGCONCEPT
            couplingResCC.accumulate(lfsv_c.child(pDrop), vertexInIntersection, - lambdaPDropPhi);
#endif
        }
    }

    /*!
     * \brief Update the volume variables of the element and extract the unknowns from dune-pdelab vectors
     *        and bring them into a form which fits to dumux. For the 2nd problem.
     */
    template<typename LFSU1, typename X>
    void updateElemVolVarsFirst (const LFSU1& lfsu_s,
            const X& unknowns1,
            const MultiDomainElement& mdElement1,
            const SubDomainElement1& sdElement1,
            FVElementGeometry1& fvElemGeom1,
            ElementVolumeVariables1& elemVolVarsCur1) const
    {
        fvElemGeom1.update(globalProblem_.sdGridView1(), sdElement1);

        //bring the local unknowns x_s into a form that can be passed to elemVolVarsCur.update()
        const int numVertsOfElem1 = mdElement1.template count<dim>();
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol1(0);
        elementSol1.resize(unknowns1.size());

        for (int idx=0; idx<numVertsOfElem1; ++idx)
        {
            for(int eqIdx1=0; eqIdx1<numEq1; ++eqIdx1)
                elementSol1[eqIdx1*numVertsOfElem1+idx] = unknowns1(lfsu_s.child(eqIdx1),idx);
        }
        for(int i=0; i<elementSol1.size(); i++)
            Valgrind::CheckDefined(elementSol1[i]);
        // update the elemVolVars vectors using the PDELab solution
        elemVolVarsCur1.updatePDELab(globalProblem_.sdProblem1(), sdElement1,
                fvElemGeom1, elementSol1);

        return;
    }

    template<typename LFSU2, typename X>
    void updateElemVolVarsSecond (const LFSU2& lfsu_n,
            const X& unknowns2,
            const MultiDomainElement& mdElement2,
            const SubDomainElement1& sdElement2,
            FVElementGeometry2& fvElemGeom2,
            ElementVolumeVariables2& elemVolVarsCur2,
            ElementVolumeVariables2& elemVolVarsPrev2) const
    {
        fvElemGeom2.update(globalProblem_.sdGridView2(), sdElement2);

        //bring the local unknowns x_s into a form that can be passed to elemVolVarsCur.update()
        const int numVertsOfElem2 = mdElement2.template count<dim>();
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol2(0);
        elementSol2.resize(unknowns2.size());

        for (int idx=0; idx<numVertsOfElem2; ++idx)
        {
            for(int eqIdx2=0; eqIdx2<numEq2; ++eqIdx2)
                elementSol2[eqIdx2*numVertsOfElem2+idx] = unknowns2(lfsu_n.child(eqIdx2),idx);
        }
        for(int i=0; i<elementSol2.size(); i++)
            Valgrind::CheckDefined(elementSol2[i]);
        // update the elemVolVars vectors using the PDELab solution
        elemVolVarsCur2.updatePDELab(globalProblem_.sdProblem2(), sdElement2,
                fvElemGeom2, elementSol2);
        elemVolVarsPrev2.update(globalProblem_.sdProblem2(),
                sdElement2,
                fvElemGeom2,
                true /* oldSol? */);

        return;
    }

private:
    GlobalProblem& globalProblem_;
    Scalar density_;
    Scalar refPressure_;
    Scalar tubeModelThickness_;
};

} // end namespace Dumux

#endif // DUMUX_MORTAR1P2C1P2CLOCALOPERATOR_HH
