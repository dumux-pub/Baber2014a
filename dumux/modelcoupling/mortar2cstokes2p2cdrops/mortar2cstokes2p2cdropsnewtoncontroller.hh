/****************************************************************************
 *   Copyright (C) 2013 by Katherina Baber                                     *
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2010 by Bernd Flemisch                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Reference implementation of a newton controller for coupled 2cStokes - 2p2c problems.
 */
#ifndef DUMUX_MORTARTWOCSTOKES2P2CDROPS_NEWTON_CONTROLLER_HH
#define DUMUX_MORTARTWOCSTOKES2P2CDROPS_NEWTON_CONTROLLER_HH

#include <dumux/multidomain/common/multidomainnewtoncontroller.hh>

/*!
 * \file
 */
namespace Dumux
{

/*!
 * \brief Implementation of a newton controller for 2cstokes2p2c problems with drops.
 */
template <class TypeTag>
class MortarTwoCStokesTwoPTwoCDropsNewtonController : public MultiDomainNewtonController<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;

    typedef MultiDomainNewtonController<TypeTag> ParentType;

    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;

public:
    MortarTwoCStokesTwoPTwoCDropsNewtonController(const Problem &problem)
        : ParentType(problem)
    {  }

    /*!
    * \brief Indidicates the beginning of a newton iteration and calls the condition for the drop formation.
    */
    void newtonBeginStep()
    {
        //check whether drops form depending on pressure and pore distribution
        ParentType::problem_().dropFormation();

        ParentType::newtonBeginStep();
    }

    /*!
    * \brief Indicates that one Newton iteration was finished.
    */
   void newtonEndStep(SolutionVector &uCurrentIter, SolutionVector &uLastIter)
   {
       //update drop map and evaluate detachment
       ParentType::problem_().updateDropMap(false);

       ParentType::newtonEndStep(uCurrentIter, uLastIter);

//       this->model_().sdModel2().checkPlausibility();

       //call update static data, otherwise variable switch is not called
       this->model_().sdModel2().updateStaticData(this->model_().sdModel2().curSol(),
                                                  this->model_().sdModel2().prevSol());
       //update drop map and evaluate detachment
//       ParentType::problem_().updateDropMap();
   }
};

} // namespace Dumux


#endif
