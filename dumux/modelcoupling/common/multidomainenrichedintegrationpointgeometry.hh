/*!
* \file
* \brief Identifies integration points and geoemetry information for the mortar method in case of non-conforming grids.
*/
#ifndef DUMUX_MORTARGEOMETRYY_HH
#define DUMUX_MORTARGEOMETRYY_HH

#include <iostream>

namespace Dumux {
//geometric information about IntegrationPoints on the interface
struct IntegrationPointGeometry
{
    int vertexInElement;
    int vertexInFace;
    double weight;
    Dune::FieldVector<double, 2> integrationPointLocalInElement;
    Dune::FieldVector<double/*DomainFieldType???*/, 2-1> integrationPointLocalInFace;
};

struct IntegrationPointGeometryCoupling
{
    double weight1;
    double weight2;
    Dune::FieldVector<double/*DomainFieldType???*/, 2-1> integrationPointLocalInFace1;
    Dune::FieldVector<double/*DomainFieldType???*/, 2-1> integrationPointLocalInFace2;
};
/*!
 * \brief Identifies integration points and geometry information for the mortar method in case of non-conforming grids.
 */
template<class TypeTag>
class MortarGeometry
{
public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) OneP1TypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) OneP2TypeTag;

    typedef typename GET_PROP_TYPE(OneP1TypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(OneP2TypeTag, FVElementGeometry) FVElementGeometry2;

    // Multidomain Grid and Subgrid types
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MultiDomainGridTraits;
    typedef Dune::MultiDomainGrid<HostGrid, MultiDomainGridTraits> MultiDomainGrid;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;

    typedef typename GET_PROP_TYPE(OneP1TypeTag, GridView) OneP1GridView;
    typedef typename GET_PROP_TYPE(OneP2TypeTag, GridView) OneP2GridView;

    typedef typename OneP1GridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename OneP2GridView::template Codim<0>::Entity SubDomainElement2;
    typedef typename SubDomainGrid::Traits::template Codim<0>::EntityPointer SubDomainElementPointer;

    typedef typename std::vector<int> TubeIndicesOfIntersection;
    typedef typename MultiDomainGrid::LeafGridView::IndexSet::IndexType IndexType;

    enum
    {
        dim = MultiDomainGrid::dimension,
        dimWorld = MultiDomainGrid::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim> ReferenceElement1;
    typedef typename Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim> ReferenceElement2;

    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator SubDomainInterfaceIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator::Intersection Intersection;//is this the right type?
    typedef typename Dune::PDELab::IntersectionGeometry<Intersection>::IntersectionGeometry IntersectionGeometry;

    typedef std::vector<IntegrationPointGeometry> IntegrationPointGeometryVector;
    typedef std::vector<IntegrationPointGeometryCoupling> IntegrationPointGeometryVectorCoupling;
    typedef typename std::pair<IndexType,IntegrationPointGeometryVector> IntegrationPointGeometryIntersectionPair;
    typedef typename std::pair<IndexType,IntegrationPointGeometryVectorCoupling> IntegrationPointGeometryIntersectionPairCoupling;

    MortarGeometry(GlobalProblem& globalProblem)
    : globalProblem_(globalProblem)
    {}

    void createEnrichedIPGeometryMap()
    {
        //identify and store geometry information of integration points
        const MultiDomainGridView& mdGridView = globalProblem_.mdGrid().leafView();
        const SubDomainInterfaceIterator endIfIt = globalProblem_.mdGrid().leafSubDomainInterfaceEnd(globalProblem_.sdID1(), globalProblem_.sdID2());
        for (SubDomainInterfaceIterator ifIt = globalProblem_.mdGrid().leafSubDomainInterfaceBegin(globalProblem_.sdID1(), globalProblem_.sdID2());
              ifIt != endIfIt; ++ifIt)
        {
            //vectors to store integration Point Geometry per intersection and domain
            IntegrationPointGeometryVector integrationPointGeometryVectorFirst;
            IntegrationPointGeometryVector integrationPointGeometryVectorSecond;
            IntegrationPointGeometryVectorCoupling integrationPointGeometryVectorCoupling;

            //turn intersection into intersectionGeometry (ifIt = pointer, *ifIt = intersection)
            //const Intersection& intersectionGeometry(*ifIt);//ifIt->boundarySegmentIndex()); //index=0???
            IntersectionGeometry intersectionGeometry(*ifIt,0);
            MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
            MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
            const MultiDomainElement& mdElement1 = *(mdElementPointer1);
            const MultiDomainElement& mdElement2 = *(mdElementPointer2);

            //use globalIndex of intersection instead of intersectionGeometry to store IP information
            //(needs less space and is not a temporary information)
            //use face conform with interface
            IndexType globalIntersectionIndex;
            if(mdElement1.level() == mdElement2.level() || mdElement1.level() > mdElement2.level())
                globalIntersectionIndex = mdGridView.indexSet().subIndex(mdElement1, intersectionGeometry.indexInInside(), 1);
            else
                globalIntersectionIndex = mdGridView.indexSet().subIndex(mdElement2, intersectionGeometry.indexInOutside(), 1);

            //ToDo use the right reference element (does not really matter as long as it is only used for numVerticesOfFace
//            if(ifIt->inside().level() == ifIt->outside().level() || ifIt->inside().level() > ifIt->outside().level())
            const ReferenceElement1& refElement = Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
//            else
//                const ReferenceElement& refElement = Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(ifIt->outside().type());

            //fill vectors containing a struct(vertexInElement, weight, IPLocalInFace/Element) for each vertex of the intersection
            const int numVerticesOfFace = refElement.size(intersectionGeometry.indexInInside(), 1, dim);
            for (int vertexInIntersection = 0; vertexInIntersection < numVerticesOfFace; vertexInIntersection++)
            {
                /***************************               integration points in first domain             **************************/
                //this stores a struct with the IP geometry information corresponding to vertexInIntersection in integrationPointGeometryVector
                //since the loop starts at vertexInIntersection = 0, the 0th entry of integrationPointGeometryVector corresponds to vertexInIntersection
                integrationPointGeometryVectorFirst.push_back(integrationPointsEnrichedCouplingFirst(intersectionGeometry/*corresponds to intersectionGeometry given by multidomain/PDELab???*/,
                                                                                                                        vertexInIntersection));

                /***************************               integration points in second domain             **************************/
                integrationPointGeometryVectorSecond.push_back(integrationPointsEnrichedCouplingSecond(intersectionGeometry/*corresponds to intersectionGeometry given by multidomain/PDELab???*/,
                                                                                                                        vertexInIntersection));

                /***************************               integration points in second domain             **************************/
                integrationPointGeometryVectorCoupling.push_back(integrationPointsEnrichedCoupling(intersectionGeometry/*corresponds to given by multidomain/PDELabintersectionGeometry ???*/,
                                                                                                                    vertexInIntersection));
            }
            //map that connects intersection information (globalsIdx) to geometry information of integration points (vector containing two structs)
            globalProblem_.insertInIntegrationPointGeometryMapFirst().insert(IntegrationPointGeometryIntersectionPair(globalIntersectionIndex,integrationPointGeometryVectorFirst));
            //map that connects intersection information (globalsIdx) to geometry information of integration points (vector containing two structs)
            globalProblem_.insertInIntegrationPointGeometryMapSecond().insert(IntegrationPointGeometryIntersectionPair(globalIntersectionIndex,integrationPointGeometryVectorSecond));

            //map that connects intersection information to geometry information of integration points
            globalProblem_.insertInIntegrationPointGeometryMapCoupling().insert(IntegrationPointGeometryIntersectionPairCoupling(globalIntersectionIndex,integrationPointGeometryVectorCoupling));
        }
}

    /*!
     * \brief Identify non-conforming grids and determine geometry information of integration points
     *        in the Element of domain1,
     *        in the (mortar) face,
     *        and the corresponding node in element of domain one.
     */
    template<typename IntersectionGeom>
    IntegrationPointGeometry integrationPointsEnrichedCouplingFirst(const IntersectionGeom& intersectionGeometry,
            /*const MultiDomainElement& mdElement1, const MultiDomainElement& mdElement2,
            const SubDomainElement1& sdElement1,*/ const int& vertexInIntersection
            /*int& vertexInElement, Scalar& weight,
            DimVector& integrationPointLocalInElement,
            IPLocalInFace& integrationPointLocalInFace*/)
    {
        MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
        const MultiDomainElement& mdElement1 = *(mdElementPointer1);
        const MultiDomainElement& mdElement2 = *(mdElementPointer2);
        const SubDomainElementPointer sdElementPointer1 = globalProblem_.sdElementPointer1(mdElement1);
        const SubDomainElement1& sdElement1 = *sdElementPointer1;
        //reference element of neighboring element of intersection in subdomain1
        const ReferenceElement1& refElement1 =
            Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());

        // face index wrt element
        int faceIdx1 = intersectionGeometry.indexInInside();
        int faceIdx2 = intersectionGeometry.indexInOutside();

        /*********************     indentify and deal with nonconforming grids   *******************************************/
        //conforming grids or domain1 is fine and conform with interface -> use BIPs of domain one as integration points
        if(mdElement1.level() == mdElement2.level() || mdElement1.level() > mdElement2.level())
        {
            FVElementGeometry1 fvGeometry1;
            fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);

            //check if orientation of intersection and orientation of face in element match
            //local coordinates of vertex in intersection in Element1 (conform with interface)
            DimVector intersectionVertexLocalInElement =
                    intersectionGeometry.geometryInInside().global(vertexInIntersection);
            //local coordinates of vertexInIntersection vertex in element face
            DimVector faceVertexLocalInElement =
                    refElement1.position(refElement1.subEntity(faceIdx1, 1, vertexInIntersection, dim), dim);

            int vertexInFace1;
            if((intersectionVertexLocalInElement - faceVertexLocalInElement).two_norm() < 1e-5)
                vertexInFace1 = vertexInIntersection; //element face and intersection have the same orientation
            else
                vertexInFace1 = abs(vertexInIntersection - 1);//vertexInIntersection = 0: |0 - 1| = 1; vertexInIntersection = 1: |1 - 1| = 0

            //get the according index of the boundary face
            const int boundaryFaceIdx1 = fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace1);
            //get local coordinates of boundary integration point
            const DimVector& boundaryIPLocal1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].ipLocal;
            //transform 2D coordinates of BIP into 1D coordinates
            integrationPointGeometry_.integrationPointLocalInElement = boundaryIPLocal1;
            integrationPointGeometry_.integrationPointLocalInFace = intersectionGeometry.geometryInInside().local(integrationPointGeometry_.integrationPointLocalInElement);
            //for integration, multiplication with respective area (here scvf-area)
            integrationPointGeometry_.weight = fvGeometry1.boundaryFace[boundaryFaceIdx1].area;
            //vertex wrt to reference element of first element
            integrationPointGeometry_.vertexInElement = refElement1.subEntity(faceIdx1, 1, vertexInFace1, dim);
            //idx of vertex in face
            integrationPointGeometry_.vertexInFace = vertexInFace1;

//            if(GET_PROP_VALUE(TypeTag, WithTubes))
//            {
//                IndexType globalIntersectionIndex = mdGrid.leafView().indexSet().subIndex(mdElement1, 1, faceIdx1);
//                //get tubes on intersection
//                TubeIndicesOfIntersection tubeIndices = globalProblem_.tubesPerIntersectionMap()[globalIntersectionIndex];
//
//                //determine boundaries of integration area
//                Scalar xMin, xMax;
//                const DimVector& vertexInElementGlobal = refElement1.position(refElement1.subEntity(faceIdx1, 1, vertexInElement, dim), dim);
//                if(vertexInElementGlobal[0] < integrationPointLocalInElement[0])
//                {
//                    xMin = vertexInElementGlobal[0];
//                    xMax = xMin + weight;
//                }
//                else
//                {
//                    xMax = vertexInElementGlobal[0];
//                    xMin = xMax - weight;
//                }
//
//                //find out whether tubes lie on current integration area: integrationPointInElement + weight
//
//                for(std::size_t i=0; i < tubeIndices.size(); i++)
//                {
            //ToDo use subProblem2 statt onePTwoC1Problem
//                    //a horizontal interface with constant y-coord is assumed, so only x-coord has to be compared
//                    if(xMin <= globalProblem_.onePTwoC1Problem().spatialParams().tubeGeometryVector()[i].position_
//                            && xMax >= globalProblem_.onePTwoC1Problem().spatialParampatialParameters().tubeGeometryVector()[i].index_);
//                }
//            }
        }
        //domain2 is fine and conform with interface -> use BIPs of domain two as integration points
        if(mdElement1.level() < mdElement2.level())
        {
            const SubDomainElementPointer sdElementPointer2 = globalProblem_.sdElementPointer2(mdElement2);
            const SubDomainElement2& sdElement2 = *sdElementPointer2;

            FVElementGeometry2 fvGeometry2;
            fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);

            //reference element of neighboring element of intersection in subdomain2
            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& refElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());

            //check if orientation of intersection and orientation of face in element match
            //local coordinates of vertex in intersection in Element2 (conform with interface)
            DimVector intersectionVertexLocalInElement =
                    intersectionGeometry.geometryInOutside().global(vertexInIntersection);
            //local coordinates if 0th vertex in element face
            DimVector faceVertexLocalInElement =
                    refElement2.position(refElement2.subEntity(faceIdx2, 1, vertexInIntersection, dim), dim);

            int vertexInFace2;
            if((intersectionVertexLocalInElement - faceVertexLocalInElement).two_norm() < 1e-5)
                vertexInFace2 = vertexInIntersection;//element face and intersection have the same orientation
            else
                vertexInFace2 = abs(vertexInIntersection - 1);//vertexInIntersection = 0: |0 - 1| = 1; vertexInIntersection = 1: |1 - 1| = 0

            //get the according index of the boundary face
            const int boundaryFaceIdx2 = fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace2);
            //get local coordinates of boundary integration point
            const DimVector& boundaryIPLocal2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].ipLocal;
            //transform local element coords in element2 in global coords
            const GlobalPosition& boundaryIPGlobal2 = mdElement2.geometry().global(boundaryIPLocal2);
            //get local coordinates of BIP2 in element1
            integrationPointGeometry_.integrationPointLocalInElement = mdElement1.geometry().local(boundaryIPGlobal2);
            //transform 2D coordinates of BIP into 1D coordinates
            integrationPointGeometry_.integrationPointLocalInFace = intersectionGeometry.geometryInInside().local( integrationPointGeometry_.integrationPointLocalInElement);
            //for integration, multiplication with respective area (here scvf-area)
            integrationPointGeometry_.weight = fvGeometry2.boundaryFace[boundaryFaceIdx2].area;

            //identify vertex wrt to reference element of first element
            //local element coords of 0th and 1st face vertex
            DimVector faceVertex0LocalInElement =
                         refElement1.position(refElement1.subEntity(faceIdx1, 1, 0, dim), dim);
            DimVector faceVertex1LocalInElement =
                                  refElement1.position(refElement1.subEntity(faceIdx1, 1, 1, dim), dim);
            //distance from integration point to 0th and 1st face vertex in element
            Scalar leftDistance = (faceVertex0LocalInElement - integrationPointGeometry_.integrationPointLocalInElement).two_norm();
            Scalar rightDistance = (faceVertex1LocalInElement - integrationPointGeometry_.integrationPointLocalInElement).two_norm();
            //get vertex index in element
            if(leftDistance < rightDistance) //IP is closer to face vertex 0
                integrationPointGeometry_.vertexInElement = refElement1.subEntity(faceIdx1, 1, 0, dim);
            else
                integrationPointGeometry_.vertexInElement = refElement1.subEntity(faceIdx1, 1, 1, dim);

            //idx of vertex in face //todo check if correct
            integrationPointGeometry_.vertexInFace =  refElement1.subEntity(integrationPointGeometry_.vertexInElement, dim, faceIdx1, dim-1);
        }

        return integrationPointGeometry_;
    }

    /*!
     * \brief Identify non-conforming grids and determine geometry information of integration points
     *        in Element of domain1,
     *        in (mortar) face,//            if(GET_PROP_VALUE(TypeTag, WithTubes))
     *
     *        the corresponding node in element of domain one.
     */
    template<typename IntersectionGeom>
    IntegrationPointGeometry integrationPointsEnrichedCouplingSecond(const IntersectionGeom& intersectionGeometry,
            /*const MultiDomainElement& mdElement1, const MultiDomainElement& mdElement2,
            const SubDomainElement2& sdElement2,*/ const int& vertexInIntersection
            /*int& vertexInElement, Scalar& weight,
            DimVector& integrationPointLocalInElement,
            IPLocalInFace& integrationPointLocalInFace*/)
    {
        MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
        const MultiDomainElement& mdElement1 = *(mdElementPointer1);
        const MultiDomainElement& mdElement2 = *(mdElementPointer2);
        const SubDomainElementPointer sdElementPointer2 = globalProblem_.sdElementPointer2(mdElement2);
        const SubDomainElement1& sdElement2 = *sdElementPointer2;
        //reference element of neighboring element of intersection in subdomain1
        const ReferenceElement2& refElement2 =
            Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());

        // face index wrt element
        int faceIdx1 = intersectionGeometry.indexInInside();
        int faceIdx2 = intersectionGeometry.indexInOutside();
        /*********************     indentify and deal with nonconforming grids   *******************************************/
        //conforming grids or domain2 is fine and conform with interface -> use BIPs of domain two as integration points
        if(mdElement1.level() == mdElement2.level() || mdElement2.level() > mdElement1.level())
        {
           FVElementGeometry2 fvGeometry2;
           fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);

           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element2 (conform with interface)
           DimVector intersectionVertexLocalInElement =
               intersectionGeometry.geometryInOutside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement =
               refElement2.position(refElement2.subEntity(faceIdx2, 1, vertexInIntersection, dim), dim);

           int vertexInFace2;
           if((intersectionVertexLocalInElement - faceVertexLocalInElement).two_norm() < 1e-5)
               vertexInFace2 = vertexInIntersection; //element face and intersection have the same orientation
           else
               vertexInFace2 = abs(vertexInIntersection - 1);//vertexInIntersection = 0: |0 - 1| = 1; vertexInIntersection = 1: |1 - 1| = 0

           //get the according index of the boundary face
           const int boundaryFaceIdx2 = fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace2);
           //get local coordinates of boundary integration point
           const DimVector& boundaryIPLocal2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].ipLocal;
           //transform 2D coordinates of BIP into 1D coordinates
           integrationPointGeometry_.integrationPointLocalInElement = boundaryIPLocal2;
           integrationPointGeometry_.integrationPointLocalInFace = intersectionGeometry.geometryInOutside().local( integrationPointGeometry_.integrationPointLocalInElement);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometry_.weight = fvGeometry2.boundaryFace[boundaryFaceIdx2].area;
           //vertex wrt to reference element of first element
           integrationPointGeometry_.vertexInElement = refElement2.subEntity(faceIdx2, 1, vertexInFace2, dim);
           //idx of vertex in face
           integrationPointGeometry_.vertexInFace = vertexInFace2;
        }
        //domain1 is fine and conform with interface -> use BIPs of domain one as integration points
        if(mdElement2.level() < mdElement1.level())
        {
           const SubDomainElementPointer sdElementPointer1 = globalProblem_.sdElementPointer1(mdElement1);
           const SubDomainElement1& sdElement1 = *sdElementPointer1;

           FVElementGeometry1 fvGeometry1;
           fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);

           //reference element of neighboring element of intersection in subdomain1
           const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& refElement1 =
                   Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());

           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element1 (conform with interface)
           DimVector intersectionVertexLocalInElement =
                   intersectionGeometry.geometryInInside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement =
                   refElement1.position(refElement1.subEntity(faceIdx1, 1, vertexInIntersection, dim), dim);

           int vertexInFace1;
           if((intersectionVertexLocalInElement - faceVertexLocalInElement).two_norm() < 1e-5)
               vertexInFace1 = vertexInIntersection;
           else
               vertexInFace1 = abs(vertexInIntersection - 1);//vertexInIntersection = 0: |0 - 1| = 1; vertexInIntersection = 1: |1 - 1| = 0

           //get the according index of the boundary face
           const int boundaryFaceIdx1 = fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace1);
           //get local coordinates of boundary integration point
           const DimVector& boundaryIPLocal1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].ipLocal;
           //transform local element coords in element1 in global coords
           const GlobalPosition& boundaryIPGlobal1 = mdElement1.geometry().global(boundaryIPLocal1);
           //get local coordinates of BIP1 in element2
           integrationPointGeometry_.integrationPointLocalInElement = mdElement2.geometry().local(boundaryIPGlobal1);
           //transform 2D coordinates of BIP into 1D coordinates
           integrationPointGeometry_.integrationPointLocalInFace = intersectionGeometry.geometryInOutside().local( integrationPointGeometry_.integrationPointLocalInElement);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometry_.weight = fvGeometry1.boundaryFace[boundaryFaceIdx1].area;

           //identify vertex wrt to reference element of second element
           //local element coords of 0th and 1st face vertex
           DimVector faceVertex0LocalInElement =
                        refElement2.position(refElement2.subEntity(faceIdx2, 1, 0, dim), dim);
           DimVector faceVertex1LocalInElement =
                                 refElement2.position(refElement2.subEntity(faceIdx2, 1, 1, dim), dim);
           //distance from integration point to 0th and 1st face vertex in element
           Scalar leftDistance = (faceVertex0LocalInElement - integrationPointGeometry_.integrationPointLocalInElement).two_norm();
           Scalar rightDistance = (faceVertex1LocalInElement - integrationPointGeometry_.integrationPointLocalInElement).two_norm();
           //get vertex index in element
           if(leftDistance < rightDistance) //IP is closer to face vertex 0
               integrationPointGeometry_.vertexInElement = refElement2.subEntity(faceIdx2, 1, 0, dim);
           else
               integrationPointGeometry_.vertexInElement = refElement2.subEntity(faceIdx2, 1, 1, dim);

           //idx of vertex in face //todo check if correct
           integrationPointGeometry_.vertexInFace =  refElement2.subEntity(integrationPointGeometry_.vertexInElement, dim, faceIdx2, dim-1);
        }
        return integrationPointGeometry_;
    }

    /*!
     * \brief Identify non-conforming grids and determine geometry information of integration points
     *        in Element of domain1,
     *        in (mortar) face,
     *        the corresponding node in element of domain one.
     */
    template<typename IntersectionGeom>
    IntegrationPointGeometryCoupling integrationPointsEnrichedCoupling(const IntersectionGeom& intersectionGeometry,
            /*const MultiDomainElement& mdElement1, const MultiDomainElement& mdElement2,
            const SubDomainElement1& sdElement1, const SubDomainElement2& sdElement2,*/
            const int& vertexInIntersection
            /*Scalar& weight1, Scalar& weight2,
            IPLocalInFace& integrationPointLocalInFace1, IPLocalInFace& integrationPointLocalInFace2*/)
    {
        MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
        MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
        const MultiDomainElement& mdElement1 = *(mdElementPointer1);
        const MultiDomainElement& mdElement2 = *(mdElementPointer2);
        const SubDomainElementPointer sdElementPointer1 = globalProblem_.sdElementPointer1(mdElement1);
        const SubDomainElementPointer sdElementPointer2 = globalProblem_.sdElementPointer2(mdElement2);
        const SubDomainElement1& sdElement1 = *sdElementPointer1;
        const SubDomainElement2& sdElement2 = *sdElementPointer2;
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& refElement1 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
        const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& refElement2 =
                Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());

        FVElementGeometry1 fvGeometry1;
        FVElementGeometry2 fvGeometry2;
        fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);
        fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);

        // faceIdx wrt elements
        int faceIdx1 = intersectionGeometry.indexInInside();
        int faceIdx2 = intersectionGeometry.indexInOutside();

        //identify integration points
       if(mdElement1.level() == mdElement2.level()) //conforming grids: BIPs = integration points
       {
           /***********************     domain1     **********************************************************/
           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element1 (conform with interface)
           DimVector intersectionVertexLocalInElement1 =
                   intersectionGeometry.geometryInInside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement1 =
                   refElement1.position(refElement1.subEntity(faceIdx1, 1, vertexInIntersection, dim), dim);

           int vertexInFace1;
           if((intersectionVertexLocalInElement1 - faceVertexLocalInElement1).two_norm() < 1e-5)
               vertexInFace1 = vertexInIntersection;
           else
               vertexInFace1 = abs(vertexInIntersection - 1);

           //get local coordinates of integration point
           const int boundaryFaceIdx1 = fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace1);
           const DimVector& boundaryIPLocal1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].ipLocal;
           integrationPointGeometryCoupling_.integrationPointLocalInFace1 = intersectionGeometry.geometryInInside().local(boundaryIPLocal1);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometryCoupling_.weight1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].area;

           /***********************     domain2     **********************************************************/
           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element2 (conform with interface)
           DimVector intersectionVertexLocalInElement2 =
                   intersectionGeometry.geometryInOutside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement2 =
                   refElement2.position(refElement2.subEntity(faceIdx2, 1, vertexInIntersection, dim), dim);

           int vertexInFace2;
           if((intersectionVertexLocalInElement2 - faceVertexLocalInElement2).two_norm() < 1e-5)
               vertexInFace2 = vertexInIntersection;
           else
               vertexInFace2 = abs(vertexInIntersection - 1);

           //get local coordinates of integration point
           const int boundaryFaceIdx2 = fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace2);
           const DimVector& boundaryIPLocal2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].ipLocal;
           integrationPointGeometryCoupling_.integrationPointLocalInFace2 = intersectionGeometry.geometryInOutside().local(boundaryIPLocal2);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometryCoupling_.weight2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].area;
       }
       else if(mdElement1.level() > mdElement2.level()) //domain1 fine, interface is conform with domain1
       {   //use BIP of domain1
           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element1 (conform with interface)
           DimVector intersectionVertexLocalInElement1 =
                   intersectionGeometry.geometryInInside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement1 =
                   refElement1.position(refElement1.subEntity(faceIdx1, 1, vertexInIntersection, dim), dim);

           int vertexInFace1;
           if((intersectionVertexLocalInElement1 - faceVertexLocalInElement1).two_norm() < 1e-5)
               vertexInFace1 = vertexInIntersection;
           else
               vertexInFace1 = abs(vertexInIntersection - 1);

           //get local coordinates of integration point
           const int boundaryFaceIdx1 = fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace1);
           const DimVector& boundaryIPLocal1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].ipLocal;
           integrationPointGeometryCoupling_.integrationPointLocalInFace1 = intersectionGeometry.geometryInInside().local(boundaryIPLocal1);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometryCoupling_.weight1 = fvGeometry1.boundaryFace[boundaryFaceIdx1].area;

           //transform local element coords in element1 in global coords
           const GlobalPosition& boundaryIPGlobal1 = mdElement1.geometry().global(boundaryIPLocal1);
           //get local coordinates of BIP1 in element2
           const DimVector& boundaryIPLocal2 = mdElement2.geometry().local(boundaryIPGlobal1);
           //transform 2D coordinates of BIP into 1D coordinates
           integrationPointGeometryCoupling_.integrationPointLocalInFace2 = intersectionGeometry.geometryInOutside().local(boundaryIPLocal2);
           //for integration, multiplication with scvf area of domain1
           integrationPointGeometryCoupling_.weight2 = fvGeometry1.boundaryFace[boundaryFaceIdx1].area;

       }
       else //mdElement1.level() < mdElement2.level() -> domain2 fine, interface is conform with domain2
       {//use BIP of domain2
           //check if orientation of intersection and orientation of face in element match
           //local coordinates of vertex in intersection in Element2 (conform with interface)
           DimVector intersectionVertexLocalInElement2 =
                   intersectionGeometry.geometryInOutside().global(vertexInIntersection);
           //local coordinates if 0th vertex in element face
           DimVector faceVertexLocalInElement2 =
                   refElement2.position(refElement2.subEntity(faceIdx2, 1, vertexInIntersection, dim), dim);

           int vertexInFace2;
           if((intersectionVertexLocalInElement2 - faceVertexLocalInElement2).two_norm() < 1e-5)
               vertexInFace2 = vertexInIntersection;
           else
               vertexInFace2 = abs(vertexInIntersection - 1);

           //get local coordinates of integration point
           const int boundaryFaceIdx2 = fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace2);
           const DimVector& boundaryIPLocal2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].ipLocal;
           integrationPointGeometryCoupling_.integrationPointLocalInFace2 = intersectionGeometry.geometryInOutside().local(boundaryIPLocal2);
           //for integration, multiplication with respective area (here scvf-area)
           integrationPointGeometryCoupling_.weight2 = fvGeometry2.boundaryFace[boundaryFaceIdx2].area;

           //transform local element coords in element2 in global coords
           const GlobalPosition& boundaryIPGlobal2 = mdElement2.geometry().global(boundaryIPLocal2);
           //get local coordinates of BIP1 in element1
           const DimVector& boundaryIPLocal1 = mdElement1.geometry().local(boundaryIPGlobal2);
           //transform 2D coordinates of BIP into 1D coordinates
           integrationPointGeometryCoupling_.integrationPointLocalInFace1 = intersectionGeometry.geometryInInside().local(boundaryIPLocal1);//geometryInInside
           //for integration, multiplication with scvf area of domain2
           integrationPointGeometryCoupling_.weight1 = fvGeometry2.boundaryFace[boundaryFaceIdx2].area;
       }
       return integrationPointGeometryCoupling_;
    }
private:
    GlobalProblem& globalProblem_;
    IntegrationPointGeometry integrationPointGeometry_;
    IntegrationPointGeometryCoupling integrationPointGeometryCoupling_;
//    TubeIndicesOfIntersection& relevantTubeIndices_;

};

} // end namespace Dumux

#endif // DUMUX_MORTAR1P1PLOCALOPERATOR_HH
