// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
* \file
* \brief Multidomainassembler for coupled problems using the mortar method.
*/
#ifndef DUMUX_MULTIDOMAINENRICHED_ASSEMBLER_HH
#define DUMUX_MULTIDOMAINENRICHED_ASSEMBLER_HH

#include "multidomainenrichedproperties.hh"

namespace Dumux {
/*!
 * \brief Multidomainassembler for coupled problems using the mortar method.
 *
 * This Multidomainassembler is similar to the general on,
 * but includes gridfunctionspaces on the coupling elements
 * and uses the enrichedCoupling-classes for the mortar method.
 */
template<class TypeTag>
class MultiDomainEnrichedAssembler
{
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) SubTypeTag2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, Problem) SubProblem1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Problem) SubProblem2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, LocalFEMSpace) FEM1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, LocalFEMSpace) FEM2;

    typedef typename GET_PROP_TYPE(SubTypeTag1, Constraints) Constraints1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Constraints) Constraints2;

    typedef typename GET_PROP_TYPE(TypeTag, MortarFEMSpace) MortarFEM;
    typedef typename GET_PROP_TYPE(TypeTag, MortarPredicat) MortarPredicat;

    typedef typename GET_PROP_TYPE(SubTypeTag1, ScalarGridFunctionSpace) ScalarGridFunctionSpace1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, ScalarGridFunctionSpace) ScalarGridFunctionSpace2;
    typedef typename GET_PROP_TYPE(TypeTag, MortarScalarGridFunctionSpace) MortarScalarGridFunctionSpace;

    typedef typename GET_PROP_TYPE(SubTypeTag1, GridFunctionSpace) GridFunctionSpace1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridFunctionSpace) GridFunctionSpace2;
    typedef typename GET_PROP_TYPE(TypeTag, MortarGridFunctionSpace) MortarGridFunctionSpace;

    typedef typename GET_PROP_TYPE(SubTypeTag1, LocalOperator) LocalOperator1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, LocalOperator) LocalOperator2;

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) MultiDomainGridFunctionSpace;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCondition) MultiDomainCondition;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem1) MultiDomainSubProblem1;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem2) MultiDomainSubProblem2;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCouplingLocalOperator) MultiDomainCouplingLocalOperator;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCoupling) MultiDomainCoupling;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainConstraintsTrafo) MultiDomainConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator) MultiDomainGridOperator;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;
    typedef typename GET_PROP_TYPE(TypeTag, JacobianMatrix) JacobianMatrix;

    // copying the jacobian assembler is not a good idea
    MultiDomainEnrichedAssembler(const MultiDomainEnrichedAssembler &);

public:
    MultiDomainEnrichedAssembler()
    {
        globalProblem_ = 0;
        problem1_= 0;
        problem2_= 0;
    }

    ~MultiDomainEnrichedAssembler()
    {
    }

    void init(Problem& problem)
    {
        globalProblem_ = &problem;
        problem1_ = &globalProblem_->sdProblem1();
        problem2_ = &globalProblem_->sdProblem2();

        fem1_ = Dune::make_shared<FEM1>();
        fem2_ = Dune::make_shared<FEM2>();
        mortarfem_ = Dune::make_shared<MortarFEM>();


        scalarGridFunctionSpace1_ = Dune::make_shared<ScalarGridFunctionSpace1>(problem1_->gridView(),
        														 *fem1_);
        scalarGridFunctionSpace2_ = Dune::make_shared<ScalarGridFunctionSpace2>(problem2_->gridView(),
        														 *fem2_);
        condition1_ = Dune::make_shared<MultiDomainCondition>(0);
        condition2_ = Dune::make_shared<MultiDomainCondition>(1);

        mortarPredicat_ = Dune::make_shared<MortarPredicat>(globalProblem_->gridView(), *condition1_, *condition2_ );
        mortarScalarGridFunctionSpace_ = Dune::make_shared<MortarScalarGridFunctionSpace>(globalProblem_->gridView(),
                                                                         *mortarfem_, *mortarPredicat_);

        gridFunctionSpace1_ = Dune::make_shared<GridFunctionSpace1>(*scalarGridFunctionSpace1_);
        gridFunctionSpace2_ = Dune::make_shared<GridFunctionSpace2>(*scalarGridFunctionSpace2_);
        mortarGridFunctionSpace_ = Dune::make_shared<MortarGridFunctionSpace>(*mortarScalarGridFunctionSpace_);

        mdGridFunctionSpace_ = Dune::make_shared<MultiDomainGridFunctionSpace>(globalProblem_->mdGrid(),
        											   *gridFunctionSpace1_,
        											   *gridFunctionSpace2_,
        											   *mortarGridFunctionSpace_);

        localOperator1_ = Dune::make_shared<LocalOperator1>(problem1_->model());
        localOperator2_ = Dune::make_shared<LocalOperator2>(problem2_->model());

        mdSubProblem1_ = Dune::make_shared<MultiDomainSubProblem1>(*localOperator1_, *condition1_);
        mdSubProblem2_ = Dune::make_shared<MultiDomainSubProblem2>(*localOperator2_, *condition2_);

        enrichedCouplingLocalOperator_ = Dune::make_shared<MultiDomainCouplingLocalOperator>(*globalProblem_);
        mdEnrichedCoupling_ = Dune::make_shared<MultiDomainCoupling>(*mdSubProblem1_, *mdSubProblem2_, *enrichedCouplingLocalOperator_);

        // TODO proper constraints stuff
        constraintsTrafo_ = Dune::make_shared<MultiDomainConstraintsTrafo>();

        mdGridOperator_ = Dune::make_shared<MultiDomainGridOperator>(*mdGridFunctionSpace_, *mdGridFunctionSpace_,
        									 *constraintsTrafo_, *constraintsTrafo_,
        								     *mdSubProblem1_, *mdSubProblem2_, *mdEnrichedCoupling_);

        matrix_ = Dune::make_shared<JacobianMatrix>(*mdGridOperator_);
        *matrix_ = 0;

        residual_.resize(matrix_->N());
    }

    void assemble()
    {
    	// assemble the matrix
    	*matrix_ = 0;
    	mdGridOperator_->jacobian(globalProblem_->model().curSol(), *matrix_);
//    	Dune::writeMatrixToMatlab(matrix_->base(), "matrix");
//    	printmatrix(std::cout, matrix_->base(), "global stiffness matrix", "row", 11, 3);

    	// calculate the global residual
    	residual_ = 0;
    	mdGridOperator_->residual(globalProblem_->model().curSol(), residual_);
//    	printvector(std::cout, residual_, "residual", "row", 200, 1, 3);
    }
    
    void reassembleAll(){}

    //! return const reference to matrix
    const JacobianMatrix& matrix() const
    { return *matrix_; }

    //! return const reference to residual
    const SolutionVector& residual() const
    { return residual_; }
    SolutionVector& residual()
    { return residual_; }

private:
    Problem *globalProblem_;
    SubProblem1 *problem1_;
    SubProblem2 *problem2_;

    Dune::shared_ptr<FEM1> fem1_;
    Dune::shared_ptr<FEM2> fem2_;
    Dune::shared_ptr<MortarFEM> mortarfem_;

    // probably not required
//    Dune::shared_ptr<Constraints1> constraints1_;
//    Dune::shared_ptr<Constraints2> constraints2_;

    Dune::shared_ptr<MortarPredicat> mortarPredicat_;

    Dune::shared_ptr<ScalarGridFunctionSpace1> scalarGridFunctionSpace1_;
    Dune::shared_ptr<ScalarGridFunctionSpace2> scalarGridFunctionSpace2_;
    Dune::shared_ptr<MortarScalarGridFunctionSpace> mortarScalarGridFunctionSpace_;

    Dune::shared_ptr<GridFunctionSpace1> gridFunctionSpace1_;
    Dune::shared_ptr<GridFunctionSpace2> gridFunctionSpace2_;
    Dune::shared_ptr<MortarGridFunctionSpace> mortarGridFunctionSpace_;
    Dune::shared_ptr< MultiDomainGridFunctionSpace> mdGridFunctionSpace_;

    Dune::shared_ptr<LocalOperator1> localOperator1_;
    Dune::shared_ptr<LocalOperator2> localOperator2_;

    Dune::shared_ptr<MultiDomainCondition> condition1_;
    Dune::shared_ptr<MultiDomainCondition> condition2_;

    Dune::shared_ptr<MultiDomainSubProblem1> mdSubProblem1_;
    Dune::shared_ptr<MultiDomainSubProblem2> mdSubProblem2_;

//    MultiDomainCouplingLocalOperator couplingLocalOperator_;
    Dune::shared_ptr<MultiDomainCouplingLocalOperator> enrichedCouplingLocalOperator_;
    Dune::shared_ptr<MultiDomainCoupling> mdEnrichedCoupling_;

    Dune::shared_ptr<MultiDomainConstraintsTrafo> constraintsTrafo_;
    Dune::shared_ptr<MultiDomainGridOperator> mdGridOperator_;

    Dune::shared_ptr<JacobianMatrix> matrix_;
    SolutionVector residual_;
};

} // namespace Dumux

#endif
