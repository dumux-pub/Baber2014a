/*****************************************************************************
 *   Copyright (C) 2009-2012  by Katherina Baber                      *
 *   Copyright (C) 2009 by Andreas Lauser, Bernd Flemisch                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_COUPLEDMORTARDROPSPROBLEM_HH
#define DUMUX_COUPLEDMORTARDROPSPROBLEM_HH

//stores drop information per interface node (has to be defined before the coupling operator is included)
namespace Dumux
{
struct DropInformation
{
    //part (in %) of the intersection that couples to gas flow or to drop
    double areaGas; //Ag/(area of cells per drop) -> percentage without drops
    double areaDrop; //Adrop/(area of cells per drop) -> percentage with drops
    double dropSurface;//actual surface in m^2/(area of cells per drop)
    //drop information
    double dropRadius;//actual radius in m
    double totalDropVolume;//actual volume in m^3
    double prevRhoVDrop;//LP (lambdarhoVdrop) from last time step, needed for storage term in coupling operator
    double qDropFormation; //source/sink when drop forms
    bool detached; //true = no drop on interface; false = drop grows
};
}

#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#include <dumux/io/vtkmultiwriter.hh>
#include <appl/xfem/common/helperfunctions.hh>

#include <dumux/multidomain/common/multidomainproblem.hh>
#include <dumux/modelcoupling/common/multidomainenrichedassembler.hh>

#include <dumux/material/fluidsystems/h2oairfluidsystem.hh>

namespace Dumux
{
template <class TypeTag>
class CoupledMortarDropsProblem;

namespace Properties
{
NEW_TYPE_TAG(CoupledMortarDropsProblem, INHERITS_FROM(MultiDomainEnriched));
};


/*!
 * \brief The problem class for the coupling of a two-component Stokes (stokes2c)
 *        and a two-phase two-component Darcy model (2p2c) using the mortar method.
 *        The formation, growth and detachment of drops on the interface is also described.
 *
 *        The problem class for the coupling of a isothermal two-component Stokes (stokes2c)
 *        and a isothermal two-phase two-component Darcy model (2p2c).
 *        It uses the 2p2cCoupling model and the Stokes2ccoupling model and provides
 *        the problem specifications for common parameters of the two submodels.
 *        The formation, growth and detachment of drops on the interface is also described.
 *        The initial and boundary conditions of the submodels are specified in the two subproblems,
 *        2p2cnisubproblem.hh and stokes2cnisubproblem.hh, which are accessible via the coupled problem.
 */
template <class TypeTag>
class CoupledMortarDropsProblem : public MultiDomainProblem<TypeTag>
{
    template<int dim>
    struct VertexLayout
    {
        bool contains(Dune::GeometryType gt)
        { return gt.dim() == 0; }
    };

    typedef CoupledMortarDropsProblem<TypeTag> ThisType;
    typedef MultiDomainProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, NewtonController) NewtonController;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) MortarSubProblem1TypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) MortarSubProblem2TypeTag;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, Problem) MortarSubProblem1;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, Problem) MortarSubProblem2;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, GridView) MortarSubProblem1GridView;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, GridView) MortarSubProblem2GridView;

    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, SolutionVector) MortarSubProblem2SolutionVector;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, PrimaryVariables) Stokes2cDropsPrimaryVariables;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, PrimaryVariables) TwoPTwoCDropsPrimaryVariables;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, VolumeVariables) VolumeVariables1;

    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, ElementVolumeVariables) ElementVolumeVariables2;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, VolumeVariables) VolumeVariables2;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, FluxVariables) FluxVariables1;
    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, FluxVariables) FluxVariables2;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, FluxVariables) BoundaryVariables2;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;

    typedef Dumux::VtkMultiWriter<MultiDomainGridView> VtkMultiWriter;

    typedef typename MortarSubProblem1GridView::template Codim<0>::Entity SubDomainElement1;
    typedef typename MortarSubProblem2GridView::template Codim<0>::Entity SubDomainElement2;

    typedef typename MultiDomainGrid::Traits::template Codim<0>::EntityPointer MultiDomainElementPointer;
    typedef typename MultiDomainGrid::SubDomainGrid SubDomainGrid;
    typedef typename SubDomainGrid::Traits::template Codim<0>::EntityPointer SubDomainElementPointer;
    typedef typename MultiDomainGrid::Traits::template Codim<0>::Entity MultiDomainElement;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, Indices) MortarSubProblem1Indices;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, Indices) MortarSubProblem2Indices;

    typedef typename GET_PROP_TYPE(MortarSubProblem1TypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(MortarSubProblem2TypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) GridFunctionSpace;

    enum { dim = MortarSubProblem1GridView::dimension };
    enum { // indices of the balance equations in the Stokes domain
        momentumXIdx1 = MortarSubProblem1Indices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx1 = MortarSubProblem1Indices::momentumYIdx, //!< Index of the y-component of the momentum balance
        momentumZIdx1 = MortarSubProblem1Indices::momentumZIdx, //!< Index of the z-component of the momentum balance
        massBalanceIdx1 = MortarSubProblem1Indices::massBalanceIdx, //!< Index of the mass balance
        transportEqIdx1 = MortarSubProblem1Indices::transportEqIdx, //!< Index of the transport equation
#if NI
        energyEqIdx1 = MortarSubProblem1Indices::energyEqIdx //!< Index of the energy equation
#endif
    };
    enum{ // indices of the PVs in teh Stokes domain
        velocityXIdx1 = MortarSubProblem1Indices::velocityXIdx,
        velocityYIdx1 = MortarSubProblem1Indices::velocityYIdx,
        velocityZIdx1 = MortarSubProblem1Indices::velocityZIdx,

        pressureIdx1 = MortarSubProblem1Indices::pressureIdx,
        massOrMoleFracIdx = MortarSubProblem1Indices::massOrMoleFracIdx,
#if NI
        temperatureIdx1 = MortarSubProblem1Indices::temperatureIdx
#endif
    };
    enum { // indices of the PVs in the Darcy domain
        massBalanceIdx2 = MortarSubProblem2Indices::pressureIdx,
        switchIdx2 = MortarSubProblem2Indices::switchIdx,
#if NI
        temperatureIdx2 = MortarSubProblem2Indices::temperatureIdx
#endif
    };
    enum { // indices of the balance equations
        contiTotalMassIdx2 = MortarSubProblem2Indices::contiNEqIdx,//GET_PROP_VALUE(MortarSubProblem2TypeTag, ReplaceCompIdx) ,
        transportEqIdx = MortarSubProblem2Indices::contiWEqIdx,//GET_PROP_VALUE(MortarSubProblem2TypeTag, TransportEqIdx)
#if NI
        energyEqIdx2 = MortarSubProblem2Indices::energyEqIdx
#endif
    };
    enum { transportCompIdx1 = MortarSubProblem1Indices::transportCompIdx };
    enum {
        wCompIdx2 = MortarSubProblem2Indices::wCompIdx,
        nCompIdx2 = MortarSubProblem2Indices::nCompIdx
    };
    enum {
        wPhaseIdx2 = MortarSubProblem2Indices::wPhaseIdx,
        nPhaseIdx2 = MortarSubProblem2Indices::nPhaseIdx
    };
    enum { phaseIdx = GET_PROP_VALUE(MortarSubProblem1TypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(MortarSubProblem1TypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(MortarSubProblem2TypeTag, NumEq)
    };

#if NI
#if COUPLINGCONCEPT //with storage term
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        lambdaT = 4, //!< lagrange multiplier for energy flux,
        totalLMP = 5, //!< total number of langrange multiplier vx, vy, p, labdaX
    };
#else //with pore velocity
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        pDrop = 4, //!< lagrange multiplier: drop water pressure
        lambdaT = 5, //!< lagrange multiplier for energy flux,
        totalLMP = 6 // 4 //!< total number of langrange multiplier vx, vy, p, labdaX, pdrop
    };
#endif
#else //isothermal
#if COUPLINGCONCEPT //with storage term
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        totalLMP = 4, //!< total number of langrange multiplier vx, vy, p, labdaX
    };
#else //with pore velocity
    enum{ // indices on the mortar elements
//        lambdaBJ = 0,
        lambdaVy = 0, //!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        lambdaP = 1, //!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        lambdaX = 2, //!< lagrange multiplier standing for normal component fluxes
        rhoVdrop = 3, //!< lagrange multiplier: rhow*drop volume,
        pDrop = 4, //!< lagrange multiplier: drop water pressure
        totalLMP = 5 // 4 //!< total number of langrange multiplier vx, vy, p, labdaX, pdrop
    };
#endif
#endif

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename MortarSubProblem1GridView::template Codim<dim>::EntityPointer VertexPointer1;
    typedef typename MortarSubProblem2GridView::template Codim<dim>::EntityPointer VertexPointer2;

    typedef typename MultiDomainGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename SubDomainGrid::template Codim<0>::LeafIterator SubDomainElementIterator;
    typedef typename MortarSubProblem1GridView::IntersectionIterator StokesIntersectionIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator SubDomainInterfaceIterator;
    typedef typename MultiDomainGrid::template Codim<dim>::LeafIterator MultiDomainVertexIterator;
    typedef typename MultiDomainGrid::LeafSubDomainInterfaceIterator::Intersection Intersection;//is this the right type?
    typedef typename Dune::PDELab::IntersectionGeometry<Intersection>::IntersectionGeometry IntersectionGeometry;
    typedef typename MultiDomainGridView::IndexSet::IndexType IndexType;

    ////////////
    //maps that store information of integration points for the mortar method
    typedef std::vector<IntegrationPointGeometry> IntegrationPointGeometryVector;
    typedef std::vector<IntegrationPointGeometryCoupling> IntegrationPointGeometryVectorCoupling;
    typedef std::map<IndexType, IntegrationPointGeometryVector> IntegrationPointGeometryMap;
    typedef std::map<IndexType, IntegrationPointGeometryVectorCoupling> IntegrationPointGeometryMapCoupling;
    ////////////
    //map that stores drop information and division of interface area into normal and drop coupling per intersection
    typedef std::pair<IndexType,DropInformation> DropInformationPair;
    typedef typename GET_PROP_TYPE(TypeTag, DropMap) DropMap;
    typedef typename DropMap::iterator DropMapIterator;
    ////////////

    struct IndexPositionPair {
        IndexType index;
        GlobalPosition globalPos;
    };

    struct PoreClass {
        Scalar meanPoreRadius;
        Scalar percentage;
    };
    //compare structs for sorting the standard vector containing node indices and global Position
    struct SortVectorIndex{
        bool operator() (IndexPositionPair idxPosPair1, IndexPositionPair idxPosPair2) { return (idxPosPair1.index < idxPosPair2.index); }
    };
    struct SortVectorPosition{
        bool operator() (IndexPositionPair idxPosPair1, IndexPositionPair idxPosPair2) { return (idxPosPair1.globalPos[0] < idxPosPair2.globalPos[0]); }
    };

public:
    CoupledMortarDropsProblem(MultiDomainGrid &mdGrid,
            TimeManager &timeManager)
: ParentType(mdGrid, timeManager), meanPoreRadius_(0.0)
{
        mortarSubProblem1_ = this->sdID1();
        mortarSubProblem2_ = this->sdID2();

        //contact angle (convert degress to radians)
        theta_ =  M_PI / 180 * GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FuelCell, Theta);
        deltaTheta_ = M_PI / 180 * GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FuelCell, ContactAngleHysteresis);
        //number of interface nodes contributing to one drop
        nodesPerDrop_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FuelCell, NodesPerDrop);

        scvfAreaX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX) / GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumberOfCellsX) / 2;
        scvfAreaY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) / GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Grid, NumberOfCellsY) / 2;
        runUpDistanceX_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX); // first part of the interface without coupling

        simpleDragForce_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, FuelCell, SimpleDragForce);
}

    ~CoupledMortarDropsProblem()
    {
        //close output
        dropVolume_.close();
        mortar_.close();
    };

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        //determine number, global indices and position of interface nodes, drop borders and pore distribution
        collectInterfaceInformation();

        //init sub-models and mortar-dofs
        ParentType::init();

        //create dropMap and fill with zeros
        initializeDropMap();

        std::cout << "Writing drop Volume over time\n";
        if (this->timeManager().time() == 0)
        {
            dropVolume_.open("dropVolumes.out");
            dropVolume_ << 0.0 << " ";
            for(int drop=0; drop < numberOfDrops_; drop ++)
                dropVolume_ << 0.0 << " "<< 0.0 << " ";
            dropVolume_ <<std::endl;

            mortar_.open("mortarRhoVDrop.out");

            counter_ = 1;
        }
        else
        {
            dropVolume_.open("dropVolumes.out", std::ios_base::app);
            mortar_.open("mortarRhoVDrop.out", std::ios_base::app);
        }
    }

    /*!
     * \brief Called by the time manager before the time integration.
     */
    void preTimeStep()
    {
        ParentType::preTimeStep();
        //check whether drops form depending on pressure and pore distribution
//       dropFormation();
    }

    /*!
     * \brief Collects all relevant information for drop description at interface:
     *           - number of interface nodes
     *           - global indices and position of interface nodes
     *           - borders of drops
     *           - pore distribution sorted into classes per radius
     */
    void collectInterfaceInformation()
    {
        numInterfaceNodes_ = 0;//determine number of interface nodes
        std::vector<IndexType> interfaceNodes; //vector to check whether node has already been visited
        const MultiDomainGridView& mdGridView = this->gridView();

        GlobalPosition globalPos;
        IndexType globalVertexIdx;

        //loop over intersections
        const SubDomainInterfaceIterator endIfIt = this->mdGrid().leafSubDomainInterfaceEnd(mortarSubProblem1_, mortarSubProblem2_);
        for (SubDomainInterfaceIterator ifIt =
                this->mdGrid().leafSubDomainInterfaceBegin(mortarSubProblem1_, mortarSubProblem2_); ifIt != endIfIt; ++ifIt)
        {
            MultiDomainElementPointer mdElementPointer1 = ifIt->firstCell();
            MultiDomainElementPointer mdElementPointer2 = ifIt->secondCell();
            const MultiDomainElement& mdElement1 = *(mdElementPointer1);
            const MultiDomainElement& mdElement2 = *(mdElementPointer2);
            const int faceIdx1 = ifIt->indexInFirstCell();
            const int faceIdx2 = ifIt->indexInSecondCell();

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement1.size(faceIdx1, 1, dim);

            //loop over nodes of intersection
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                //determine global vertex index and global position
                if(mdElement1.level() == mdElement2.level() || mdElement1.level() > mdElement2.level())
                {
                    const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, nodeInFace, dim);
                    globalVertexIdx = mdGridView.indexSet().subIndex(mdElement1, vertInElem1, dim);
                    globalPos = mdElement1.geometry().corner(vertInElem1);
                }
                else
                {
                    const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, nodeInFace, dim);
                    globalVertexIdx = mdGridView.indexSet().subIndex(mdElement2, vertInElem2 , dim);
                    globalPos = mdElement2.geometry().corner(vertInElem2);
                }

                //check whether node has already been visited
                if(std::find(interfaceNodes.begin(), interfaceNodes.end(), globalVertexIdx)!= interfaceNodes.end())
                    continue;
                else
                    interfaceNodes.push_back(globalVertexIdx);

                //fill vector witch struct storing global node index and global position
                IndexPositionPair idxPosPair;
                idxPosPair.index = globalVertexIdx;
                idxPosPair.globalPos = globalPos;

                interfaceNodeInformation_.push_back(idxPosPair);

                numInterfaceNodes_++;
            }
        }

        //sort vector in ascending order of global-X-Position
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorPosition());

        //collect general drop information
        //check whether number of interface nodes can be divided by number of drops
        if(numInterfaceNodes_ % nodesPerDrop_)
            std::cerr << "Interface nodes and nodesPerDrop do not match in coupled problem\n";
        numberOfDrops_ = numInterfaceNodes_/nodesPerDrop_;

        detached_.resize(numberOfDrops_);
        closeToDetaching_.resize(numberOfDrops_);
        lastDragForce_.resize(numberOfDrops_);
        totalDropVolumes_.resize(numberOfDrops_);

        //resize Matrix that stores global Indices belonging to one drop (filled in initializeDropMap)
        dropIdxMatrix_.resize(numberOfDrops_);

        //determine borders between drops and set status to detached
        for (int drop=1; drop <= numberOfDrops_; drop++)
        {
            const int borderVertex = nodesPerDrop_*drop - 1;//ith drop of the interface
            globalPos = interfaceNodeInformation_[borderVertex].globalPos;

            borders_.push_back(globalPos);
        }
        for (int drop=0; drop < numberOfDrops_; drop++)
        {
            detached_[drop] = true;
            closeToDetaching_[drop] = false;
            lastDragForce_[drop] = 0.0;
            totalDropVolumes_[drop] = 0.0;
        }

        if(interfaceNodeInformation_[numInterfaceNodes_-1].globalPos[0] != GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX))
            std::cerr << "Last drop "<< interfaceNodeInformation_[numInterfaceNodes_-1].globalPos[0]<< " border does not match domain size "<< GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightX)<<" !\n";

        //sort pore distribution into classes sorted by increasing mean radius
        //ToDo use some real pore distribution, call function determinePoreDistrubution()
        //       const int numberOfClasses = 3;
        //       poreDistribution_.resize(numberOfClasses);
        //       //Gurau&Mann(2010)
        //       poreDistribution_[0].meanPoreRadius = 1e-8; //geraten
        //       poreDistribution_[0].percentage = 0.3;
        //       poreDistribution_[1].meanPoreRadius = 5e-7; //geraten
        //       poreDistribution_[1].percentage = 0.2;
        //       poreDistribution_[2].meanPoreRadius = 19.5e-6;//maximum pore radius according to Gurau&Mann(2010)
        //       poreDistribution_[2].percentage = 0.5; //fraction according to Gurau&Mann(2010)

        //Acosta et al. (2006)
        const int numberOfClasses = 4;
        poreDistribution_.resize(numberOfClasses);
        //0-100nm -> 50nm
        poreDistribution_[0].meanPoreRadius = 5e-8;
        poreDistribution_[0].percentage = 0.15;
        //100-1000nm -> 500nm
        poreDistribution_[1].meanPoreRadius = 5e-7;
        poreDistribution_[1].percentage = 0.3;
        //1000 - 10 000nm -> 5000nm
        poreDistribution_[2].meanPoreRadius = 5e-6;
        poreDistribution_[2].percentage = 0.3;
        //10 000nm -100 000mn -> 50 000nm
        poreDistribution_[3].meanPoreRadius = 50e-6;
        poreDistribution_[3].percentage = 0.25;

        for(int i=0; i<numberOfClasses; i++)
            meanPoreRadius_ += poreDistribution_[i].meanPoreRadius * poreDistribution_[i].percentage;

        //       const int numberOfClasses = 5;
        //      poreDistribution_.resize(numberOfClasses);
        ////       poreDistribution_[0].meanPoreRadius = 1e-9;
        ////       poreDistribution_[0].percentage = 0.;
        //       poreDistribution_[0].meanPoreRadius = 1e-8;
        //       poreDistribution_[0].percentage = 0.05;
        //       poreDistribution_[1].meanPoreRadius = 5e-7;
        //       poreDistribution_[1].percentage = 0.2;
        //       poreDistribution_[2].meanPoreRadius = 1e-6;
        //       poreDistribution_[2].percentage = 0.35;
        //       poreDistribution_[3].meanPoreRadius = 50e-6;
        //       poreDistribution_[3].percentage = 0.2;
        //       poreDistribution_[4].meanPoreRadius = 100e-6;
        //       poreDistribution_[4].percentage = 0.2;
    }

    /*!
     * \brief Called by the Dumux::TimeManager to initialize the Lagrange multiplier dofs
     * on the mortar elements.
     */
    void initMortarElements()
    {
        const MultiDomainGrid& mdGrid = this->mdGrid();
        const MultiDomainGridView& mdGridView = this->gridView();
        SolutionVector &curSol = ParentType::model().curSol();

        FVElementGeometry1 fvElemGeom1;
        FVElementGeometry2 fvElemGeom2;
        ElementVolumeVariables1 elemVolVars1;
        ElementVolumeVariables2 elemVolVars2;
        BoundaryTypes1 boundaryTypes1;
        BoundaryTypes2 boundaryTypes2;

        //degrees of freedom of subdomains one and two (without mortar elements)
        const int numDofs = this->model().numDofs();

        std::vector<IndexType> interfaceNodes; //vector to check whether node has already been visited
        //sort vector in ascending order of global Index to access global sol vector
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorIndex());

        //loop over interface to initialize mortar elements (necessary to create volume variables)
        const SubDomainInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(mortarSubProblem1_, mortarSubProblem2_);
        for (SubDomainInterfaceIterator ifIt = mdGrid.leafSubDomainInterfaceBegin(mortarSubProblem1_, mortarSubProblem2_);
                ifIt != endIfIt; ++ifIt)
        {
            const int firstFaceIdx = ifIt->indexInFirstCell();
            const MultiDomainElementPointer firstElementPointer = ifIt->firstCell();
            const MultiDomainElement& mdElement1 = *firstElementPointer;
            const SubDomainElementPointer sdElementPointer1 =mdGrid.subDomain(mortarSubProblem1_).subDomainEntityPointer(mdElement1);
            const SubDomainElement1& sdElement1 =*sdElementPointer1;
            const int secondFaceIdx = ifIt->indexInSecondCell();
            const MultiDomainElementPointer secondElementPointer = ifIt->secondCell();
            const MultiDomainElement& mdElement2 = *secondElementPointer;
            const SubDomainElementPointer sdElementPointer2 = mdGrid.subDomain(mortarSubProblem2_).subDomainEntityPointer(mdElement2);
            const SubDomainElement1& sdElement2 =*sdElementPointer2;

            fvElemGeom1.update(this->sdGridView1(), sdElement1);
            elemVolVars1.update(this->sdProblem1(),
                    sdElement1,
                    fvElemGeom1,
                    false);
            fvElemGeom2.update(this->sdGridView2(), sdElement2);
            elemVolVars2.update(this->sdProblem2(),
                    sdElement2,
                    fvElemGeom2,
                    false);

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement1.size(firstFaceIdx, 1, dim);
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem1 = referenceElement1.subEntity(firstFaceIdx, 1, nodeInFace, dim);
                const int vertInElem2 = referenceElement2.subEntity(secondFaceIdx, 1, nodeInFace, dim);

                //check whether node has already been visited
                int globalVertexIdx = mdGridView.indexSet().subIndex(mdElement1, vertInElem1, dim);
                if(std::find(interfaceNodes.begin(), interfaceNodes.end(), globalVertexIdx)!= interfaceNodes.end())
                    continue;
                else
                    interfaceNodes.push_back(globalVertexIdx);

                //obtain consecutive node number on interface to access global solution vector
                int nodeNumber;
                for(int node=0; node < interfaceNodeInformation_.size(); node++)
                {
                    if(interfaceNodeInformation_[node].index == globalVertexIdx)
                    {
                        nodeNumber = node;
                        break;
                    }
                }
                // obtain the boundary types
                const VertexPointer1 vPtr1 = sdElement1.template subEntity<dim>(vertInElem1);
                const VertexPointer2 vPtr2 = sdElement2.template subEntity<dim>(vertInElem2);

                this->sdProblem1().boundaryTypes(boundaryTypes1, *vPtr1);
                this->sdProblem2().boundaryTypes(boundaryTypes2, *vPtr2);

                const VolumeVariables1& vertVars1 = elemVolVars1[vertInElem1];
                const VolumeVariables2& vertVars2 = elemVolVars2[vertInElem2];

                const int firstBfIdx = fvElemGeom1.boundaryFaceIndex(firstFaceIdx, nodeInFace);
                const Scalar firstBfArea = fvElemGeom1.boundaryFace[firstBfIdx].area;

                //initialize velocities
                //                if(boundaryTypes2.isMortarCoupling(momentumXIdx1))
                //                curSol[numDofs + interfaceNodeIdx + (interfaceNodeIdx * (totalLMP -1)) + velocityXFF] = vertVars1.velocity[0];
                //                else
                //                    curSol[numDofs + interfaceNodeIdx + (interfaceNodeIdx * (totalLMP -1)) + velocityXFF] = 0.0;
                if(boundaryTypes2.isMortarCoupling(massBalanceIdx2))
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaVy] = vertVars1.velocity()[1]*vertVars1.density();
                else
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaVy] = 0.0;
                //initialize pressure
                if(boundaryTypes1.isMortarCoupling(momentumYIdx1))
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaP] = vertVars2.pressure(nPhaseIdx2);//hydrophobic material: pn=pgas=pnon-water
                else
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaP] = vertVars2.pressure(nPhaseIdx2);
                //initialize component flux
                //Initialization with flux from first domain (I cant't think of anything better right now)
                const BoundaryVariables1 boundaryVars1(this->sdProblem1(),
                        sdElement1,
                        fvElemGeom1,
                        firstBfIdx,
                        elemVolVars1,
                        /* on boundary*/ true);

                Scalar tmpAdv = boundaryVars1.normalVelocity()*vertVars1.density()
                               *vertVars1.fluidState().massFraction(phaseIdx, transportCompIdx1);
                // diffusive flux of comp1 component in phase0
                Scalar tmpDiff = 0;
                for (int i = 0; i < dim; ++ i)
                {
                    tmpDiff += boundaryVars1.moleFractionGrad(transportCompIdx1)[i]*boundaryVars1.face().normal[i];
                }
                tmpDiff *= -1;
                tmpDiff *= boundaryVars1.diffusionCoeff(transportCompIdx1)*boundaryVars1.molarDensity()*FluidSystem::molarMass(transportCompIdx1);

                if(boundaryTypes1.isMortarCoupling(transportEqIdx1) && boundaryTypes2.isMortarCoupling(transportEqIdx))
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaX] = (tmpAdv + tmpDiff)/firstBfArea; //TESTING!!!
                else
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaX] = 0.0;

                //initialize drop volume with 0
                curSol[numDofs + (nodeNumber * totalLMP) + rhoVdrop] = 0.0;

#if NI
                //simplification: use only convective flux for initialization
                Scalar tmpConv = boundaryVars1.normalVelocity()*vertVars1.density() * vertVars1.enthalpy();
                Scalar tmpCond = 0;
                for (int i = 0; i < dim; ++ i)
                {
                   tmpCond += boundaryVars1.temperatureGrad()[i]*boundaryVars1.face().normal[i];
                }
                tmpCond *= -1;
                tmpCond *= boundaryVars1.thermalConductivity()*boundaryVars1.density();
                if(boundaryTypes1.isMortarCoupling(energyEqIdx1) && boundaryTypes2.isMortarCoupling(energyEqIdx2))
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaT] = (tmpConv + tmpCond)/firstBfArea; //TESTING!!!
                else
                    curSol[numDofs + (nodeNumber * totalLMP) + lambdaT] = 0.0;
#endif

#if !COUPLINGCONCEPT //with pore velocity
                //initialize drop pressure with pg
                curSol[numDofs + (nodeNumber * totalLMP) + pDrop] = vertVars1.pressure();
#endif
            }
        }
#if HAVE_VALGRIND
        for (int i=0; i < curSol.size(); i++) {
            Valgrind::CheckDefined(curSol[i]);
        }
#endif // HAVE_VALGRIND
    }

    /*!
     * \brief Initialize the information about the drops per interface vertex assuming that no drops are present yet.
     *
     * Initialize the information about the drops per interface vertex.
     * A matrix which connects a drop with its contributing vertex (global vertex idx) is created.
     * The area of cells per drop is calculated.
     * A map storing drop properties per interface vertex is filled. The drop properties are initialized
     * with zero. The drop radii are set to 1e20 to  turn 2 gamma/r to zero in coupling condition.
     */
    void initializeDropMap()
    {
        //sort vector in ascending order of global Position
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorPosition());

        IndexType globalVertexIdx;
        GlobalPosition globalPos;
        std::vector<Scalar> fractionalInterfaceArea(numberOfDrops_, 0.0);
        for(int node=0; node < interfaceNodeInformation_.size(); node++)
        {
            globalVertexIdx = interfaceNodeInformation_[node].index;
            globalPos = interfaceNodeInformation_[node].globalPos;

            int boundaryFactor = 2; //if not on boundary, two scvf belong to one node
            //determine if node is on interface corners
            if(this->sdProblem2().isInterfaceCornerPoint(globalPos))
                boundaryFactor = 1;

            //determine node indices belonging to each drop and area of cells contributing to drop
            for (int drop=0; drop < numberOfDrops_; drop++ )
                if(globalPos[0] <= borders_[drop][0])
                {
                    dropIdxMatrix_[drop].push_back(globalVertexIdx);
                    fractionalInterfaceArea[drop] += boundaryFactor*scvfAreaX_;
                    break;
                }

            //fill drop map
            //assume: no drops at the beginning
            DropInformation dropInfo;
            dropInfo.dropRadius = 1e20;//no drop, big radius so that 2gamma/r is zero and "traditional" coupling conditions are applied
            dropInfo.totalDropVolume = 0.0;
            dropInfo.prevRhoVDrop = 0.0;
            dropInfo.dropSurface = 0.0;
            dropInfo.areaDrop = 0.0;//1 for TESTING reasons only!!!! needs to be zero
            dropInfo.areaGas = 1-dropInfo.areaDrop;
            dropInfo.qDropFormation = 0.0;
            dropInfo.detached = true; //no drop

            //fill map (connect intersection index and drop information)
            dropMap_.insert(DropInformationPair(globalVertexIdx,dropInfo));
        }

        fractionalInterfaceArea_ = fractionalInterfaceArea;
        for (int drop=0; drop < numberOfDrops_; drop++)
                std::cout<<"Fractional interface area per drop: "<<fractionalInterfaceArea[drop];
        std::cout<<std::endl;
    }

    /*!
     * \brief If drops form (determined by function invasionCondition()),
     *  calculate drop volumes and radii. Initialize lagrange multiplier of the drop volume.
     *
     * Depending on the pore-size distribution on the interface and on the pressure distribution,
     * the interface is divided into areas with and without drops. Furthermore, the initial
     * drop radii and drop volumina are calculated. The information is stored in a map connecting
     * each intersection with the according information.
     *
     * Called before each Newton step as long as drops are not formed.
     */
    void dropFormation()
    {
        std::vector<IndexType> interfaceNodes;//vector to check whether node has already been visited
        //sort vector interfaceNodeInformation_ in ascending order of global Index
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorIndex());

        //degrees of freedom of domain 1 and 2 without mortar elements
        const int numDofs = this->model().numDofs();
        SolutionVector &curSol = ParentType::model().curSol();

        FVElementGeometry2 fvElemGeom2;
        ElementVolumeVariables2 elemVolVars2;

        //initial drop volume after drop formation according to pore distribution
        std::vector<Scalar> dropVolume(numberOfDrops_, 0.0);
        //water flux across the interface (into the drop) according to current pressure gradient
        std::vector<Scalar> pmFlux(numberOfDrops_, 0.0);

        //1st check if pores are penetrated by water and drops are formed, if yes store resulting drop volume and water flux feeding the drop
        invasionCondition(dropVolume, pmFlux);

        //2nd: update drop map
        const MultiDomainGrid& mdGrid = this->mdGrid();
        const MultiDomainGridView& mdGridView = this->gridView();
        const SubDomainInterfaceIterator endIfIt = this->mdGrid().leafSubDomainInterfaceEnd(mortarSubProblem1_, mortarSubProblem2_);
        for (SubDomainInterfaceIterator ifIt = this->mdGrid().leafSubDomainInterfaceBegin(mortarSubProblem1_, mortarSubProblem2_);
                ifIt != endIfIt; ++ifIt)
        {
            IntersectionGeometry intersectionGeometry(*ifIt,0);
            const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
            const MultiDomainElement& mdElement2 = *mdElementPointer2;
            const SubDomainElementPointer sdElementPointer2 = mdGrid.subDomain(mortarSubProblem2_).subDomainEntityPointer(mdElement2);
            const SubDomainElement1& sdElement2 =*sdElementPointer2;
            int faceIdx2 = intersectionGeometry.indexInOutside();

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement2.size(faceIdx2, 1, dim);

            fvElemGeom2.update(this->sdGridView2(), sdElement2);
            elemVolVars2.update(this->sdProblem2(),
                    sdElement2,
                    fvElemGeom2,
                    false);

            //loop over vertices of face
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, nodeInFace, dim);
                IndexType globalVertexIdx = mdGridView.indexSet().subIndex(mdElement2, vertInElem2 , dim);

                //check whether node has already been visited
                if(std::find(interfaceNodes.begin(), interfaceNodes.end(), globalVertexIdx)!= interfaceNodes.end())
                    continue;
                else
                    interfaceNodes.push_back(globalVertexIdx);

                const VolumeVariables2& vertVars2 = elemVolVars2[vertInElem2];

                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: drop formation \n";
                }

                //only continue if drop is detached / no drop is yet present
                if(!dropMapIt->second.detached)
                    continue;

                //obtain consecutive node number on interface to access global solution vector
                int nodeNumber;
                for(int node=0; node < interfaceNodeInformation_.size(); node++)
                {
                    if(interfaceNodeInformation_[node].index == globalVertexIdx)
                    {
                        nodeNumber = node;
                        break;
                    }
                }

                //check to which drop globalVertexIdx contributes  - 1st part
                for(int drop=0; drop < numberOfDrops_; drop++)
                {
                    //calculate flux that is necessary to form a drop determined by the pore-size and pressure distribution
                    Scalar dropFlux = dropVolume[drop] * vertVars2.density(wPhaseIdx2) / this->timeManager().timeStepSize();

                    //only calculate drop information if drop has formed and if the flux from the pm is sufficient to form it
                    if(dropVolume[drop] > 0.0 && pmFlux[drop] > 0.0)// && pmFlux[drop] >= dropFlux)
                    {
                        //check to which drop globalVertexIdx contributes  - 2nd part
                        if(std::find(dropIdxMatrix_[drop].begin(), dropIdxMatrix_[drop].end(), globalVertexIdx) != dropIdxMatrix_[drop].end())
                        {
                            //calculate the drop volume forming due to the water flux from the porous medium which was calculated in invasionCondition()
                            Scalar volumeFromFlux = pmFlux[drop] / vertVars2.density(wPhaseIdx2) * this->timeManager().timeStepSize();
//                            std::cout<< "required flux for volume : "<<dropFlux<<" flux: "<<pmFlux[drop]<<"drop "<<drop<<" can be formed\n";
                            std::cout<<"properties of newly formed drop "<<drop<<" are calculated: ";
                            /*********    3D spherical drop    *******************/
                            //                          radius = pow((3.0/M_PI * volumeFromFlux / (pow((1.0 - std::cos(theta_)),2.0) * (2.0 + std::cos(theta_)))),1.0/3.0); //the same for all nodes contributing to a drop
                            //                          dropMapIt->second.dropRadius = radius;
                            //                          //calculate areal fractions of interface
                            //                          dropSurface = 2.0 * M_PI * pow(dropMapIt->second.dropRadius,2.0) * (1.0 - std::cos(theta_)); //total drop surface
                            //                          dropMapIt->second.dropSurface = dropSurface / fractionalInterfaceArea_[drop];//total drop surface divided by total area of contributing cells
                            //                          dropArea = M_PI *  pow(dropMapIt->second.dropRadius * std::sin(theta_),2.0);//total drop contact area
                            //                          dropMapIt->second.areaDrop = dropArea / fractionalInterfaceArea_[drop];//total drop contact area divided by total area of contributing cells
                            /*********    2D zylindrical drop of depth 1m    *******************/
                            Scalar phi = 2 * theta_;
                            Scalar radius = 2 * volumeFromFlux / (phi - std::sin(phi)) /* /1m */;
                            radius = sqrt(radius);
                            dropMapIt->second.dropRadius = radius;
                            //calculate drop surface and volume
                            Scalar dropSurface = dropMapIt->second.dropRadius * phi /*  *1m */;//total drop surface
                            dropMapIt->second.dropSurface = dropSurface/ fractionalInterfaceArea_[drop];//total drop surface divided by total area of contributing cells
                            //TESTING: set adrop = 1
                            Scalar dropArea = 2  * dropMapIt->second.dropRadius * std::sin(theta_)/* /1m */;//total drop contact area
                            dropMapIt->second.areaDrop = dropArea / fractionalInterfaceArea_[drop];//total drop contact area divided by total area of contributing cells
                            dropMapIt->second.areaGas = 1.0 - dropMapIt->second.areaDrop;
                            dropMapIt->second.detached = false;
                            detached_[drop] = false;
                            std::cout<<"radius: "<<radius<< " drop area in percent: "<<dropMapIt->second.areaDrop*100<<std::endl;
                            dropMapIt->second.prevRhoVDrop = 0.0;//previous lambdaRhoVdrop is zero since drop just formed
                            //intialize / set lambaVdrop in solution vector
                            Scalar rhoVDrop = volumeFromFlux/nodesPerDrop_ * vertVars2.density(wPhaseIdx2) * vertVars2.fluidState().massFraction(wPhaseIdx2, wCompIdx2);
                            curSol[numDofs + nodeNumber * totalLMP + rhoVdrop] = rhoVDrop;
                            //source/sink term due to dropformation need in local operator to define flux from pm during current time-step
                            dropMapIt->second.qDropFormation = rhoVDrop / this->timeManager().timeStepSize();
                            std::cout<<rhoVDrop<<std::endl;
                        }
                    }
                    else
                    {
                        if(detached_[drop])
                            std::cout<< "required flux for volume : "<<dropFlux<<" flux: "<<pmFlux[drop]<<"drop "<<drop<<" cannot be formed\n";
                    }
                }
            }
        }
    }


    /*!
     * \brief Evaluate if drops form depending on pore size and pressure distribution.
     *
     * Depending on the pore-size distribution on the interface, on the pressure distribution and on the
     * resulting fluxes, the interface is divided into areas with and without drops. From this, the initial
     * drop radii and drop volumina can be calculated.
     */
    void invasionCondition(std::vector<Scalar> &dropVolume, std::vector<Scalar> &pmFlux)
    {
        //as starting point: assume the same pore distribution is valid for the whole interface and for each drop area
        std::vector<IndexType> interfaceNodes;//vector to check whether node has already been visited

        FVElementGeometry1 fvElemGeom1;
        FVElementGeometry2 fvElemGeom2;
        ElementVolumeVariables1 elemVolVars1;
        ElementVolumeVariables2 elemVolVars2;

        //loop over interface
        const MultiDomainGrid& mdGrid = this->mdGrid();
        const MultiDomainGridView& mdGridView = this->gridView();
        const SubDomainInterfaceIterator endIfIt = this->mdGrid().leafSubDomainInterfaceEnd(mortarSubProblem1_, mortarSubProblem2_);
        for (SubDomainInterfaceIterator ifIt = this->mdGrid().leafSubDomainInterfaceBegin(mortarSubProblem1_, mortarSubProblem2_);
                ifIt != endIfIt; ++ifIt)
        {
            //turn intersection into intersectionGeometry (ifIt = pointer, *ifIt = intersection)
            IntersectionGeometry intersectionGeometry(*ifIt,0);
            const MultiDomainElementPointer mdElementPointer1 = intersectionGeometry.inside();
            const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
            const MultiDomainElement& mdElement1 = *mdElementPointer1;
            const MultiDomainElement& mdElement2 = *mdElementPointer2;
            const SubDomainElementPointer sdElementPointer1 = mdGrid.subDomain(mortarSubProblem1_).subDomainEntityPointer(mdElement1);
            const SubDomainElementPointer sdElementPointer2 = mdGrid.subDomain(mortarSubProblem2_).subDomainEntityPointer(mdElement2);
            const SubDomainElement1& sdElement1 =*sdElementPointer1;
            const SubDomainElement1& sdElement2 =*sdElementPointer2;
            int faceIdx1 = intersectionGeometry.indexInInside();
            int faceIdx2 = intersectionGeometry.indexInOutside();

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement1 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement1.type());
            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement1.size(faceIdx1, 1, dim);

            fvElemGeom1.update(this->sdGridView1(), sdElement1);
            elemVolVars1.update(this->sdProblem1(),
                    sdElement1,
                    fvElemGeom1,
                    false);
            fvElemGeom2.update(this->sdGridView2(), sdElement2);
            elemVolVars2.update(this->sdProblem2(),
                    sdElement2,
                    fvElemGeom2,
                    false);

            //loop over vertices of current face
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, nodeInFace, dim);
                const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, nodeInFace, dim);

                //get the global index of the current vertex
                IndexType globalVertexIdx;
                GlobalPosition globalPos;
                if(mdElement1.level() == mdElement2.level() || mdElement1.level() > mdElement2.level())
                {
                    globalVertexIdx = mdGridView.indexSet().subIndex(mdElement1, vertInElem1, dim); //auch mit vertexMappter moeglich?
                    globalPos = mdElement1.geometry().corner(vertInElem1);
                }
                else
                {
                    globalVertexIdx = mdGridView.indexSet().subIndex(mdElement2, vertInElem2 , dim);
                    globalPos = mdElement2.geometry().corner(vertInElem2);
                }

                //check whether node has already been visited
                if(std::find(interfaceNodes.begin(), interfaceNodes.end(), globalVertexIdx)!= interfaceNodes.end())
                    continue;
                else
                    interfaceNodes.push_back(globalVertexIdx);

                //get drop information corresponding to vertex
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: invasion condition \n";
                }

                //only continue if drop is detached (not there), only then a new one may form
                if(!dropMapIt->second.detached)
                    continue;

                //get corresponding volume variables
                const VolumeVariables1& vertVars1 = elemVolVars1[vertInElem1];
                const VolumeVariables2& vertVars2 = elemVolVars2[vertInElem2];
                //get flux variables of Darcy domain
                const int boundaryFaceIdx = fvElemGeom2.boundaryFaceIndex(faceIdx2, nodeInFace);
                BoundaryVariables2 boundaryVars2(this->sdProblem2(),
                        sdElement2,
                        fvElemGeom2,
                        boundaryFaceIdx,
                        elemVolVars2,
                        true);

                //determine if node is on boundary, to determine the interface area belonging to the drop (1 or 2 scvf)
                int boundaryFactor = 2; //if not on boundary, two scvf belong to one node
                //determine if node is on interface corners
                if(this->sdProblem2().isInterfaceCornerPoint(globalPos))
                    boundaryFactor = 1;

                //calculate water flux that would flow (kg/s) across the interface with the current pressure conditions
                Scalar waterFluxPM = boundaryVars2.volumeFlux(wPhaseIdx2); //m^3/s
                waterFluxPM *= vertVars2.density(wPhaseIdx2);//kg/s
                //this is so far the flux across one scvf, so multiply with 2 = boundaryFactor (each node is only visited once, and I do not know how to do it otherwise)
                waterFluxPM *= boundaryFactor; //SIMPLIFICATION!!!

                //get pressures in the two domain and the surface tension to evaluate the pore invasion condition
                const Scalar gasPressureFF = vertVars1.pressure(); //TESTING //HIER KANN MAN DEN AKTUELLEN TROPFEN NEHMEN, WENN DIE KOPPLUNGSBEDINGUNG VON TROPFENBILDNG ABH������NGT
                const Scalar waterPressurePM = vertVars2.pressure(wPhaseIdx2);
                const Scalar surfaceTension = FluidSystem::surfaceTension(vertVars2.fluidState());

                //check to which drop globalVertexIdx contributes
                for(int drop=0; drop < numberOfDrops_; drop++)
                {
                    if(std::find(dropIdxMatrix_[drop].begin(), dropIdxMatrix_[drop].end(), globalVertexIdx) != dropIdxMatrix_[drop].end())
                    {
                        //add up all water fluxes that would contribute to the drop
                        pmFlux[drop] += waterFluxPM;

                        //loop over classes of pore distribution and check for each radius, if the pores get invaded by water
                        for(int r=0; r<poreDistribution_.size(); r++)
                        {
                            Scalar entryPressure = 2 * surfaceTension / poreDistribution_[r].meanPoreRadius;

                            if(waterPressurePM >= gasPressureFF + entryPressure) //pore invasion condition
                            {
                                //determine number of pores: phi * area * % /poreArea
                                //                               const Sclara poreArea = M_PI * pow(poreDistribution_[r].meanPoreRadius,2.0);//3D
                                const Scalar poreArea = 2 * poreDistribution_[r].meanPoreRadius; //2D
                                const Scalar nPores = this->sdProblem2().spatialParams().porosity(sdElement2, fvElemGeom2, vertInElem2) *  boundaryFactor*scvfAreaX_ * poreDistribution_[r].percentage / poreArea;

                                //calculate contribution of pore class to initial drop volume: drop cap with radius of pore times number of pores with this radius
                                //                               dropVolume += M_PI * 2 / 3 * pow(poreDistribution_[r].meanPoreRadius,3.0) * nPores; //3D
                                dropVolume[drop] += M_PI / 2 * pow(poreDistribution_[r].meanPoreRadius,2.0) * nPores; //2D
                            }
                        }
                    }
                }
            }
        }
    }

    /*!
     * \brief Called by the time manager after the time integration.
     */
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();

        counter_ = this->sdProblem1().currentVTKFileNumber() + 1;
        //update drop map and evaluate detachment
        updateDropMap(true);
    }

    /*!
     * \brief Called by Dumux::TimeManager whenever a solution for a
     *        timestep has been computed and the simulation time has
     *        been updated.
     *
     * \param dt docme
     *
     */
    Scalar nextTimeStepSize(const Scalar dt)
    {
        bool setTimeStep(false);

        //if one of the drops has detached or is close to detachment, reduce the time-step size to 0.5s
        for(int drop=0; drop < numberOfDrops_; drop++)
            if(detached_[drop] || closeToDetaching_[drop])
            {
                setTimeStep = true;
                break;
            }

//        if(setTimeStep && this->timeManager().timeStepSize() > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, ReducedTimeStepSize) && this->timeManager().timeStepIndex() > 1)
        if(setTimeStep && this->timeManager().timeStepIndex() > 1)
        {
            std::cout<<" reduce time-step size to " <<GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, ReducedTimeStepSize)<< "s since detached or close to detaching\n";
            //reset closetoDetaching_, detached_ is needed in dropFormationCondition and is resetted there
            for(int drop=0; drop < numberOfDrops_; drop++)
                closeToDetaching_[drop] = false;

            return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, ReducedTimeStepSize);
        }
        else
            return this->newtonController().suggestTimeStepSize(dt);
    };

    /*!
     * \brief Write the relevant quantities of the current solution into an VTK output file.
     */
    void writeOutput()
    {
        ParentType::writeOutput();
        // calculate the time _after_ the time was updated
        Scalar t = this->timeManager().time() + this->timeManager().timeStepSize();
	const MultiDomainGridView& mdGridView = this->mdGrid().leafView();
        std::cout<< "Writing result file for mortar elements\n";
        if (!resultWriter_)
            resultWriter_ = Dune::make_shared<VtkMultiWriter>(mdGridView, "mortar");
        resultWriter_->beginWrite(t);
        addOutputVtkFields(ParentType::model().curSol(), *resultWriter_);
        resultWriter_->endWrite();

        //output drop information
        std::cout << "Writing drop information\n";
        char outputname[20];
        sprintf(outputname, "%s%05d%s","dropInfo_", counter_,".out");
        std::ofstream outfile(outputname, std::ios_base::out);
        outfile << "GlobalIdx "
                << "TotalVolume "
                << "RhoVdrop "
                << "radius "
                << "areaDrop "
                << std::endl;
        dropVolume_ << this->timeManager().time() + this->timeManager().timeStepSize() << " ";
        for(int drop=0; drop < numberOfDrops_; drop++)
        {
            //output: drop volumes: now total drop volumes
            Scalar phi = 2 * theta_;
            Scalar radius = sqrt(2 *totalDropVolumes_[drop] / (phi - std::sin(phi)) /* /1m */);
            Scalar height = radius * (1 - std::cos(theta_));
            if(totalDropVolumes_[drop] >= 0.0)
                dropVolume_ <<totalDropVolumes_[drop]<<" " << height << " ";
            else
                dropVolume_<<0.0<<" "<<0.0<<" ";

            if(detached_[drop])
            {
                dropVolume_ <<std::endl;
                dropVolume_ << this->timeManager().time() + this->timeManager().timeStepSize() << " ";
                dropVolume_<<0.0<<" "<<0.0<<" ";
            }

            for(int node=0; node < nodesPerDrop_; node++)
            {
                IndexType globalVertexIdx = dropIdxMatrix_[drop][node];
                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: output \n";
                }

                outfile << globalVertexIdx << " "
                        << dropMapIt->second.totalDropVolume << " "
                        << dropMapIt->second.prevRhoVDrop << " "
                        << dropMapIt->second.dropRadius << " "
                        << dropMapIt->second.areaDrop << " "
                        << std::endl;
            }
        }
        dropVolume_ <<std::endl;
    }

    /*!
     * \brief Append all quantities of interest which can be derived
     *        from the solution of the current time step on the mortar
     *        elements to the VTK writer.
     *
     * \param sol The solution vector
     * \param writer The writer for multi-file VTK datasets
     */
    template<class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, 1> > ScalarField;
        //degrees of freedom of domain 1 and 2 without mortar elements
        const int numDofs = this->model().numDofs();
        unsigned numVertices = this->gridView().size(dim);

        //sort vector in ascending order of global Index
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorIndex());

        // create the required scalar fields
        ScalarField *mortarVy = writer.allocateManagedBuffer(numVertices);//!< vertical free flow velocity used as Neumann coupling condition for the porous medium
        ScalarField *mortarP  = writer.allocateManagedBuffer(numVertices);//!< pressure in porous medium used for Neuman coupling condition for normal Stokes flux
        ScalarField *mortarX  = writer.allocateManagedBuffer(numVertices);//!< lagrange multiplier standing for normal component fluxes
        ScalarField *mortarRhoVdrop = writer.allocateManagedBuffer(numVertices);//!< lagrange multiplier: rhow*drop volume /rhow
#if NI
        ScalarField *mortarT = writer.allocateManagedBuffer(numVertices);//!< lagrange multiplier for energy fluxes
#endif

        // initialize fields
        for (unsigned int idx = 0; idx < numVertices; ++idx)
        {
            (*mortarVy)[idx] = Scalar(0.0);
            (*mortarP)[idx] = Scalar(0.0);
            (*mortarX)[idx] = Scalar(0.0);
            (*mortarRhoVdrop)[idx] = Scalar(0.0);
#if NI
            (*mortarT)[idx] = Scalar(0.0);
#endif
        }

        unsigned numElements = this->gridView().size(0);
        ScalarField *rank = writer.allocateManagedBuffer(numElements);
        for (unsigned int idx = 0; idx < numElements; ++idx)
            (*rank)[idx] = Scalar(0.0);

        //fill vtk fields, loop over interface vertices
        //sol vector is filled in ascending order of global vertex indices
        long double sumRhoVdrop(0);
        for(int node=0; node < interfaceNodeInformation_.size(); node++)
        {
            int globalIdx = interfaceNodeInformation_[node].index;
            (*mortarVy)[globalIdx] = sol[numDofs + node * totalLMP + lambdaVy];
            (*mortarP)[globalIdx] = sol[numDofs + node * totalLMP + lambdaP];
            (*mortarX)[globalIdx] = sol[numDofs + node * totalLMP + lambdaX];
            (*mortarRhoVdrop)[globalIdx] = sol[numDofs + node * totalLMP + rhoVdrop];
#if NI
            (*mortarT)[globalIdx] = sol[numDofs + node * totalLMP + lambdaT];
#endif

            if(!this->sdProblem1().isInterfaceCornerPoint(interfaceNodeInformation_[node].globalPos))
                sumRhoVdrop += sol[numDofs + node * totalLMP + rhoVdrop];
            //           mortar_<<sol[numDofs + node * totalLMP + rhoVdrop]<<" ";
        }
        mortar_.precision(20);
        mortar_<< std::scientific << sumRhoVdrop<<std::endl;

        writer.attachVertexData(*mortarVy, "mortarVy");
        writer.attachVertexData(*mortarP, "mortarP");
        writer.attachVertexData(*mortarX, "mortarX");
        writer.attachVertexData(*mortarRhoVdrop, "mortarRhoVdrop");
#if NI
        writer.attachVertexData(*mortarT, "mortarT");
#endif

        writer.attachCellData(*rank, "process rank");
    }

    /*!
     * \brief Update the information about the drops per intersection.
     *
     * Initialize the information about the drops per intersection. Calculate the new drop radius
     * and area distribution dependent on the new drop volume. The Lagrange multiplier lambdaVDrop
     * is the drop volume times the water density. The information is stored in a map connection
     * each intersection with the according information.
     */
    void updateDropMap(bool evalDetachment)
    {
        std::vector<Scalar> totalDropVolumes(numberOfDrops_, 0.0), surfaceTensions(numberOfDrops_, 0.0);

        //calculate total volume per drop and store previous sub-Volume*rho=lambdarhoVDrop
        dealWithDropVolumes(totalDropVolumes, surfaceTensions);

        //now that the total volume per drop is known, update the rest of the drop information
        //loop over interface nodes by looping over nodes per drop
        Scalar dropSurface, dropArea, radius;
        //output: drop volumes: first, write out time
//        dropVolume_ << this->timeManager().time() + this->timeManager().timeStepSize() << " ";
        for(int drop=0; drop < numberOfDrops_; drop++)
        {
//            //output: drop volumes: now total drop volumes
//            dropVolume_ <<totalDropVolumes[drop]<<" ";
            totalDropVolumes_[drop] = totalDropVolumes[drop];
            for(int node=0; node < nodesPerDrop_; node++)
            {
                IndexType globalVertexIdx = dropIdxMatrix_[drop][node];
                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: update drop map \n";
                }

                //fill dropMap (Formeln stimmen 21.02.13 mit Taschenrechner nachgerechnet)
                //if drop has not been detached
                if( totalDropVolumes[drop] > 0) //&& !dropMapIt->second.detached)//negative drop volumes are non physical
                {
                    dropMapIt->second.totalDropVolume = totalDropVolumes[drop]; //the same for all nodes contributing to a drop
                    //calculate radius from total drop volume
                    /*********    3D spherical drop    *******************/
                    //                    radius = pow((3.0/M_PI * dropMapIt->second.totalDropVolume / (pow((1.0 - std::cos(theta_)),2.0) * (2.0 + std::cos(theta_)))),1.0/3.0); //the same for all nodes contributing to a drop
                    //                    dropMapIt->second.dropRadius = radius;
                    //                    //calculate areal fractions of interface
                    //                    dropSurface = 2.0 * M_PI * pow(dropMapIt->second.dropRadius,2.0) * (1.0 - std::cos(theta_)); //total drop surface
                    //                    dropMapIt->second.dropSurface = dropSurface / fractionalInterfaceArea_[drop];//total drop surface divided by total area of contributing cells
                    //                    dropArea = M_PI *  pow(dropMapIt->second.dropRadius * std::sin(theta_),2.0);//total drop contact area
                    //                    dropMapIt->second.areaDrop = dropArea / fractionalInterfaceArea_[drop];//total drop contact area divided by total area of contributing cells
                    /*********    2D zylindrical drop of depth 1m    *******************/
                    Scalar phi = 2 * theta_;
                    radius = 2 * dropMapIt->second.totalDropVolume / (phi - std::sin(phi)) /* /1m */;
                    radius = sqrt(radius);
                    dropMapIt->second.dropRadius = radius;
                    //calculate drop surface and volume
                    dropSurface = dropMapIt->second.dropRadius * phi /*  *1m */;//total drop surface
                    dropMapIt->second.dropSurface = dropSurface/ fractionalInterfaceArea_[drop];//total drop surface divided by total area of contributing cells
                    //TESTING set adrop = 1
                    dropArea = 2  * dropMapIt->second.dropRadius * std::sin(theta_)/* /1m */;//total drop contact area
                    dropMapIt->second.areaDrop = dropArea / fractionalInterfaceArea_[drop];//total drop contact area divided by total area of contributing cells

                    dropMapIt->second.areaGas = 1.0 - dropMapIt->second.areaDrop;
                    dropMapIt->second.qDropFormation = 0.0;
//                    std::cout<<"drop "<<drop<< " : radius "<<radius<<" drop area "<<dropArea<< " in percent "<<dropMapIt->second.areaDrop<<std::endl;
                }
                else if ( totalDropVolumes[drop] < 0)
                    std::cerr << "Negative drop volumes: "<<totalDropVolumes[drop]<<" in updateDropMap\n";//ToDo something should be done in case of negative drop volumes: detachment ???
            }
        }
//        dropVolume_ <<std::endl;
        //evaluate detachment
        if(evalDetachment)
            dropDetachment(totalDropVolumes, surfaceTensions);
    }

    void dealWithDropVolumes(std::vector<Scalar>& totalDropVolumes, std::vector<Scalar>& surfaceTensions)
    {
        IndexType globalVertexIdx, vertInElem2;
        std::vector<IndexType> interfaceNodes;//vector to check whether node has already been visited
        //sort vector in ascending order of global Index
        std::sort (interfaceNodeInformation_.begin(), interfaceNodeInformation_.end(), SortVectorIndex());

        const SolutionVector &curSol = ParentType::model().curSol();
        //degrees of freedom of domain 1 and 2 without mortar elements
        const int numDofs = this->model().numDofs();
        FVElementGeometry2 fvElemGeom2;
        ElementVolumeVariables2 elemVolVars2;

        //loop over interface intersections
        const MultiDomainGridView& mdGridView = this->gridView();
        const SubDomainInterfaceIterator endIfIt = this->mdGrid().leafSubDomainInterfaceEnd(mortarSubProblem1_, mortarSubProblem2_);
        for (SubDomainInterfaceIterator ifIt = this->mdGrid().leafSubDomainInterfaceBegin(mortarSubProblem1_, mortarSubProblem2_);
                ifIt != endIfIt; ++ifIt)
        {
            //turn intersection into intersectionGeometry (ifIt = pointer, *ifIt = intersection)
            IntersectionGeometry intersectionGeometry(*ifIt,0);
            const MultiDomainElementPointer mdElementPointer2 = intersectionGeometry.outside();
            const MultiDomainElement& mdElement2 = *mdElementPointer2;
            const SubDomainElementPointer sdElementPointer2 = this->sdElementPointer2(mdElement2);
            const SubDomainElement2& sdElement2 = *sdElementPointer2;
            int faceIdx2 = intersectionGeometry.indexInOutside();

            const Dune::GenericReferenceElement<typename MultiDomainGrid::ctype,dim>& referenceElement2 =
                    Dune::GenericReferenceElements<typename MultiDomainGrid::ctype,dim>::general(mdElement2.type());
            const int numVerticesOfFace = referenceElement2.size(faceIdx2, 1, dim);

            //loop over vertices of face
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                //ToDo CHANGE FOR NON_CONFORMING GRIDS
                vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, nodeInFace, dim);
                globalVertexIdx = mdGridView.indexSet().subIndex(mdElement2, vertInElem2 , dim);

                //check whether node has already been dealt with
                if(std::find(interfaceNodes.begin(), interfaceNodes.end(), globalVertexIdx)!= interfaceNodes.end())
                    continue;
                else
                    interfaceNodes.push_back(globalVertexIdx);

                //obtain consecutive node number on interface to access global solution vector
                int nodeNumber;
                for(int node=0; node < interfaceNodeInformation_.size(); node++)
                {
                    if(interfaceNodeInformation_[node].index == globalVertexIdx)
                    {
                        nodeNumber = node;
                        break;
                    }
                }

                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: deal with drop volumes \n";
                }

                //store previous LP rho*Vdrop at node nodeNumber=globalVertexIdx
                dropMapIt->second.prevRhoVDrop = curSol[numDofs + nodeNumber * totalLMP + rhoVdrop];//rho*Vdrop,i*Xww

                //volume variables to access the density
                fvElemGeom2.update(this->sdGridView2(), sdElement2);
                elemVolVars2.update(this->sdProblem2(),
                        sdElement2,
                        fvElemGeom2,
                        false);
                const VolumeVariables2& vertVars2 = elemVolVars2[vertInElem2];

                //check to which drop globalVertexIdx contributes and add up LPs rhoVdrop / rho to obtain total drop volume
                for(int drop=0; drop < numberOfDrops_; drop++)
                    if(std::find(dropIdxMatrix_[drop].begin(), dropIdxMatrix_[drop].end(), globalVertexIdx) != dropIdxMatrix_[drop].end())
                    {
                        totalDropVolumes[drop] += curSol[numDofs + nodeNumber * totalLMP + rhoVdrop] / (vertVars2.density(wPhaseIdx2) * vertVars2.fluidState().massFraction(wPhaseIdx2, wCompIdx2));
                        surfaceTensions[drop] = FluidSystem::surfaceTension(vertVars2.fluidState()); //save surface tension to calculate retention force later
                    }
            }
        }
    }

    /*!
     * \brief Determine if drop gets detached or not.
     */
    void dropDetachment(const std::vector<Scalar>& totalDropVolumes, const std::vector<Scalar>& surfaceTensions)
    {
        IndexType globalVertexIdx;
        std::vector<Scalar> velocityX(numberOfDrops_, 0.0), density(numberOfDrops_, 0.0), viscosity(numberOfDrops_, 0.0); //only needed for simple drag force

        //vectors storing the contributions to the drag force per drop (only needed for complicated drag force)
        std::vector<Scalar> schearForces(numberOfDrops_, 0.0);
        std::vector<Dune::FieldVector<Scalar, dim> > pressures(0.0);
        pressures.resize(numberOfDrops_);
        std::vector<Scalar> dynamicPressure(numberOfDrops_, 0.0);

        if(!simpleDragForce_)
            calculateDragForceContributions(schearForces, pressures, dynamicPressure);
        else
            determineFlowProperties(velocityX, density, viscosity);

        //determine if drops get detached
        for(int drop=0; drop < numberOfDrops_; drop++)
        {
            //get drop information of first node of drop (all other nodes of that drop contain the same information)
            globalVertexIdx = dropIdxMatrix_[drop][0];
            //get corresponding drop information
            const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
            if (dropMapIt == dropMap_.end()) {
                std::cerr << "Drop information of current vertex index not found in coupled problem: detachment 1\n";
            }

            //calculate retention force
            Scalar retenForce = retentionForce(dropMapIt->second.dropRadius, surfaceTensions[drop]);

            //calculate drag force from contributing forces
            Scalar dragForce;
            if(!simpleDragForce_)
                dragForce = - schearForces[drop] + (pressures[drop][0] - pressures[drop][1]) + dynamicPressure[drop];
            else
                dragForce = calculateSimpleDragForce(dropMapIt->second.dropRadius, velocityX[drop], density[drop], viscosity[drop]);

            std::cout<<"retention force: "<<retenForce<< " drag force: "<< std::scientific<<dragForce<<std::endl;

            //detachment condition: Fdrag > Fgamma or Vdrop <= 0 or if drop is wider than the area of contributing cells
            if((dragForce > retenForce || totalDropVolumes[drop] < 0.0 || (2*dropMapIt->second.dropRadius) > fractionalInterfaceArea_[drop] /* /1m */))// && totalDropVolumes[drop] != 0.0)
//          if((this->timeManager().time() > 90 && this->timeManager().time() < 110)
//                  || (this->timeManager().time() > 195 && this->timeManager().time() < 205)
//                  || (this->timeManager().time() > 295 && this->timeManager().time() < 305)
//                  || (this->timeManager().time() > 395 && this->timeManager().time() < 405)
//                  || (this->timeManager().time() > 495 && this->timeManager().time() < 505)
//                  || (this->timeManager().time() > 595 && this->timeManager().time() < 605)
//                  || (this->timeManager().time() > 695 && this->timeManager().time() < 705)
//                  || (this->timeManager().time() > 795 && this->timeManager().time() < 805)
//                  || (this->timeManager().time() > 895 && this->timeManager().time() < 905)
//                  || (this->timeManager().time() > 995))
          {
                detached_[drop] = true;

                std::cout<<"Drop " <<drop<<" gets detached.\n";

                //detachment occurs, set drop information for all nodes to 0
                for(int node=0; node < nodesPerDrop_; node++)
                {
                    globalVertexIdx = dropIdxMatrix_[drop][node];
                    //get corresponding drop information
                    const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                    if (dropMapIt == dropMap_.end()) {
                        std::cerr << "Drop information of current vertex index not found in coupled problem: detachment 2\n";}

                    dropMapIt->second.detached = true;

                    dropMapIt->second.totalDropVolume = 0.0; //the same for all nodes contributing to a drop
                    dropMapIt->second.dropRadius = 1e20;
                    dropMapIt->second.dropSurface = 0.0;
                    dropMapIt->second.areaDrop = 0.0;
                    dropMapIt->second.areaGas = 1.0 - dropMapIt->second.areaDrop;
                    dropMapIt->second.qDropFormation = 0.0;

                }
            }
            else//determine whether drops are close to detachment
            {
                //extrapolate detachment linearly assuming dt is doubled constant
                Scalar m = (dragForce - lastDragForce_[drop]) / this->timeManager().timeStepSize();
                Scalar futureDragForce = m * 3 * this->timeManager().timeStepSize() + lastDragForce_[drop];

                //calculate relative distance of retention and future drag force
                Scalar relDiffFuture = std::abs(retenForce - futureDragForce);
                relDiffFuture /= (retenForce + futureDragForce)/2;

                //calculate relative distance of retention and current drag force
                Scalar relDiff = std::abs(retenForce - dragForce);
                relDiff /= (retenForce + dragForce)/2;

                if((futureDragForce > retenForce || relDiffFuture <= 0.1 || relDiff <= 0.1))// && totalDropVolumes[drop] != 0.0)//Todo check limit
                    closeToDetaching_[drop] = true;
            }
            //store current drag force for the next time step
            lastDragForce_[drop] = dragForce;
        }
    }

    Scalar calculateSimpleDragForce(const Scalar radius, const Scalar velocityX, const Scalar density, const Scalar viscosity)
    {
        std::cout<<"simple drag force is calculated\n";
        const Scalar interface = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        Scalar gcHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) - interface;
        Scalar vMax = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem.FF, InitialvxMax);

        Scalar dragForce, projectedArea, dragCoefficient;

        //3D projected area
        //        projectedArea = radius * radius * (theta_ - 0.5*std::sin(2*theta_));
        //2D projected area (rectangle = dropheight * 1m)
        projectedArea = radius * (1 - std::cos(theta_));

        //drag coefficient
        //Cho et al. 2011 (simple) (3D)
        Scalar re = (0.5 * vMax * gcHeight * density) / viscosity;
        dragCoefficient = 30 / sqrt(re);
        //Cho et al 2011 (empirical)(3D)
//        Scalar re = (0.5 * vMax * gcHeight * density) / viscosity; //average velocity for parabolic profile = 0.5 vmax
//        Scalar exponent1 = 0.1757;
//        Scalar exponent2 = 0.2158 * (2 * radius / gcHeight) - 0.6384;
//        Scalar factorA = 46.247 * pow((2 * radius / gcHeight),exponent1);
//        dragCoefficient = factorA * pow(re,exponent2);

//        std::cout<<"Re: "<<re<<" vMax: "<<vMax<<" gcHeight: "<<gcHeight<<" density: "<<density<<" viscosity: "<<viscosity<<std::endl;
//        std::cout<<"Re: "<<re<<" exponent2: "<<exponent2<<" factorA: "<<factorA<<std::endl;
        std::cout<<"drag coefficient: "<<dragCoefficient<<std::endl;
        //Zhang et al 2006 (3D)
        //        Scalar re = (0.5 * vMax * 2 * radius * density) / viscosity;
        //        dragCoefficient = 4.62 / pow(re,0.37) * (1 + 5.2 * pow(re,-0.63));
        //for a lying cylinder
        // ???

        dragForce = 0.5 * density * velocityX * velocityX  * dragCoefficient * projectedArea;
        std::cout<<"density: "<<density<<" velocityX: "<<velocityX<<" projectedArea: "<<projectedArea<<std::endl;

        return dragForce;
    }

    void determineFlowProperties(std::vector<Scalar>& velocityX, std::vector<Scalar>& density, std::vector<Scalar>& viscosity)
    {
        const Scalar interface = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        Scalar gcHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) - interface;

        FVElementGeometry1 fvElemGeom1;
        ElementVolumeVariables1 elemVolVars1;

        //loop over elements of Stokes domain and calculate forces
        SubDomainElementIterator elemEndIt1 = this->sdGridView1().template end<0>();
        for (SubDomainElementIterator elemIt1 = this->sdGridView1().template begin<0>(); elemIt1 != elemEndIt1; ++elemIt1)
        {
            //update element volume variables
            const SubDomainElement1& sdElement1 = *elemIt1;
            const Dune::GenericReferenceElement<typename SubDomainGrid::ctype,dim>& refElement1 =
                   Dune::GenericReferenceElements<typename SubDomainGrid::ctype,dim>::general(sdElement1.geometry().type());

            fvElemGeom1.update(this->sdGridView1(), sdElement1);
            elemVolVars1.update(this->sdProblem1(),
                   sdElement1,
                   fvElemGeom1,
                   false);

            StokesIntersectionIterator isIt = this->sdGridView1().ibegin(sdElement1);
            const StokesIntersectionIterator &endIt = this->sdGridView1().iend(sdElement1);
            for(; isIt != endIt; ++isIt)
            {
                const int faceIdx1 = isIt->indexInInside();
                DimVector scvfNormal = fvElemGeom1.subContVolFace[faceIdx1].normal;

                //2D horizontal angenommen
                if(scvfNormal[1] == 0.0) //y-entry of normal is zero -> vertical face
                    scvfAreaY_ = scvfNormal.two_norm();
                if(scvfNormal[0] == 0.0)
                    assert(Dumux::arePointsEqual(scvfNormal.two_norm(),scvfAreaX_));
            }

            //loop over vertices(scvs) of element
            for (int scvIdx = 0; scvIdx < fvElemGeom1.numScv; scvIdx++)
            {
                //global position of vertex in domain 1
                const GlobalPosition &globalPos = sdElement1.geometry().corner(scvIdx);

                if(globalPos[0] < runUpDistanceX_)
                    continue;

                //determine global index of the corresponding interface node to access drop information
                IndexType globalVertexIdx;
                for(int node=0; node < interfaceNodeInformation_.size(); node++)
                {
                    if(Dumux::arePointsEqual(interfaceNodeInformation_[node].globalPos[0], globalPos[0]))
                    {
                        globalVertexIdx = interfaceNodeInformation_[node].index;
                        break;
                    }
                }

                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: flow properties \n";
                }

                //determine drop idx
                int dropIdx;
                for(int drop=0; drop < numberOfDrops_; drop++)
                {
                    if(std::find(dropIdxMatrix_[drop].begin(), dropIdxMatrix_[drop].end(), globalVertexIdx) != dropIdxMatrix_[drop].end())
                    {
                        dropIdx = drop;
                        break;
                    }
                }
                //loop over intersections/faces of element
                isIt = this->sdGridView1().ibegin(sdElement1);
                for (; isIt != endIt; ++isIt)
                {
                    const int faceIdx1 = isIt->indexInInside();
                    //                    const Scalar scvfArea = fvElemGeom1.subContVolFace[faceIdx1].normal.two_norm();
                    const int numFaceVertices = refElement1.size(faceIdx1, 1, dim);

                    // loop over the single vertices on the current face
                    for (int faceVertIdx = 0; faceVertIdx < numFaceVertices; ++faceVertIdx)
                    {
                        //check if element and face vertex match
                        const int elemVertIdx = refElement1.subEntity(faceIdx1, 1, faceVertIdx, dim);
                        if (elemVertIdx != scvIdx)
                           continue;

                        VolumeVariables1 volVars1 = elemVolVars1[scvIdx];

                        //get coordinate information (drop height, area of influence of drop)
                        Scalar dropHeight = dropMapIt->second.dropRadius*(1 - std::cos(theta_)); //the same for 2D and 3D
                        GlobalPosition globalPosLeft, globalPosRight;
                        if (dropIdx == 0)
                        {
                           globalPosLeft[0] = 0.0; //for the first vertex, the left border is 0

                           if(dropIdx == borders_.size()-1)
                               globalPosRight[0] = borders_[dropIdx][0];// the right end of the domain is reached
                           else
                               globalPosRight[0] = borders_[dropIdx][0] + scvfAreaX_; //the right border is the corresponding entry in borders_
                        }
                        else
                        {
                           globalPosLeft[0] = borders_[dropIdx-1][0] + scvfAreaX_; //the left border is the right border of the previous drop (last node of drop) + an element-size (2 scvf)

                           if(dropIdx == borders_.size()-1)
                               globalPosRight[0] = borders_[dropIdx][0];// the right end of the domain is reached
                           else
                               globalPosRight[0] = borders_[dropIdx][0] + scvfAreaX_; //the right border is the corresponding entry in borders_
                        }
                        //determine global positions to evaluate pressure and dynamic-pressure forces
                        //the y-coordinate is the middle of the empty space above the drop
                        globalPosLeft[1] = (dropHeight + (gcHeight - dropHeight)/2) + interface;
                        globalPosRight[1] = (dropHeight + (gcHeight - dropHeight)/2) + interface;

                        GlobalPosition dropFront, dropBack;
                        dropFront[0] = globalPosLeft[0] + (globalPosRight[0] - globalPosLeft[0])/2 - dropMapIt->second.dropRadius;
                        dropFront[1] = dropHeight - dropMapIt->second.dropRadius + interface;

                        if (Dumux::isBetween(dropFront[0], globalPos[0] - scvfAreaX_, globalPos[0] + scvfAreaX_+ 1e-8)
                        && Dumux::isBetween(dropFront[1], globalPos[1] - scvfAreaY_, globalPos[1] + scvfAreaY_ + 1e-8)) //evaluate horizontal velocity, density and viscosity at drop front
                        {
                            velocityX[dropIdx] = volVars1.velocity()[velocityXIdx1];
                            density[dropIdx] = volVars1.density();
                            viscosity[dropIdx] = volVars1.dynamicViscosity();
                        }
                    }
                }
            }
        }
    }

    void calculateDragForceContributions(std::vector<Scalar>& schearForces, std::vector<Dune::FieldVector<Scalar, dim> >& pressures, std::vector<Scalar>& dynamicPressure)
    {
        std::cout<<"contribution for force balance to determine drag force is calculated\n";
        const Scalar interface = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        Scalar gcHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) - interface;

        FVElementGeometry1 fvElemGeom1;
        ElementVolumeVariables1 elemVolVars1;

        //loop over elements of Stokes domain and calculate forces
        SubDomainElementIterator elemEndIt1 = this->sdGridView1().template end<0>();
        for (SubDomainElementIterator elemIt1 = this->sdGridView1().template begin<0>(); elemIt1 != elemEndIt1; ++elemIt1)
        {
            //update element volume variables
            const SubDomainElement1& sdElement1 = *elemIt1;
            const Dune::GenericReferenceElement<typename SubDomainGrid::ctype,dim>& refElement1 =
                    Dune::GenericReferenceElements<typename SubDomainGrid::ctype,dim>::general(sdElement1.geometry().type());

            fvElemGeom1.update(this->sdGridView1(), sdElement1);
            elemVolVars1.update(this->sdProblem1(),
                    sdElement1,
                    fvElemGeom1,
                    false);

            StokesIntersectionIterator isIt = this->sdGridView1().ibegin(sdElement1);
            const StokesIntersectionIterator &endIt = this->sdGridView1().iend(sdElement1);
            for(; isIt != endIt; ++isIt)
            {
                const int faceIdx1 = isIt->indexInInside();
                DimVector scvfNormal = fvElemGeom1.subContVolFace[faceIdx1].normal;

                //2D horizontal angenommen
                if(scvfNormal[1] == 0.0) //y-entry of normal is zero -> vertical face
                    scvfAreaY_ = scvfNormal.two_norm();
                if(scvfNormal[0] == 0.0)
                    assert(Dumux::arePointsEqual(scvfNormal.two_norm(),scvfAreaX_));
            }

            //loop over vertices(scvs) of element
            for (int scvIdx = 0; scvIdx < fvElemGeom1.numScv; scvIdx++)
            {
                //global position of vertex in domain 1
                const GlobalPosition &globalPos = sdElement1.geometry().corner(scvIdx);

                if(globalPos[0] < runUpDistanceX_)
                    continue;

                //determine global index of the corresponding interface node to access drop information
                IndexType globalVertexIdx;
                for(int node=0; node < interfaceNodeInformation_.size(); node++)
                {
                    if(Dumux::arePointsEqual(interfaceNodeInformation_[node].globalPos[0], globalPos[0]))
                    {
                        globalVertexIdx = interfaceNodeInformation_[node].index;
                        break;
                    }
                }

                //get corresponding drop information
                const DropMapIterator dropMapIt =  dropMap_.find(globalVertexIdx);
                if (dropMapIt == dropMap_.end()) {
                    std::cerr << "Drop information of current vertex index not found in coupled problem: drag force \n";
                }

                //determine drop idx
                int dropIdx;
                for(int drop=0; drop < numberOfDrops_; drop++)
                {
                    if(std::find(dropIdxMatrix_[drop].begin(), dropIdxMatrix_[drop].end(), globalVertexIdx) != dropIdxMatrix_[drop].end())
                    {
                        dropIdx = drop;
                        break;
                    }
                }
                //loop over intersections/faces of element
                isIt = this->sdGridView1().ibegin(sdElement1);
                for (; isIt != endIt; ++isIt)
                {
                    const int faceIdx1 = isIt->indexInInside();
                    //                    const Scalar scvfArea = fvElemGeom1.subContVolFace[faceIdx1].normal.two_norm();
                    const int numFaceVertices = refElement1.size(faceIdx1, 1, dim);

                    // loop over the single vertices on the current face
                    for (int faceVertIdx = 0; faceVertIdx < numFaceVertices; ++faceVertIdx)
                    {
                        //check if element and face vertex match
                        const int elemVertIdx = refElement1.subEntity(faceIdx1, 1, faceVertIdx, dim);
                        if (elemVertIdx != scvIdx)
                            continue;

                        VolumeVariables1 volVars1 = elemVolVars1[scvIdx];

                        //get coordinate information (drop height, area of influence of drop)
                        Scalar dropHeight = dropMapIt->second.dropRadius*(1 - std::cos(theta_)); //the same for 2D and 3D
                        GlobalPosition globalPosLeft, globalPosRight;
                        if (dropIdx == 0)
                        {
                            globalPosLeft[0] = 0.0; //for the first vertex, the left border is 0

                            if(dropIdx == borders_.size()-1)
                                globalPosRight[0] = borders_[dropIdx][0];// the right end of the domain is reached
                            else
                                globalPosRight[0] = borders_[dropIdx][0] + scvfAreaX_; //the right border is the corresponding entry in borders_
                        }
                        else
                        {
                            globalPosLeft[0] = borders_[dropIdx-1][0] + scvfAreaX_; //the left border is the right border of the previous drop (last node of drop) + an element-size (2 scvf)

                            if(dropIdx == borders_.size()-1)
                                globalPosRight[0] = borders_[dropIdx][0];// the right end of the domain is reached
                            else
                                globalPosRight[0] = borders_[dropIdx][0] + scvfAreaX_; //the right border is the corresponding entry in borders_
                        }
                        //determine global positions to evaluate pressure and dynamic-pressure forces
                        //the y-coordinate is the middle of the empty space above the drop
                        globalPosLeft[1] = (dropHeight + (gcHeight - dropHeight)/2) + interface;
                        globalPosRight[1] = (dropHeight + (gcHeight - dropHeight)/2) + interface;

                        GlobalPosition dropFront, dropBack;
                        dropFront[0] = globalPosLeft[0] + (globalPosRight[0] - globalPosLeft[0])/2 - dropMapIt->second.dropRadius;
                        dropFront[1] = dropHeight - dropMapIt->second.dropRadius + interface;

                        dropBack[0] = globalPosLeft[0] + (globalPosRight[0] - globalPosLeft[0])/2 + dropMapIt->second.dropRadius;
                        dropBack[1] = dropHeight - dropMapIt->second.dropRadius + interface;

                        //calculate drag force contribution
                        if (this->sdProblem1().onUpperBoundary_(globalPos))//calculate shear force on upper boundary
                        {
                            const int boundaryFaceIdx = fvElemGeom1.boundaryFaceIndex(faceIdx1, faceVertIdx);
                            const Scalar boundaryFaceArea =  fvElemGeom1.boundaryFace[boundaryFaceIdx].area;

                            if (isIt->boundary() && Dumux::arePointsEqual(boundaryFaceArea, scvfAreaX_))//only evaluate shear forces on top boundary (not on vertical faces of corner nodes)
                            {
                                const FluxVariables1 fluxVars1(this->sdProblem1(),
                                        sdElement1,
                                        fvElemGeom1,
                                        boundaryFaceIdx,
                                        elemVolVars1,
                                        true/*onBoundary*/);

                                Dune::FieldMatrix<Scalar, dim, dim> velGrad = fluxVars1.velocityGrad();
                                schearForces[dropIdx] += fluxVars1.dynamicViscosity() * (velGrad[velocityXIdx1][1] + velGrad[velocityYIdx1][0]) * boundaryFaceArea * dropMapIt->second.areaDrop/* 1m*/;
                            }
                        }
                        if (Dumux::isBetween(dropFront[0], globalPos[0] - scvfAreaX_, globalPos[0] + scvfAreaX_ + 1e-6)
                        && Dumux::isBetween(globalPosLeft[1], globalPos[1]- scvfAreaY_, globalPos[1] + scvfAreaY_ + 1e-8))//calculate pressure force above drop: left
                        {
                            //                            std::cout<<"right pressure at: "<<globalPos<<" with front = " <<dropFront<<" back= "<< dropBack<<std::endl;
                            pressures[dropIdx][0] = volVars1.pressure()*(gcHeight - dropHeight) /* 1m*/;
                        }
                        if(Dumux::isBetween(dropBack[0], globalPos[0] - scvfAreaX_, globalPos[0] + scvfAreaX_ + 1e-6)
                        && Dumux::isBetween(globalPosRight[1], globalPos[1] - scvfAreaY_, globalPos[1] + scvfAreaY_ + 1e-8/*include point on scvf*/))//calculate pressure force above drop: right
                        {
                            //                            std::cout<<"right pressure at: "<<globalPos<<std::endl;
                            pressures[dropIdx][1] = volVars1.pressure()*(gcHeight - dropHeight) /* 1m*/;
                        }
                        if (Dumux::isBetween(dropFront[0], globalPos[0] - scvfAreaX_, globalPos[0] + scvfAreaX_+ 1e-6)
                        && Dumux::isBetween(dropFront[1], globalPos[1] - scvfAreaY_, globalPos[1] + scvfAreaY_ + 1e-8)) //calculate dynamic pressure force at drop front
                        {
                            dynamicPressure[dropIdx] = (volVars1.density()/2 * volVars1.velocity()[velocityXIdx1] *  volVars1.velocity()[velocityXIdx1]) * dropHeight/* 1m*/;
                        }
                    }
                }
            }
        }
    }
    /*!
     * \brief Calculates the retention force of a drop of  a certain radius.
     */
    Scalar retentionForce(const Scalar& radius, const Scalar& surfaceTension)
    {
        const Scalar interface = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
        Scalar gcHeight = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, UpperRightY) - interface;
        /*********    3D spherical drop    *******************/
       Scalar contactArea =  M_PI *  pow(radius * std::sin(theta_),2.0);//total drop contact area
       Scalar contactAreaRadius = sqrt(contactArea / M_PI);

       Scalar factor = (std::sin(deltaTheta_ - theta_) - std::sin(theta_)) / (theta_ - M_PI);
       factor += (std::sin(deltaTheta_ - theta_) -  std::sin(theta_)) / (theta_ + M_PI);

       std::cout<<"3D retention force dependent on radius: "<< (surfaceTension * contactAreaRadius * M_PI * factor)<<std::endl;

//                return (surfaceTension * contactAreaRadius * M_PI * factor); //Kumbur et al 2006
        /*********    2D zylindrical drop with depth 1m (Chen et al 2005)  *******************/
        Scalar thetaAdv = theta_;
        Scalar thetaRec = theta_ - deltaTheta_;

        return (surfaceTension * (std::cos(M_PI - thetaAdv) + std::cos(thetaRec)) /* *1m */);//* gcHeight/* *1m */);//see page 160 in my notes and Chen et al 2006
    }

    /*!
     * \brief Called by the time manager after the end of an episode.
     */
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    MortarSubProblem1& stokes2cDropsProblem()
    { return this->sdProblem1(); }
    const MortarSubProblem1& stokes2cDropsProblem() const
    { return this->sdProblem1(); }

    MortarSubProblem2& twoPtwoCDropsProblem()
    { return this->sdProblem2(); }
    const MortarSubProblem2& twoPtwoCDropsProblem() const
    { return this->sdProblem2(); }

    const IntegrationPointGeometryMap& integrationPointGeometryMapFirst()
    {return integrationPointGeometryMapFirst_;}
    IntegrationPointGeometryMap& insertInIntegrationPointGeometryMapFirst()
    {return integrationPointGeometryMapFirst_;}

    const IntegrationPointGeometryMap& integrationPointGeometryMapSecond()
    {return integrationPointGeometryMapSecond_;}
    IntegrationPointGeometryMap&  insertInIntegrationPointGeometryMapSecond()
    {return integrationPointGeometryMapSecond_;}

    const IntegrationPointGeometryMapCoupling& integrationPointGeometryMapCoupling()
    {return integrationPointGeometryMapCoupling_;}
    IntegrationPointGeometryMapCoupling&  insertInIntegrationPointGeometryMapCoupling()
    {return integrationPointGeometryMapCoupling_;}

    GridFunctionSpace &gridFunctionSpace()
    { return this->model().jacobianAssembler().mdGridFunctionSpace(); }
    const GridFunctionSpace &gridFunctionSpace() const
    { return this->model().jacobianAssembler().mdGridFunctionSpace(); }

    const DropMap& dropMap()
    {return dropMap_;}

    const Scalar contactAngle()
    {return theta_;}

    const Scalar meanPoreRadius()
    {return meanPoreRadius_;}

    const std::vector<PoreClass> poreDistribution()
    {return poreDistribution_;}

private:
    typename MultiDomainGrid::SubDomainType mortarSubProblem1_;
    typename MultiDomainGrid::SubDomainType mortarSubProblem2_;

    int counter_;
    Scalar episodeLength_;
    Scalar initializationTime_;
    IntegrationPointGeometryMap integrationPointGeometryMapFirst_;
    IntegrationPointGeometryMap integrationPointGeometryMapSecond_;
    IntegrationPointGeometryMapCoupling integrationPointGeometryMapCoupling_;

    DropMap dropMap_;
    Scalar theta_; //contact angle in radians //fluid-matrix interaction and fluid property
    Scalar deltaTheta_; //could be calculated dependent on flow conditions ...
    int nodesPerDrop_;  //number of cells contributing to one drop
    int numberOfDrops_;
    Scalar meanPoreRadius_;
    std::vector<GlobalPosition> borders_; //global Positions of right border nodes of drops
    std::vector<bool> closeToDetaching_;

    //interfaceInformation
    int numInterfaceNodes_;
    Scalar scvfAreaX_;
    Scalar scvfAreaY_;
    Scalar runUpDistanceX_;
    //vector filled with structs of index and globalPosition
    //sorted according to index for output and according to globalPosition for borders between drops
    std::vector<IndexPositionPair> interfaceNodeInformation_;
    std::vector<std::vector <IndexType> > dropIdxMatrix_; //connects global indices of vertexes contributing to a drop
    std::vector<PoreClass> poreDistribution_;
    std::vector<Scalar> fractionalInterfaceArea_;
    std::vector<Scalar> lastDragForce_;
    std::vector<Scalar> totalDropVolumes_;

    std::vector<bool> detached_;
    bool simpleDragForce_;

    template <int numEquations>
    struct InterfaceFluxes
    {
        int count;
        int interfaceVertex;
        int globalIdx;
        Scalar xCoord;
        Scalar yCoord;
        Dune::FieldVector<Scalar, numEquations> defect;

        InterfaceFluxes()
        {
            count = 0;
            interfaceVertex = 0;
            globalIdx = 0;
            xCoord = 0.0;
            yCoord = 0.0;
            defect = 0.0;
        }
    };
    std::ofstream dropVolume_;
    std::ofstream mortar_;
    Dune::shared_ptr<VtkMultiWriter> resultWriter_;

};

} //end namespace

#endif
