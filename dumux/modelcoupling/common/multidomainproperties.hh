// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_PROPERTIES_HH
#define DUMUX_MULTIDOMAIN_PROPERTIES_HH

#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblemlocalfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>
#include <dune/pdelab/multidomain/subdomainset.hh>
#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>

#include <dune/pdelab/finiteelementmap/p1fem.hh>
#include <dune/pdelab/finiteelementmap/q1fem.hh>
#include <dune/pdelab/backend/istlmatrixbackend.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>

#include <dumux/linear/linearsolverproperties.hh>

#include "coupledproperties.hh"
#include "subdomainpropertydefaults.hh"

/*!
 * \file
 * \brief Specify properties required for the multidomain stuff
 */
namespace Dumux
{
namespace Properties
{
/*!
 * \addtogroup ModelCoupling
 */
// \{

//////////////////////////////////////////////////////////////////
//! The type tag for problems which use dune-multidomain
NEW_TYPE_TAG(MultiDomain, INHERITS_FROM(CoupledModel));
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

//! Specifies the host grid
NEW_PROP_TAG(Grid);

//! Specifies the multidomain grid
NEW_PROP_TAG(MDGrid);

//! Specifies the multidomain grid function space
NEW_PROP_TAG(MDGridFunctionSpace);

//! Specifies the equality conditions
NEW_PROP_TAG(MDCondition);

//! Specifies the multidomain type based subproblem for subdomain 1
NEW_PROP_TAG(MDSubProblem1);
//! Specifies the multidomain type based subproblem for subdomain 2
NEW_PROP_TAG(MDSubProblem2);

//! the local coupling operator for use with dune-multidomain
NEW_PROP_TAG(MDCouplingLocalOperator);

//! Specifies the multidomain coupling
NEW_PROP_TAG(MDCoupling);

//! Property tag for the multidomain constraints transformation
NEW_PROP_TAG(MDConstraintsTrafo);

//! Specifies the multidomain grid operator
NEW_PROP_TAG(MDGridOperator);

NEW_PROP_TAG(ConstraintsTrafo);

//type tags for drops
NEW_PROP_TAG(DropMap);
}
}

namespace Dumux
{
namespace Properties
{
//////////////////////////////////////
// Set property values
//////////////////////////////////////
SET_PROP(MultiDomain, MDGrid)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef typename Dune::mdgrid::FewSubDomainsTraits<HostGrid::dimension,4> MDGridTraits;
public:
    typedef typename Dune::MultiDomainGrid<HostGrid, MDGridTraits> type;
};

SET_PROP(MultiDomain, MDGridFunctionSpace)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGrid) MDGrid;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;
    typedef typename GET_PROP_TYPE(SubTypeTag1, GridFunctionSpace) GridFunctionSpace1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridFunctionSpace) GridFunctionSpace2;
public:
    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<MDGrid, 
																	Dune::PDELab::ISTLVectorBackend<1>, 
																	GridFunctionSpace1,
																	GridFunctionSpace2> type;
};

// set the subdomain equality condition by default
SET_PROP(MultiDomain, MDCondition)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGrid) MDGrid;
public:
    typedef Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MDGrid> type;
};

SET_PROP(MultiDomain, MDSubProblem1)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGridFunctionSpace) MDGridFunctionSpace;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(SubTypeTag1, Constraints) Constraints1;
    typedef typename GET_PROP_TYPE(SubTypeTag1, LocalOperator) LocalOperator1;
    typedef typename GET_PROP_TYPE(TypeTag, MDCondition) MDCondition;
    typedef typename GET_PROP_TYPE(SubTypeTag1, GridFunctionSpace) GridFunctionSpace1;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<MDGridFunctionSpace,
                                                  MDGridFunctionSpace,
    										      LocalOperator1, MDCondition,
    											  0> type;
};

SET_PROP(MultiDomain, MDSubProblem2)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGridFunctionSpace) MDGridFunctionSpace;
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem2TypeTag) SubTypeTag2;
    typedef typename GET_PROP_TYPE(SubTypeTag2, Constraints) Constraints2;
    typedef typename GET_PROP_TYPE(SubTypeTag2, LocalOperator) LocalOperator2;
    typedef typename GET_PROP_TYPE(TypeTag, MDCondition) MDCondition;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridFunctionSpace) GridFunctionSpace2;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<MDGridFunctionSpace,
    											  MDGridFunctionSpace,
    											  LocalOperator2, MDCondition,
    											  1> type;
};

SET_PROP(MultiDomain, MDCoupling)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDSubProblem1) MDSubProblem1;
    typedef typename GET_PROP_TYPE(TypeTag, MDSubProblem2) MDSubProblem2;
    typedef typename GET_PROP_TYPE(TypeTag, MDCouplingLocalOperator) MDCouplingLocalOperator;
public:
    typedef Dune::PDELab::MultiDomain::Coupling<MDSubProblem1,
        										MDSubProblem2,
        										MDCouplingLocalOperator> type;
};

// set trivial constraints transformation by default
// TODO create a proper constraint transformation
SET_PROP(MultiDomain, MDConstraintsTrafo)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, SubProblem1TypeTag) SubTypeTag1;
public:
    typedef typename GET_PROP_TYPE(SubTypeTag1, ConstraintsTrafo) type;
};

SET_PROP(MultiDomain, MDGridOperator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGridFunctionSpace) MDGridFunctionSpace;
    typedef Dune::PDELab::ISTLBCRSMatrixBackend<1,1> MBE;
    typedef typename GET_PROP_TYPE(TypeTag, MDSubProblem1) MDSubProblem1;
    typedef typename GET_PROP_TYPE(TypeTag, MDSubProblem2) MDSubProblem2;
    typedef typename GET_PROP_TYPE(TypeTag, MDCoupling) MDCoupling;
    typedef typename GET_PROP_TYPE(TypeTag, MDConstraintsTrafo) MDConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
        			MDGridFunctionSpace, MDGridFunctionSpace,
        			MBE, Scalar, Scalar, Scalar,
        			MDConstraintsTrafo, MDConstraintsTrafo,
        			MDSubProblem1, MDSubProblem2, MDCoupling> type;
};

SET_PROP(MultiDomain, JacobianMatrix)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MDGridOperator) MDGridOperator;
public:
    typedef typename MDGridOperator::Traits::Jacobian type;
};

SET_INT_PROP(MultiDomain, LinearSolverBlockSize, GET_PROP_VALUE(TypeTag, NumEq));

// \}

}
}

#endif
