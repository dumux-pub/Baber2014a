// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_ENRICHED_PROPERTIES_HH
#define DUMUX_MULTIDOMAIN_ENRICHED_PROPERTIES_HH

#include <dune/pdelab/backend/istlvectorbackend.hh>
#include "multidomainproperties.hh"

/*!
 * \file
 * \brief Specify properties required for the multidomain stuff
 */
namespace Dumux
{
namespace Properties
{
/*!
 * \addtogroup ModelCoupling
 */
// \{

//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for problems which use dune-multidomain
NEW_TYPE_TAG(MultiDomainEnriched, INHERITS_FROM(MultiDomain));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

//!Specifies the FEM space used for the mortar elements
NEW_PROP_TAG(MortarFEMSpace);

//!Specifies the constraints for the mortar elements
NEW_PROP_TAG(MortarConstraints);

//!Specifies the predicat indicating the interface for additional dofs
NEW_PROP_TAG(MortarPredicat);

//! Specifies the scalar grid function space for the mortar elements
NEW_PROP_TAG(MortarScalarGridFunctionSpace);

//! Specifies the grid function space for the mortar elements
NEW_PROP_TAG(MortarGridFunctionSpace);

//! Specifies whether a tube micro-model has to be considered at the interface, set to false by default
NEW_PROP_TAG(WithTubes);
    
//!type tag to store drop properties
NEW_PROP_TAG(DropMap);

}
}

namespace Dumux
{
namespace Properties
{

//////////////////////////////////////
// Set property values
//////////////////////////////////////
SET_BOOL_PROP(MultiDomainEnriched, WithTubes, false);

SET_PROP(MultiDomainEnriched, MortarFEMSpace)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;
    enum{dim = MultiDomainGridView::dimension};
public:
    typedef Dune::PDELab::Q1LocalFiniteElementMap<Scalar,Scalar,dim-1> type;
};

SET_PROP(MultiDomainEnriched, MortarConstraints)
{
    typedef typename Dune::PDELab::NoConstraints type;
};

SET_PROP(MultiDomainEnriched, MortarPredicat)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCondition) MultiDomainCondition;
public:
    typedef typename Dune::PDELab::MultiDomain::SubProblemSubProblemInterface<MultiDomainGridView,MultiDomainCondition,MultiDomainCondition> type;

};

SET_PROP(MultiDomainEnriched, MortarScalarGridFunctionSpace)
{private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename MultiDomainGrid::LeafGridView MultiDomainGridView;
    typedef typename GET_PROP_TYPE(TypeTag, MortarFEMSpace) MortarFEM;
    typedef typename GET_PROP_TYPE(TypeTag, MortarConstraints) MortarConstraints;
    typedef typename GET_PROP_TYPE(TypeTag, MortarPredicat) MortarPredicat;
public:

    typedef Dune::PDELab::MultiDomain::CouplingGridFunctionSpace<MultiDomainGridView, MortarFEM, MortarPredicat, MortarConstraints, Dune::PDELab::ISTLVectorBackend<1> >
           type;
};

SET_PROP(MultiDomainEnriched, MortarGridFunctionSpace)
{private:
    typedef typename GET_PROP_TYPE(TypeTag, MortarScalarGridFunctionSpace) MortarScalarGridFunctionSpace;
    enum{numEq = GET_PROP_VALUE(TypeTag, NumEq)};
public:

    typedef Dune::PDELab::MultiDomain::PowerCouplingGridFunctionSpace<MortarScalarGridFunctionSpace, numEq, Dune::PDELab::GridFunctionSpaceBlockwiseMapper>
        type;
};

//replaces MultiDomainGridFunctionSpace defined in parent class to include Mortarspace
SET_PROP(MultiDomainEnriched, MultiDomainGridFunctionSpace)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MultiDomainGrid;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) SubTypeTag2;
    typedef typename GET_PROP_TYPE(SubTypeTag1, GridFunctionSpace) GridFunctionSpace1;
    typedef typename GET_PROP_TYPE(SubTypeTag2, GridFunctionSpace) GridFunctionSpace2;
    typedef typename GET_PROP_TYPE(TypeTag, MortarGridFunctionSpace) MortarGridFunctionSpace;
public:
    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace
    								   <MultiDomainGrid, Dune::PDELab::ISTLVectorBackend<1>, GridFunctionSpace1,GridFunctionSpace2, MortarGridFunctionSpace> type;
};

//replaces ConstraintsTrafo defined in pdelabadapter, now member of MultiDomainGridFunctionSpace
SET_PROP(MultiDomainEnriched, MultiDomainConstraintsTrafo)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) MultiDomainGridFunctionSpace;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) SubTypeTag1;
    typedef typename GET_PROP_TYPE(SubTypeTag1, LocalFEMSpace) LocalFEMSpace;
    typedef typename LocalFEMSpace::Traits::FiniteElementType::Traits::
          LocalBasisType::Traits::RangeFieldType R;
public:
    typedef typename MultiDomainGridFunctionSpace::template ConstraintsContainer<R>::Type type;
};
//replaces MultiDomainGridOperator, so that the new MultiDomainConstraintsTrafo is used
SET_PROP(MultiDomainEnriched, MultiDomainGridOperator)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace) MultiDomainGridFunctionSpace;
    typedef Dune::PDELab::ISTLBCRSMatrixBackend<1,1> MBE;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem1) MultiDomainSubProblem1;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem2) MultiDomainSubProblem2;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCoupling) MultiDomainCoupling;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainConstraintsTrafo) MultiDomainConstraintsTrafo;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
                    MultiDomainGridFunctionSpace, MultiDomainGridFunctionSpace,
                    MBE, Scalar, Scalar, Scalar,
                    MultiDomainConstraintsTrafo, MultiDomainConstraintsTrafo,
                    MultiDomainSubProblem1, MultiDomainSubProblem2, MultiDomainCoupling> type;
};
//replaces MultiDomainCoupling defined in parent class, now of type Enriched Coupling
SET_PROP(MultiDomainEnriched, MultiDomainCoupling)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem1) MultiDomainSubProblem1;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainSubProblem2) MultiDomainSubProblem2;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCouplingLocalOperator) MultiDomainCouplingLocalOperator;
public:
    typedef Dune::PDELab::MultiDomain::EnrichedCoupling<MultiDomainSubProblem1,
                                              MultiDomainSubProblem2,
                                              MultiDomainCouplingLocalOperator,2> type; //what does the 2 stand for?
};

}
}

#endif
