// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Specification of the material parameters
 *       for the Acosta constitutive relations.
 */
#ifndef ACOSTA_PARAMS_HH
#define ACOSTA_PARAMS_HH

namespace Dumux
{
/*!
 *
 * \brief Specification of the material parameters
 *       for the Acosta constitutive relations.

 *
 * \ingroup fluidmatrixinteractionsparams
 */
template<class ScalarT>
class AcostaParams
{
public:
    typedef ScalarT Scalar;

    AcostaParams()
    {}

    AcostaParams(Scalar acA, Scalar acB, Scalar acC, Scalar acD, Scalar acE)
    {
        setAcA(acA);
        setAcB(acB);
        setAcC(acC);
        setAcD(acD);
        setAcE(acE);
    };

    /*!
     * \ Acosta's curve.
     */
    Scalar acA() const
    { return acA_; }

    /*!
     * \ Acosta's curve.
     */
    void setAcA(Scalar a)
    { acA_ = a; }

    /*!
     * \Acosta's curve.
     */
    Scalar acB() const
    { return acB_; }

    void setAcB(Scalar b)
    { acB_ = b; }

	/*!
	 * \ Acosta's curve.
	 */
	Scalar acC() const
	{ return acC_; }

	/*!
	 * \ Acosta's curve.
	 */
	void setAcC(Scalar c)
	{ acC_ = c; }

	/*!
	 * \ Acosta's curve.
	 */
	Scalar acD() const
	{ return acD_; }

	/*!
	 * \ Acosta's curve.
	 */
	void setAcD(Scalar d)
	{ acD_ = d; }

	/*!
	 * \ Acosta's curve.
	 */
	Scalar acE() const
	{ return acE_; }

	/*!
	 * \ Acosta's curve.
	 */
	void setAcE(Scalar e)
	{ acE_ = e; }



private:
    Scalar acA_;
    Scalar acB_;
    Scalar acC_;
    Scalar acD_;
    Scalar acE_;
};
} // namespace Dumux

#endif
