
install(FILES
        acosta.hh
        acostaparams.hh
        philtophoblaw.hh
        regularizedacosta.hh
        regularizedacostaparams.hh
        thermalconductivityfuelcell.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/material/fluidmatrixinteractions/2p)
