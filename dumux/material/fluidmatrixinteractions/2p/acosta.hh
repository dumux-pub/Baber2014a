// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   Implementation of the capillary pressure and
 *          water phase saturation according to Acosta.
 */
#ifndef ACOSTA_HH
#define ACOSTA_HH

#include "acostaparams.hh"

#include <algorithm>
#include <cmath>
#include <cassert>

namespace Dumux
{

static const int integrationSteps = 20;
static const double intImb01 = 3.1349682090e-08;
static const double intDra01 = 9.2189777362e-01;
static const std::vector<double> intImb0Sw = {0.0000000000e+00, 1.4448654771e-08, 2.2819077599e-08, 2.7262869143e-08, 2.9465634517e-08, 3.0504079857e-08,
		3.0976893747e-08, 3.1187237078e-08, 3.1279418104e-08, 3.1319431548e-08, 3.1336696665e-08, 3.1344118639e-08, 3.1347301932e-08, 3.1348665344e-08,
		3.1349248803e-08, 3.1349498362e-08, 3.1349605071e-08, 3.1349650691e-08, 3.1349670192e-08, 3.1349678527e-08, 3.1349682090e-08};
static const std::vector<double> intImbSw1 = {3.1349682090e-08, 1.6901027319e-08, 8.5306044914e-09, 4.0868129471e-09, 1.8840475735e-09, 8.4560223338e-10,
		3.7278834340e-10, 1.6244501261e-10, 7.0263986151e-11, 3.0250542321e-11, 1.2985425390e-11, 5.5634511134e-12, 2.3801587239e-12, 1.0167462066e-12,
		4.3328717089e-13, 1.8372876392e-13, 7.7019533374e-14, 3.1399697336e-14, 1.1898564029e-14, 3.5629091876e-15, 0.0000000000e+00};
static const std::vector<double> intDra0Sw = {0.0000000000e+00, 9.0286303155e-11, 1.0440347151e-09, 5.3755677032e-09, 2.0922261595e-08, 7.4676307329e-08,
		2.8041525597e-07, 1.3662599439e-06, 2.4661875855e-05, 9.2186966645e-01, 9.2188585863e-01, 9.2189413795e-01, 9.2189728035e-01, 9.2189773354e-01,
		9.2189777056e-01, 9.2189777338e-01, 9.2189777360e-01, 9.2189777360e-01, 9.2189777359e-01, 9.2189777356e-01, 9.2189777362e-01};
static const std::vector<double> intDraSw1 = {9.2189777362e-01, 9.2189777350e-01, 9.2189777259e-01, 9.2189776823e-01, 9.2189775271e-01, 9.2189769898e-01,
		9.2189749322e-01, 9.2189640736e-01, 9.2187311174e-01, 2.8107160428e-05, 1.1914958378e-05, 3.6356677731e-06, 4.9325835742e-07, 4.0074681584e-08,
		3.0250536841e-09, 2.3593717317e-10, 1.8918448614e-11, 1.5374841055e-12, 1.2497552223e-13, 9.4684316652e-15, 0.0000000000e+00};
/*!
 * \ingroup fluidmatrixinteractionslaws
 *
 * \brief Implementation of the Acosta capillary pressure <->
 *        saturation relation. This class bundles the "raw" curves
 *        as static members and doesn't concern itself converting
 *        absolute to effective saturations and vice versa.
 *
 * For general info: EffToAbsLaw
 *
 * \see AcostaParams
 */
template <class ScalarT, class ParamsT = AcostaParams<ScalarT> >
class Acosta
{
public:
    typedef ParamsT     Params;
    typedef typename    Params::Scalar Scalar;

    /*!
     * \brief The capillary pressure-saturation curve according to Acosta.
     *
     * Acosta's empirical capillary pressure <-> saturation
     * function is given by
     * \f[
     p_c = A*exp(B*Sw+C)+D*(1-Sw)+E/Sw
     \f]
     * Acosta et al equation 28
     * \param swe       effective water phase saturation
     *
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar pc(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);
//        std::cout<<"Acosta\n";
//        std::cout<<"saturaint: "<<swe<<std::endl;

//		return params.acA() * exp(params.acB() * swe + params.acC()) + params.acD() * (1-swe) + params.acE() / swe;

        Scalar pc = exp(params.acB() * swe + params.acC());
        pc *= params.acA();
        pc += params.acD() * (1.0 - swe);
        pc += params.acE() / swe;
//        pc /= 10000;
        return pc;
    }


    /*!
     * \brief The saturation-capillary pressure curve according to Acosta.
     *
     * This is the inverse of the capillary pressure-saturation curve:
     * \f[ does not exist yet \f]
     *
     * \param pc        Capillary pressure
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     * \return          The effective saturation of the wetting phase
     */
    static Scalar sw(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::sw(params, pc)");
    }

    /*!
     * \brief The partial derivative of the capillary
     *        pressure w.r.t. the effective saturation according to Acosta.
     *
     * This is equivalent to
     * \f[ does not exist yet \f]
     *
     * \param swe       Effective saturation of the wetting phase \f$\overline{S}_w\f$
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
    */
    static Scalar dpc_dsw(const Params &params, Scalar swe)
    {
    	assert(0 <= swe && swe <= 1);

//    	return params.acA() * params.acB() * exp(params.acB() * swe + params.acC()) - params.acD() - params.acE() / (swe+swe);

    	Scalar dpc_dsw = exp(params.acB() * swe + params.acC());
    	dpc_dsw *= params.acA();
    	dpc_dsw *= params.acB();
    	dpc_dsw -= params.acD();
    	dpc_dsw -= params.acE() / (swe*swe);
    	return dpc_dsw;

//        DUNE_THROW(Dune::NotImplemented, "Acosta::dpc_dsw(params, swe)");
    }

    /*!
     * \brief The partial derivative of the effective
     *        saturation to the capillary pressure according to Acosta.
     *
     *        function does not exist yet!
     *
     * \param pc        Capillary pressure \f$p_C\f$
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dsw_dpc(const Params &params, Scalar pc)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dsw_dpc(params, pc)");
    }

    /*!
     * \brief The relative permeability for the wetting phase of
     *        the medium implied by Acosta's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.     */
    static Scalar krw(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

//		return swe*swe;//EffLaw::krn(params, SwToSwe(params, 1.0 - sNonWet));

        Scalar krw;
        //imbibition
        if(!params.acE())
        {
			Scalar int0Sw = evaluateIntegral_(intImb0Sw, swe);
			krw = swe * swe *(int0Sw / intImb01);
        }
        //drainage
        else
        {
        	Scalar int0Sw = evaluateIntegral_(intDra0Sw, swe);
        	krw = swe * swe *(int0Sw / intDra01);
        }

//        //Scalar r = 1. - pow(1 - pow(swe, 1/params.vgm()), params.vgm());
//        //return sqrt(swe)*r*r;

//      Scalar krw;
//    	double integral, integral01, error;
//
//    	romberg(integral01, error, 0., 1., 1.0e-12, params);
//    	romberg(integral, error, 0., swe, 1.0e-12, params);
//    	krw = swe*swe*(integral/integral01);
//
    	assert(0 <= krw && krw <= 1);

		return(krw);

    };

    /*!
     * \brief The derivative of the relative permeability for the
     *        wetting phase in regard to the wetting saturation of the
     *        medium implied by the van Genuchten parameterization.
     *
     * \param swe       The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrw_dsw(const Params &params, Scalar swe)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dkrw_dsw(params, swe)");
    };

    /*!
     * \brief The relative permeability for the non-wetting phase
     *        of the medium implied by van Genuchten's
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar krn(const Params &params, Scalar swe)
    {
        assert(0 <= swe && swe <= 1);

//            return (1.0-swe)*(1.0-swe)*(1.0-swe);//EffLaw::krw(params, SwToSwe(params, 1.0 - sNonWet));

        Scalar krn;
        //imbibition
        if(!params.acE())
        {
			Scalar intSw1= evaluateIntegral_(intImbSw1, swe);
			krn = (1.0 - swe) * (1 - swe) *(intSw1 / intImb01);
        }
        //drainage
        else
        {
        	Scalar intSw1 = evaluateIntegral_(intDraSw1, swe);
        	krn = (1.0 - swe) * (1 - swe) *(intSw1 / intDra01);
        }

//        //return
//           // pow(1 - swe, 1.0/3) *
//           // pow(1 - pow(swe, 1/params.vgm()), 2*params.vgm());

//      Scalar krn;
//      double integral, integral01, error;
//
//		romberg(integral01, error, 0., 1., 1.0e-12, params);
//		romberg(integral, error, swe, 1., 1.0e-12, params);
//		krn = (1.-swe)*(1.-swe)*(integral/integral01);
//
		assert(0 <= krn && krn <= 1);

		return(krn);

    };

    /*!
     * \brief The derivative of the relative permeability for the
     *        non-wetting phase in regard to the wetting saturation of
     *        the medium as implied by the van Genuchten
     *        parameterization.
     *
     * \param swe        The mobile saturation of the wetting phase.
     * \param params    A container object that is populated with the appropriate coefficients for the respective law.
     *                  Therefore, in the (problem specific) spatialParameters  first, the material law is chosen, and then the params container
     *                  is constructed accordingly. Afterwards the values are set there, too.
     */
    static Scalar dkrn_dsw(const Params &params, Scalar swe)
    {
        DUNE_THROW(Dune::NotImplemented, "Acosta::dkrn_dsw(params, swe)");
    }

private:

    static Scalar evaluateIntegral_(const std::vector<double>& intVector, const Scalar swe)
    {
    	//(integrationSteps - 1)*(swe - swMin) / (swMax - swMin)
    	Scalar satPos = (integrationSteps - 1) * swe;
    	unsigned satIdx = (unsigned)satPos;
    	Scalar weight = satPos - satIdx;

    	//interpolate integral at swe
    	Scalar integral = intVector[satIdx] * (1 - weight) + intVector[satIdx + 1] * weight;

    	return integral;
    }

};
}

#endif
