// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief   Relation for the saturation-dependent effective thermal conductivity
 */
#ifndef THERMALCONDUCTIVITY_FUELCELL_HH
#define THERMALCONDUCTIVITY_FUELCELL_HH

#include <algorithm>

namespace Dumux
{
/*!
 * \ingroup fluidmatrixinteractionslaws
 *
 * \brief Relation for the saturation-dependent effective thermal conductivity
 *
 * The effective thermal conductivity of the anisotropic fibrous hydrophobic gas diffusion layer of PEM fuel cells
 * should represent the wetting properties and the influence of the pore distribution. As a first guess,
 * a weighted average of the heat conductivity of the void space and the solid material is employed, as suggested by
 * Das et al. (2010) and Pharoah et al. (2006).
 *
 * The material law is:
 * \f[
 \lambda_\text{eff} =  \Phi * \lambda_{\text{void}} +  \left(1 - \Phi\right) \lambda_\text{s}
 \f]
 *
 * with
 * \f[
 \lambda_\text{void} = \frac{\lambda_1 + \lambda_2}{2}
 \f]
 * and
 *
 * \f[
 \lambda_1 = \frac{1}{\frac{S_l}{\lambda_l} + \frac{(1-S_l)}{\lambda_g}}
 \f]
  * \f[
 \lambda_1 = S_l \lambda_l + (1-S_l) \lambda_g
 \f]
 */
template<class Scalar>
class ThermalConductivityFuelCell
{
public:
    /*!
     * \brief Returns the effective thermal conductivity \f$[W/(m K)]\f$.
     *
     * \param sw The saturation of the wetting phase
     * \param lambdaW the thermal conductivity of the wetting phase
     * \param lambdaN the thermal conductivity of the non-wetting phase
     * \param lambdaSolid the thermal conductivity of the solid phase
     * \param porosity The porosity
     *
     * \return Effective thermal conductivity \f$[W/(m K)]\f$ after Somerton (1974)
     *
     */
    static Scalar effectiveThermalConductivity(const Scalar sw,
                                               const Scalar lambdaW,
                                               const Scalar lambdaN,
                                               const Scalar lambdaSolid,
                                               const Scalar porosity)
    {
        //Das et al. (2010)
        const Scalar lambda1 = 1.0 / (sw/lambdaW + (1-sw)/lambdaN);
        const Scalar lambda2 = sw*lambdaW + (1-sw)*lambdaN;
        const Scalar lambdaVoid = 0.5*(lambda1 + lambda2);
        //Pharoah et al. (2006)
        const Scalar lambdaeff = porosity*lambdaVoid + (1-porosity)*lambdaSolid;
        
        return lambdaeff;
        
    }
};
}
#endif
