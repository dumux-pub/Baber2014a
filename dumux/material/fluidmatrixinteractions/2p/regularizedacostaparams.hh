// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Specification of the material parameters
 *       for the Acosta constitutive relations.
 */
#ifndef REGULARIZED_ACOSTA_PARAMS_HH
#define REGULARIZED_ACOSTA_PARAMS_HH

#include "acostaparams.hh"

namespace Dumux
{
/*!
 *
 * \brief  Parameters that are necessary for the \em regularization of
 *          the Acosta capillary pressure model.

 *
 * \ingroup fluidmatrixinteractionsparams
 */
template <class ScalarT>
class RegularizedAcostaParams : public Dumux::AcostaParams<ScalarT>
{
    typedef Dumux::AcostaParams<ScalarT> AcostaParams;

public:
    typedef ScalarT Scalar;

    RegularizedAcostaParams()
        : AcostaParams()
    {
    }

    RegularizedAcostaParams(Scalar acA, Scalar acB, Scalar acC, Scalar acD, Scalar acE)
        : AcostaParams(acA, acB, acC, acD, acE)
    {
    }

    /*!
     * \brief Threshold saturation below which the capillary pressure
     *        is regularized.
     *
     * This is just 1%. If you need a different value, overload this
     * class.
     */
    Scalar thresholdSw() const
    {
        // Some problems are very sensitive to this value
        // (e.g. makeing it smaller might result in negative
        // pressures), if you change it here, you will almost
        // certainly break someone's code!
        //
        // If you want to use a different regularization threshold,
        // overload this class and supply the new class as second
        // template parameter for the RegularizedVanGenuchten law!
        return /* PLEASE DO _NOT_ */ 1e-3; /* CHANGE THIS VALUE. READ
                                            * COMMENT ABOVE! */
    }

};
} // namespace Dumux

#endif

