Summary
=======
This is the DuMuX module containing the code for producing the results
published in:

K. Baber<br>
[Coupling free flow and flow in porous media in biological and technical
applications: from a simple to a complex interface description]
(http://elib.uni-stuttgart.de/handle/11682/611?locale=en)<br>
PhD thesis, 2014<br>
DOI: 10.18419/opus-594

Installation
============

The easiest way to install this module is to create a new folder and to execute
the file [installBaber2014a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Baber2014a/raw/master/installBaber2014a.sh)
in this folder.
For more detailed informations on installation, have a look at the
[DuMuX installation guide](http://www.dumux.org/installation.php)
or use the [DuMuX handbook]
(http://www.dumux.org/documents/dumux-handbook-2.9.pdf), chapter 2.

Applications
============

The applications can be found in the folder `appl`, particularly in the
following subfolders.

* `fuelcelldrops`: Section 7.3.4 Simplified parameter study. Contains the source
  file `test_fuelcelldrops2cstokes2p2c.cc`. The program runs, but breaks before
  the results in the Dissertation can be fully reproduced.
* `fuelcelldropsni`: Sections 7.3.2 and 7.3.3 The actual drop model. Contains
  the source file `test_fuelcelldropsncnistokes2p2cni.cc`. The program runs and
  reproduces the reference case with one drop.
* `fuelcellcoupling`: Section 7.3.1 Simple coupling with electro-chemistry.
  Contains the source file `fuelcellcoupling.cc`. Exhibits worse convergence
  than results presented in the dissertation.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules as well as
grid managers, have a look at [installBaber2014a.sh]
(https://git.iws.uni-stuttgart.de/dumux-pub/Baber2014a/raw/master/installBaber2014a.sh).

In addition, the linear solver SuperLU has to be installed.

The module has been tested successfully with GCC 4.8. Problems occurred with
other compilers, for example GCC 5.
