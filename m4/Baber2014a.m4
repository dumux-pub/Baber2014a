dnl -*- autoconf -*-
# Macros needed to find Baber2014a and dependent libraries.  They are called by
# the macros in ${top_src_dir}/dependencies.m4, which is generated by
# "dunecontrol autogen"

# Additional checks needed to build Baber2014a
# This macro should be invoked by every module which depends on Baber2014a, as
# well as by Baber2014a itself
AC_DEFUN([BABER2014A_CHECKS])

# Additional checks needed to find Baber2014a
# This macro should be invoked by every module which depends on Baber2014a, but
# not by Baber2014a itself
AC_DEFUN([BABER2014A_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([Baber2014a],[Baber2014a/Baber2014a.hh])
])
